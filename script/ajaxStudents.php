<?php

include "pdoAjax.php";

$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length'];
$columnIndex = $_POST['order'][0]['column'];
$columnName = $_POST['columns'][$columnIndex]['data'];
$columnSortOrder = $_POST['order'][0]['dir'];
$searchValue = $_POST['search']['value'];

$searchArray = array();

$searchQuery = " ";
if ($searchValue != ''){
    $searchQuery = " AND (firstname LIKE :firstname OR email LIKE :email OR lastname LIKE :lastname) ";
    $searchArray = array(
        'firstname' => "%$searchValue%",
        'lastname' => "%$searchValue",
        'email' => "%$searchValue",
    );
}

## Sans filtre
$stmt = $conn->prepare('SELECT COUNT(*) AS allcount FROM nfoz_users');
$stmt->execute();
$records = $stmt->fetch();
$totalRecords = $records['allcount'];

## Avec filtre

$stmt = $conn->prepare('SELECT COUNT(*) AS allcount FROM nfoz_users WHERE 1 ' .$searchQuery);
$stmt->execute($searchArray);
$records = $stmt->fetch();
$totalRecordsWithFilter = $records['allcount'];

## Fetch records

$stmt = $conn->prepare('SELECT * FROM nfoz_users WHERE 1' .$searchQuery. ' ORDER BY ' .$columnName. ' ' .$columnSortOrder. ' LIMIT ' . (int)$row . ', ' . (int)$rowperpage);
foreach($searchArray as $key => $search){
    $stmt->bindValue(':'.$key, $search,PDO::PARAM_STR);
}
$stmt->execute();
$userRecords = $stmt->fetchAll();

$data = array();

foreach($userRecords as $row){
    $data[] = array(
        "id" => $row['id'],
        "firstname" => $row['firstname'],
        "lastname" => $row['lastname'],
        "email" => $row['email'],
        "address" => $row['address'],
    );
}


$response = array(
    "draw" => intVal($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordsWithFilter,
    "aaData" => $data
);

echo json_encode($response);