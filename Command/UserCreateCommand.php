<?php

namespace Scolabs\Command;

use Scolabs\Core\Command\CommandInterface;
use Scolabs\Core\Command\Receiver;
use Scolabs\Managers\UserManager;
use Scolabs\Models\User;

class UserCreateCommand implements CommandInterface
{
    /**
     * @var Receiver
     */
    private $output;


    private $args;


    /**
     * UserCreateCommand constructor.
     * @param Receiver $console
     */
    public function __construct(Receiver $console)
    {
        $this->output = $console;
        $this->output->enableDate();
    }

    public function execute()
    {
        $this->output->write('Création en cours');

        $user = new User();

        $user->setEmail($this->args[0]);
        $user->setPassword(password_hash($this->args[1], PASSWORD_BCRYPT));
        $user->setFirstname($this->args[2]);
        $user->setLastname($this->args[3]);

        $userManager = new UserManager();
        $userManager->save($user);

        $this->output->write('User Created Successfully');

        $this->output->printOutput();
    }

    /**
     * @param array $args
     * @return UserCreateCommand
     */
    public function setArgs(array $args)
    {
        $this->args = $args;

        return $this;
    }
}