<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class InvoiceProduct
 * @package Scolabs\models
 */
class Invoiceproduct extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var float
     */
    protected $vat;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return Invoice
     */
    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice): void
    {
        $this->invoice = $invoice;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return float
     */
    public function getVat(): float
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat(float $vat): void
    {
        $this->vat = $vat;
    }

    /**
     * @return array
     */
    public function initRelation(): array
    {
        return [
            'id_product' => [
                'class' => Product::class,
                'setter' => 'setProduct',
                'name' => 'product'
            ],
            'id_invoice' => [
                'class' => Invoice::class,
                'setter' => 'setInvoice',
                'name' => 'invoice'
            ]
        ];
    }
}
