<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class Book
 * @package Scolabs\Models
 */
class Book extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $isbn;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var object
     */
    private $informations;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIsbn(): string
    {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn(string $isbn)
    {
        $this->isbn = $isbn;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return object
     */
    public function getInformations()
    {
        return $this->informations;
    }

    /**
     * @param object $informations
     */
    public function setInformations($informations): void
    {
        $this->informations = $informations;
    }
}
