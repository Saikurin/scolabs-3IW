<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class Role
 * @package Scolabs\models
 */
class Contact extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var bool
     */
    protected $viewed;

    /**
     * @var bool
     */
    protected $answered;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return bool
     */
    public function getViewed()
    {
        return $this->viewed ?? false;
    }

    /**
     * @param bool $viewed
     */
    public function setViewed($viewed = 0): void
    {
        $this->viewed = $viewed;
    }

    /**
     * @return bool
     */
    public function getAnswered(): bool
    {
        return $this->answered ?? false;
    }

    /**
     * @param bool $answered
     */
    public function setAnswered($answered = 0): void
    {
        $this->answered = $answered;
    }
}
