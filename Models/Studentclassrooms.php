<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class StudentClassrooms
 * @package Scolabs\models
 */
class Studentclassrooms extends Model
{

    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var User
     */
    protected $student;

    /**
     * @var Classroom
     */
    protected $classroom;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getStudent(): User
    {
        return $this->student;
    }

    /**
     * @param User $student
     * @return void
     */
    public function setStudent(User $student)
    {
        $this->student = $student;
    }

    /**
     * @return Classroom
     */
    public function getClassroom(): Classroom
    {
        return $this->classroom;
    }

    /**
     * @param Classroom $classroom
     * @return void
     */
    public function setClassroom(Classroom $classroom)
    {
        $this->classroom = $classroom;
    }

    /**
     * @return array|string[][]
     */
    public function initRelation(): array
    {
        return [
            'id_student' => [
                'class' => User::class,
                'setter' => 'setStudent',
                'name' => 'student'
            ],
            'id_classroom' => [
                'class' => Classroom::class,
                'setter' => 'setClassroom',
                'name' => 'classroom'
            ]
        ];
    }
}
