<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class Acl
 * @package Scolabs\models
 */
class Acl extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $action;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Acl
     */
    public function setId(int $id): Acl
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     * @return Acl
     */
    public function setController(string $controller): Acl
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     * @return Acl
     */
    public function setAction(string $action): Acl
    {
        $this->action = $action;
        return $this;
    }
}
