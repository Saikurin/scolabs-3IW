<?php


namespace Scolabs\Models;


use Scolabs\Core\Model;

class Mark extends Model
{

    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var float
     */
    protected $note;

    /**
     * @var float
     */
    protected $coeff;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var Subject
     */
    protected $subject;

    /**
     * @var User
     */
    protected $student;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return float
     */
    public function getNote(): float
    {
        return $this->note;
    }

    /**
     * @param float $note
     */
    public function setNote(float $note)
    {
        $this->note = $note;
    }

    /**
     * @return float
     */
    public function getCoeff(): float
    {
        return $this->coeff;
    }

    /**
     * @param float $coeff
     */
    public function setCoeff(float $coeff)
    {
        $this->coeff = $coeff;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date)
    {
        $this->date = $date;
    }

    /**
     * @return Subject
     */
    public function getSubject(): Subject
    {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject(Subject $subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return User
     */
    public function getStudent(): User
    {
        return $this->student;
    }

    /**
     * @param User $student
     */
    public function setStudent(User $student)
    {
        $this->student = $student;
    }

    /**
     * @return array|string[]
     */
    public function initRelation(): array
    {
        return [
            'id_student' => [
                'class' => User::class,
                'setter' => 'setStudent',
                'name' => 'student'
            ],
            'id_subject' => [
                'class' => Subject::class,
                'setter' => 'setSubject',
                'name' => 'subject'
            ]
        ];
    }
}