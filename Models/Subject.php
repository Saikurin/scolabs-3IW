<?php


namespace Scolabs\Models;


use Scolabs\Core\Model;

class Subject extends Model
{
    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var User
     */
    protected $teacher;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return User
     */
    public function getTeacher(): User
    {
        return $this->teacher;
    }

    /**
     * @param User $teacher
     */
    public function setTeacher(User $teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @return array|string[]
     */
    public function initRelation(): array
    {
        return [
            'id_teacher' => [
                'class' => User::class,
                'setter' => 'setTeacher',
                'name' => 'teacher'
            ]
        ];
    }

}