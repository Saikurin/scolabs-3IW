<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class BookBorrowing
 * @package Scolabs\Models
 */
class BookBorrowing extends Model
{
    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var string
     */
    protected $start;
    /**
     * @var string
     */
    protected $end;
    /**
     * @var string
     */
    protected $state;
    /**
     * @var Book
     */
    protected $book;
    /**
     * @var User
     */
    protected $user;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @param string $start
     */
    public function setStart(string $start): void
    {
        $this->start = $start;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @param string $end
     */
    public function setEnd(string $end): void
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return Book
     */
    public function getBook(): Book
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook(Book $book): void
    {
        $this->book = $book;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return array|string[]
     */
    public function initRelation(): array
    {
        return [
            'id_book' => [
                'class' => Book::class,
                'setter' => 'setBook',
                'name' => 'book'
            ],
            'id_user' => [
                'class' => User::class,
                'setter' => 'setUser',
                'name' => 'user'
            ]
        ];
    }
}
