<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class AclRole
 * @package Scolabs\models
 */
class Aclrole extends Model
{

    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var Role
     */
    protected $role;

    /**
     * @var Acl
     */
    protected $acl;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     */
    public function setRole(Role $role): void
    {
        $this->role = $role;
    }

    /**
     * @return Acl
     */
    public function getAcl(): Acl
    {
        return $this->acl;
    }

    /**
     * @param Acl $acl
     */
    public function setAcl(Acl $acl): void
    {
        $this->acl = $acl;
    }

    /**
     * @return array|string[]
     */
    public function initRelation(): array
    {
        return [
            'id_role' => [
                'class' => Role::class,
                'setter' => 'setRole',
                'name' => 'role'
            ],
            'id_acl' => [
                'class' => Acl::class,
                'setter' => 'setAcl',
                'name' => 'acl'
            ]
        ];
    }
}
