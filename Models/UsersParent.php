<?php


namespace Scolabs\models;

use Scolabs\Core\Model;

/**
 * Class UsersParents
 * @package Scolabs\models
 */
class UsersParent extends Model
{
    /**
     * @var null|int|string
     */
    protected $id;

    /**
     * @var int
     */
    protected $id_parent;

    /**
     * @var int
     */
    protected $id_student;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdParent(): int
    {
        return $this->id_parent;
    }

    /**
     * @param int $id_parent
     */
    public function setIdParent(int $id_parent): void
    {
        $this->id_parent = $id_parent;
    }


    /**
     * @return int
     */
    public function getIdStudent(): int
    {
        return $this->id_student;
    }

    /**
     * @param int $id_student
     */
    public function setIdStudent(int $id_student): void
    {
        $this->id_student = $id_student;
    }

}
