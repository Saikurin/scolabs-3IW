<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class Invoice
 * @package Scolabs\models
 */
class Invoice extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $state;

    /**
     * @var string
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $updatedAt;

    /**
     * @var User
     */
    protected $creator;

    /**
     * @var User
     */
    protected $updator;

    /**
     * @var User
     */
    protected $student;

    /**
     * @var User
     */
    protected $customer;

    /**
     * @return array|string[]
     */
    public function initRelation(): array
    {
        return [
            'id_creator' => [
                'class' => User::class,
                'setter' => 'setCreator',
                'name' => 'creator',
            ],
            'id_student' => [
                'class' => User::class,
                'setter' => 'setStudent',
                'name' => 'student',
            ],
            'id_updator' => [
                'class' => User::class,
                'setter' => 'setUpdator',
                'name' => 'updator',
            ],
            'id_customer' => [
                'class' => User::class,
                'setter' => 'setCustomer',
                'name' => 'customer',
            ],
        ];
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt(string $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    /**
     * @param string $updatedAt
     */
    public function setUpdatedAt(string $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return User
     */
    public function getCreator(): User
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator(User $creator): void
    {
        $this->creator = $creator;
    }

    /**
     * @return User
     */
    public function getUpdator(): User
    {
        return $this->updator;
    }

    /**
     * @param User $updator
     */
    public function setUpdator(User $updator): void
    {
        $this->updator = $updator;
    }

    /**
     * @return User
     */
    public function getStudent(): User
    {
        return $this->student;
    }

    /**
     * @param User $student
     */
    public function setStudent(User $student): void
    {
        $this->student = $student;
    }

    /**
     * @return User
     */
    public function getCustomer(): User
    {
        return $this->customer;
    }

    /**
     * @param User $customer
     */
    public function setCustomer(User $customer): void
    {
        $this->customer = $customer;
    }
}
