<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

class Course extends Model
{
    /**
     * @var ?int
     */
    protected $id;

    /**
     * @var string
     */
    protected $start;

    /**
     * @var string
     */
    protected $end;

    /**
     * @var string
     */
    protected $room;

    /**
     * @var Classroom
     */
    protected $classroom;

    /**
     * @var Subject
     */
    protected $subject;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getStart(): string
    {
        return $this->start;
    }

    /**
     * @param string $start
     */
    public function setStart(string $start): void
    {
        $this->start = $start;
    }

    /**
     * @return string
     */
    public function getEnd(): string
    {
        return $this->end;
    }

    /**
     * @param string $end
     */
    public function setEnd(string $end): void
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getRoom(): string
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom(string $room): void
    {
        $this->room = $room;
    }

    /**
     * @return Classroom
     */
    public function getClassroom(): Classroom
    {
        return $this->classroom;
    }

    /**
     * @param Classroom $classroom
     */
    public function setClassroom(Classroom $classroom): void
    {
        $this->classroom = $classroom;
    }

    /**
     * @return Subject
     */
    public function getSubject(): Subject
    {
        return $this->subject;
    }

    /**
     * @param Subject $subject
     */
    public function setSubject(Subject $subject): void
    {
        $this->subject = $subject;
    }

    public function initRelation(): array
    {
        return [
            "id_subject" => [
                "class"=> Subject::class,
                "setter"=> "setSubject",
                "name" => "subject"
            ],
            "id_classroom" => [
                "class"=> Classroom::class,
                "setter"=> "setClassroom",
                "name" => "classroom"
            ]
        ];
    }
}
