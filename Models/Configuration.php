<?php


namespace Scolabs\Models;

use Scolabs\Core\Model;

/**
 * Class Configuration
 * @package Scolabs\Models
 */
class Configuration extends Model
{

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $conf;

    /**
     * @var string
     */
    protected $value;

    /**
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getConf(): string
    {
        return $this->conf;
    }

    /**
     * @param string $conf
     */
    public function setConf(string $conf): void
    {
        $this->conf = $conf;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
