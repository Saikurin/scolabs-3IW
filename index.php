<?php

use Scolabs\Core\ConstantLoader;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\core\exceptions\RequiredMissingException;
use Scolabs\Core\Helpers;
use Scolabs\Core\Router;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Models\Configuration;

session_start();

function myAutoloader($class)
{
    require("vendor/autoload.php");
    $class = str_replace('Scolabs', '', $class);
    $class = str_replace('\\', '/', $class);

    if ($class[0] === '/') {
        include substr($class . '.php', 1);
    } else {
        include $class . '.php';
    }
}

spl_autoload_register("myAutoloader");

try {
    new ConstantLoader();
    Helpers::checkInstall();

    $routes = yaml_parse_file("routes.yml");
    $router = new Router($_SERVER["REQUEST_URI"]);

    $configurationManager = new ConfigurationManager();
    /** @var Configuration $timezoneObject */
    $timezoneObject = $configurationManager->findOneBy(['conf' => 'timezone']);

    date_default_timezone_set($timezoneObject->getValue());

    $router->load($routes);
    $router->run();
} catch (NotAllowedException $exception) {
    $view = new View('errors.401', 'err');
    $view->assign('errorMessage', $exception->getMessage());
} catch (NotFoundException $exception) {
    http_response_code(404);
    $view = new View('errors.404', 'err');
    $view->assign('errorMessage', $exception->getMessage());
} catch (NotSupportedException $exception) {
    http_response_code(501);
    $view = new View('errors.501', 'err');
    $view->assign('errorMessage', $exception->getMessage());
} catch (RequiredMissingException $exception) {
    http_response_code(501);
    $view = new View('errors.501', 'err');
    $view->assign('errorMessage', $exception->getMessage());
}
