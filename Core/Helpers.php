<?php

namespace Scolabs\Core;

use Exception;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Managers\UserManager;

/**
 * Class helpers
 */
class Helpers
{
    use UrlTrait;

    /**
     * @return bool
     * @throws Exceptions\NotFoundException
     */
    public static function checkInstall()
    {
        if (!filter_var(INSTALLED, FILTER_VALIDATE_BOOLEAN)) {
            if (!isset($_SESSION['install'])) {
                $_SESSION['install'] = [
                    "index" => [
                        "validate" => true,
                        "url" => self::getUrl('Install', 'index')
                    ],
                    "bdd" => [
                        "validate" => false,
                        "url" => self::getUrl('Install', 'bdd')
                    ],
                    "smtp" => [
                        "validate" => false,
                        "url" => self::getUrl('Install', 'smtp')
                    ],
                    "site" => [
                        "validate" => false,
                        "url" => self::getUrl('Install', 'site')
                    ],
                    "admin" => [
                        "validate" => false,
                        "url" => self::getUrl('Install', 'admin')
                    ]
                ];
            }

            $uri = $_SERVER["REQUEST_URI"];
            $url = substr($uri, 1, 7);

            $steps = $_SESSION["install"];
            $action = substr($uri, 9) ? substr($uri, 9) : "index";

            if ($url === "install") {
                if ($action !== "bdd") {
                    if ($action !== "index") {
                        $keys = array_keys($steps);
                        $found_index = array_search($action, $keys);
                        if ($found_index === false || $found_index === 0) {
                            return false;
                        }
                        $prev = $keys[$found_index - 1];
                        if (!$steps[$action]['validate'] && !$steps[$prev]['validate']) {
                            self::redirect($steps[$prev]['url']);
                        }
                    }
                }
            } else {
                self::redirect(self::getUrl('Install', 'index'));
            }
        } else {
            if (strpos(trim($_SERVER['REQUEST_URI']), '/install') === 0) {
                self::redirect(self::getUrl('Auth', 'login'));
            }
        }
    }

    /**
     * @param string $route
     */
    public static function redirect(string $route): void
    {
        header('Location:' . $route);
    }

    /**
     * @param array $array
     * @return array
     */
    public static function cleanArray(array $array): array
    {
        return self::filter_array_recursive($array, function ($item) {
            return htmlspecialchars(htmlentities($item));
        });
    }

    /**
     * @param array $array
     * @param callable $fn
     * @return array
     */
    public static function filter_array_recursive(array $array, callable $fn)
    {
        return array_map(function ($item) use ($fn) {
            if (is_numeric($item) > 0) {
                return intval($item);
            }
            return is_array($item) ? self::filter_array_recursive($item, $fn) : $fn($item);
        }, $array);
    }

    /**
     * @param string $string
     * @return string
     */
    public static function removeAccent(string $string)
    {
        $search = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
        $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');

        return str_replace($search, $replace, $string);
    }

    /**
     * @param int $length
     * @return false|string
     */
    public static function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length / strlen($x)))), 1, $length);
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function generateCsrfToken()
    {
        if (!isset($_SESSION['_csrf'])) {
            $_SESSION['_csrf'] = bin2hex(random_bytes(32));
        }
        return $_SESSION['_csrf'];
    }

    /**
     * @param $firstname
     * @param $lastname
     * @return string
     */
    public static function generateUsername($firstname, $lastname)
    {
        $userManager = new UserManager;
        $count = 0;
        $username = strtolower(Helpers::removeAccent($firstname)) . '.' . strtolower(Helpers::removeAccent($lastname));
        if ($userManager->findOneBy(['username' => $username])->getId()) {
            while ($userManager->findOneBy(['username' => $username])->getId()) {
                $count++;
                $username = strtolower(Helpers::removeAccent($username)) . '.' . strtolower(Helpers::removeAccent($username)) . $count;
            }
        }
        return $username;
    }
}
