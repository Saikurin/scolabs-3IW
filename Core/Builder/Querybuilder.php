<?php


namespace Scolabs\Core\Builder;

use Scolabs\Core\Connection\DBInterface;
use Scolabs\Core\Connection\PDOConnection;
use Scolabs\Core\Connection\PDOResult;

/**
 * Class Querybuilder
 * @package Scolabs\core\builder
 */
class Querybuilder
{
    /**
     * @var array
     */
    private $select;
    /**
     * @var array
     */
    private $from;

    /**
     * @var array
     */
    private $where;

    /**
     * @var array
     */
    private $group = [];

    /**
     * @var array
     */
    private $order = [];

    /**
     * @var string
     */
    private $limit;

    /**
     * @var DBInterface|null
     */
    private $connection;

    /**
     * @var array
     */
    private $parameters;

    /**
     * @var array
     */
    private $join = ['LEFT' => [], 'RIGHT' => []];

    public function __construct(DBInterface $connection = null)
    {
        $this->connection = $connection;
        if ($connection === null) {
            $this->connection = new PDOConnection();
        }
    }

    /**
     * @param string $table
     * @param string|null $alias
     * @return $this
     */
    public function from(string $table, ?string $alias = null): self
    {
        if ($alias) {
            $this->from[$alias] = DB_PREFIXE . $table;
        } else {
            $this->from[] = $table;
        }
        return $this;
    }

    /**
     * @param string ...$fields
     * @return $this
     */
    public function select(string ...$fields): self
    {
        $this->select = $fields;
        return $this;
    }

    /**
     * @param string $conditions
     * @return $this
     */
    public function where(string $conditions): self
    {
        $this->where[] = $conditions;
        return $this;
    }

    /**
     * @param int $start
     * @param int|null $end
     * @return $this
     */
    public function limit(int $start, ?int $end)
    {
        $this->limit = $start . ($end ? ', ' . $end : '');
        return $this;
    }

    /**
     * @param string $table
     * @param string $aliasTarget
     * @param string $fieldSource
     * @param string $fieldTarget
     * @param bool $selected
     * @return $this
     */
    public function leftJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id', $selected = true): self
    {
        array_push($this->join['LEFT'], [
            'condition' => DB_PREFIXE . "$table $aliasTarget ON $fieldSource = $aliasTarget.$fieldTarget",
            'selected' => $selected,
            'alias' => $aliasTarget,
            'table' => DB_PREFIXE . $table
        ]);
        return $this;
    }

    /**
     * @param string $table
     * @param string $aliasTarget
     * @param string $fieldSource
     * @param string $fieldTarget
     * @param bool $selected
     * @return $this
     */
    public function rightJoin(string $table, string $aliasTarget, string $fieldSource = 'id', string $fieldTarget = 'id', $selected = true): self
    {
        array_push($this->join['RIGHT'], [
            'condition' => DB_PREFIXE . "$table $aliasTarget ON $fieldSource = $aliasTarget.$fieldTarget",
            'selected' => $selected,
            'alias' => $aliasTarget,
            'table' => DB_PREFIXE . $table
        ]);
        return $this;
    }

    /**
     * @return string
     */
    public function toSQL()
    {
        $parts = ['SELECT'];
        foreach ($this->from as $alias => $table) {
            $q = "DESCRIBE " . $table;
            $tables[$alias] = $this
                ->connection
                ->query($q)
                ->getColumn();
        }
        foreach ($this->join as $tablesJoined) {
            foreach ($tablesJoined as $table) {
                if (isset($table['selected']) && $table['selected']) {
                    $q = "DESCRIBE " . $table['table'];
                    $tables[$table['alias']] = $this
                        ->connection
                        ->query($q)
                        ->getColumn();
                }
            }
        }

        if ($this->select) {
            $parts[] = join(',', $this->select);
        } else {
            $fields = [];
            foreach ($tables as $alias => $table) {
                foreach ($table as $field) {
                    $fields[] = $alias . "." . $field . " AS " . $alias . "__" . $field;
                }
            }
            $parts[] = implode(', ', $fields);
        }

        $parts[] = 'FROM';
        $parts[] = $this->buildFrom();
        $parts[] = $this->buildJoin();
        if ($this->where) {
            $parts[] = "WHERE";
            $parts[] = "(" . join(') AND (', $this->where) . ")";
        }

        if ($this->group) {
            $parts[] = "GROUP BY";
            $parts[] = $this->group;
        }

        if ($this->order) {
            $parts[] = 'ORDER BY';
            foreach ($this->order as $column => $direction) {
                $parts[] = $column . " " . $direction;
            }
        }

        if ($this->limit) {
            $parts[] = "LIMIT " . $this->limit;
        }

        return join(' ', $parts);
    }

    public function getQuery(): PDOResult
    {
        $query = $this->toSQL();

        return $this->connection->query($query, $this->parameters);
    }

    public function groupBy(string $field)
    {
        $this->group[] = $field;
        return $this;
    }

    public function orderBy(string $field, string $direction)
    {
        $this->order[$field] = $direction;
        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function setParameter(string $key, $value): self
    {
        $this->parameters[$key] = $value;
        return $this;
    }

    /**
     * @return string
     */
    private function buildFrom(): string
    {
        $from = [];
        foreach ($this->from as $key => $value) {
            if (is_string($key)) {
                $from[] = $value . " AS " . $key;
            } else {
                $from[] = $value;
            }
        }
        return join(', ', $from);
    }

    /**
     * @return string
     */
    private function buildJoin(): string
    {
        $query = "";
        if ($this->join) {
            foreach ($this->join as $direction => $conditions) {
                foreach ($conditions as $condition) {
                    if ($condition['selected']) {
                        $query .= $direction . " JOIN " . $condition['condition'] . " ";
                    }
                }
            }
        }
        return trim($query, ' ');
    }
}
