<?php


namespace Scolabs\Core\Connection;

use PDO;
use PDOStatement;
use Scolabs\Core\Model;

/**
 * Class PDOResult
 * @package Scolabs\Core\Connection
 */
class PDOResult implements ResultInterface
{

    /**
     * @var PDOStatement
     */
    protected $statement;

    /**
     * PDOResult constructor.
     * @param PDOStatement $statement
     */
    public function __construct(PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    /**
     * @param string|null $class
     * @return array
     */
    public function getArrayResult(string $class = null): array
    {
        $result =  $this->statement->fetchAll(PDO::FETCH_ASSOC);

        if ($class) {
            $results = [];
            foreach ($result as $key => $value) {
                array_push($results, (new $class())->hydrate($value));
            }
            return $results;
        }
        return $result;
    }

    /**
     * @param string|null $class
     * @return Model|null
     */
    public function getOneOrNullResult(string $class = null): ?Model
    {
        $result =  $this->statement->fetch(PDO::FETCH_ASSOC);

        if ($class) {
            return (new $class())->hydrate($result);
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getValueResult()
    {
        return $this->statement->fetchColumn();
    }

    /**
     * @param int $column
     * @return mixed
     */
    public function getColumn(int $column = null)
    {
        if (!is_null($column)) {
            return $this->statement->fetchAll(PDO::FETCH_COLUMN, $column);
        }
        return $this->statement->fetchAll(PDO::FETCH_COLUMN);
    }
}
