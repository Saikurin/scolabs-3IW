<?php

namespace Scolabs\Core\Connection;

/**
 * Interface DBInterface
 * @package Scolabs\Core\Connection
 */
interface DBInterface
{
    /**
     * @return mixed
     */
    public function connect();

    /**
     * @param string $query
     * @param array|null $parameters
     * @return mixed
     */
    public function query(string $query, array $parameters = null);
}
