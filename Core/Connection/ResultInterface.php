<?php

namespace Scolabs\Core\Connection;

/**
 * Interface ResultInterface
 * @package Scolabs\Core\Connection
 */
interface ResultInterface
{
    /**
     * @return mixed
     */
    public function getArrayResult();

    /**
     * @return mixed
     */
    public function getOneOrNullResult();

    /**
     * @return mixed
     */
    public function getValueResult();

    /**
     * @param int $column
     * @return mixed
     */
    public function getColumn(int $column);
}
