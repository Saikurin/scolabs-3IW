<?php

namespace Scolabs\Core\Connection;

use PDO;
use PDOException;

/**
 * Class PDOConnection
 * @package Scolabs\Core\Connection
 */
class PDOConnection implements DBInterface
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * PDOConnection constructor.
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * Connect to db with PDO
     */
    public function connect()
    {
        try {
            $this->pdo = new PDO(DB_DRIVER.":host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PWD);
        } catch (\Throwable $e) {
            die("Erreur SQL : ".$e->getMessage());
        }
    }

    /**
     * @param string $query
     * @param array|null $parameters
     * @return PDOResult
     */
    public function query(string $query, array $parameters = null)
    {
        if ($parameters) {
            $queryPrepared = $this->pdo->prepare($query);
            try {
                $queryPrepared->execute($parameters);
            } catch (PDOException $p) {
                $p->getMessage();
            }

            return new PDOResult($queryPrepared);
        } else {
            $queryPrepared = $this->pdo->prepare($query);
            $queryPrepared->execute();

            return new PDOResult($queryPrepared);
        }
    }

    public function lastInsertId(){
        return $this->pdo->lastInsertId();
    }
}
