<?php

namespace Scolabs\Core;

use Scolabs\Core\Connection\DBInterface;
use Scolabs\Core\Builder\Querybuilder;
use Scolabs\Core\Connection\PDOConnection;

/**
 * Class DB
 */
class Manager
{
    /**
     * @var string
     */
    protected $table;
    /**
     * @var DBInterface
     */
    protected $connection;

    /**
     * @var string
     */
    protected $class;

    /**
     * DB constructor.
     * @param string $class
     * @param string $table
     * @param DBInterface|null $connection
     */
    public function __construct(string $class, string $table, DBInterface $connection = null)
    {
        $this->class = $class;
        $this->table = $table;

        $this->connection = $connection;
        if ($connection === null) {
            $this->connection = new PDOConnection();
        }
    }


    /**
     * @param $objectToSave
     * @return void
     */
    public function save(Model $objectToSave)
    {
        $objectArray = $objectToSave->__toArray();
        $relations = $objectToSave->initRelation();

        if ($relations) {
            foreach ($objectArray as $key => $value) {
                if (is_object($value)) {
                    unset($objectArray[$key]);

                    $key = key(array_filter($relations, function ($relation) use ($key) {
                        return $relation['name'] === $key;
                    }));

                    $objectArray[$key] = $value->getId();
                }
            }
        }

        $columnsData = array_values($objectArray);
        $columns = array_keys($objectArray);

        $params = array_combine(
            array_map(function ($k) {
                return ':' . $k;
            }, array_keys($objectArray)),
            $objectArray
        );
        if (!is_numeric($objectToSave->getId())) {
            array_shift($columns);
            array_shift($params);

            $sql = "INSERT INTO " . DB_PREFIXE . $this->table . " (" . implode(",", $columns) . ") VALUES (:" . implode(",:", $columns) . ");";
        } else {
            foreach ($columns as $column) {
                $sqlUpdate[] = $column . "=:" . $column;
            }

            $sql = "UPDATE " . DB_PREFIXE . $this->table . " SET " . implode(",", $sqlUpdate) . " WHERE id=:id;";
        }
        $this->connection->query($sql, $params);
    }


    /**
     * @param int $id
     * @return Model|null
     */
    public function find(int $id): ?Model
    {
        return $this->getQueryBuilder()
            ->where('id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult($this->class);
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->getQueryBuilder()
            ->getQuery()
            ->getArrayResult($this->class);
    }

    /**
     * @param array $params
     * @param array|null $order
     * @param string $class
     * @return mixed
     */
    public function findBy(array $params, array $order = null, string $class = '')
    {
        $queryBuilder = $this->getQueryBuilder();

        foreach ($params as $key => $value) {

            if (is_string($value)) {
                $queryBuilder->where($key . " LIKE :" . $key);
            } else {
                $queryBuilder->where($key . " = :" . $key);
            }

            $queryBuilder->setParameter($key, $value);

            $params[":$key"] = $value;
            unset($params[$key]);
        }

        if ($order) {
            $queryBuilder->orderBy(key($order), $order[key($order)]);
        }

        return $queryBuilder->getQuery()->getArrayResult($class);
    }

    /**
     * @param array $params
     * @param array|null $order
     * @return Model
     */
    public function findOneBy(array $params, array $order = null): Model
    {
        $queryBuilder = $this->getQueryBuilder();

        foreach ($params as $key => $value) {
            if (is_string($value)) {
                $queryBuilder->where($key . " LIKE :" . $key);
            } else {
                $queryBuilder->where($key . " = :" . $key);
            }

            $queryBuilder->setParameter($key, $value);
            $params[":$key"] = $value;
            unset($params[$key]);
        }

        if ($order) {
            $queryBuilder->orderBy(key($order), $order[key($order)]);
        }

        return $queryBuilder->getQuery()->getOneOrNullResult($this->class);
    }

    /**
     * @param array $params
     * @return int
     */
    public function count(array $params = []): int
    {
        $queryBuilder = $this->getQueryBuilder();

        foreach ($params as $key => $value) {
            if (array_search($key, array_keys($params)) === 0) {
                if (is_string($value)) {
                    $queryBuilder->where("$key LIKE :$key");
                } else {
                    $queryBuilder->where("$key = :$key");
                }
            } else {
                if (is_string($value)) {
                    $queryBuilder->where("$key LIKE :$key");
                } else {
                    $queryBuilder->where("$key = :$key");
                }
            }
            $queryBuilder->setParameter($key, $value);
        }

        return count($queryBuilder->getQuery()->getArrayResult());
    }


    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $sql = "DELETE FROM " . DB_PREFIXE . "$this->table WHERE id = :id";
        $this->connection->query($sql, ['id' => $id]);
        return true;
    }

    /**
     * @return Querybuilder
     */
    protected function getQueryBuilder(): Querybuilder
    {
        $pathOfClass = explode('\\', $this->class);

        $queryBuilder = new Querybuilder();
        $queryBuilder->from(lcfirst($this->table), array_pop($pathOfClass));

        return $queryBuilder;
    }

    /**
     * @return integer
     */
    public function lastInsertId(): int
    {
        return $this->connection->lastInsertId();
    }

}
