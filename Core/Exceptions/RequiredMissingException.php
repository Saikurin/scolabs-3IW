<?php


namespace Scolabs\core\exceptions;

class RequiredMissingException extends \Exception
{

    /**
     * RequiredMissingException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, $code = 404)
    {
        parent::__construct($message, $code);
    }
}
