<?php


namespace Scolabs\Core\Exceptions;

class NotSupportedException extends \Exception
{
    /**
     * NotSupportedException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, $code = 404)
    {
        parent::__construct($message, $code);
    }
}
