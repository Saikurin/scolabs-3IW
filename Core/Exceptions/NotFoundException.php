<?php

namespace Scolabs\Core\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, $code = 404)
    {
        parent::__construct($message, $code);
    }
}