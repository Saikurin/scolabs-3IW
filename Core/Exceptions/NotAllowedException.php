<?php


namespace Scolabs\Core\Exceptions;

use Exception;

class NotAllowedException extends Exception
{
    /**
     * NotAllowedException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code = 405)
    {
        parent::__construct($message, $code);
    }
}
