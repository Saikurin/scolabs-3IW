<?php

namespace Scolabs\Core;

/**
 * Class FileManager
 */
class FileManager
{

    /**
     * @var string
     */
    private $upload_function = 'move_uploaded_file';

    /**
     * Array with the information obtained from the variable $_FILES
     *
     * @var array
     */
    private $file_array = [];

    /**
     * If the file you are trying to upload already exists it will be
     * overwritten if you set the variable to true
     *
     * @var bool
     */
    private $overwrite_file = false;

    /**
     * Input element
     * Example :
     *  <input type='file' name='file'/>
     * Result :
     *  FileManager::$input = file
     *
     * @var string
     */
    private $input;

    /**
     * Path output
     *
     * @var string
     */
    private $destination_directory;

    /**
     * Output filename
     *
     * @var string
     */
    private $filename;

    /**
     * Max file size
     *
     * @var float
     */
    private $max_file_size = 0.0;

    /**
     * List of allowed mime types
     *
     * @var array
     */
    private $allowed_mime_types = [];

    /**
     * Callbacks
     *
     * @var array
     */
    private $callback = ['before' => null, 'after' => null];

    /**
     * File object
     *
     * @var object
     */
    private $file;

    /**
     * @var string
     */
    private $destinationFilename;

    /**
     * Helping mime_types
     *
     * @var array
     */
    private $mime_helping = [
        'text' => [
            'text/plain'
        ],
        'image' => [
            'image/jpeg',
            'image/jpg',
            'image/pjpeg',
            'image/png',
            'image/gif',
        ],
        'document' => [
            'application/msword',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'application/vnd.ms-powerpoint',
            'application/vnd.ms-excel',
            'application/vnd.oasis.opendocument.spreadsheet',
            'application/vnd.oasis.opendocument.presentation',
        ],
        'video' => [
            'video/3gpp',
            'video/3gpp',
            'video/x-msvideo',
            'video/avi',
            'video/mpeg4',
            'video/mp4',
            'video/mpeg',
            'video/mpg',
            'video/quicktime',
            'video/x-sgi-movie',
            'video/x-ms-wmv',
            'video/x-flv',
        ]
    ];

    /**
     * @var bool
     */
    private $allowOverwritting = false;

    /**
     * @var string
     */
    private $originPath;

    /**
     * FileManager constructor.
     */
    public function __construct()
    {
        $this->file = [
            'status' => false, // True: success upload
            'mime' => '',
            'filename' => '',
            'original' => '',
            'size' => 0,
            'sizeFormatted' => '0B',
            'destination' => './',
            'allowed_mime_types' => [],
            'log' => [],
            'error' => 0
        ];

        $this->destination_directory = getcwd() . DIRECTORY_SEPARATOR . DATA_DIR . DIRECTORY_SEPARATOR . 'uploads';

        $this->originPath = getcwd();

        if (isset($_FILES) && is_array($_FILES)) {
            $this->file_array = $_FILES;
        } elseif (isset($HTTP_POST_FILES) && is_array($HTTP_POST_FILES)) {
            $this->file_array = $HTTP_POST_FILES;
        }
    }

    /**
     *   Set input.
     *    If you have $_FILES["file"], you must use the key "file"
     *    Example:
     *        $object->setInput("file");
     *
     * @param $input
     * @return bool
     */
    public function setInput($input)
    {
        if (!empty($input) && (is_string($input) || is_numeric($input))) {
            $this->input = $input;
            return true;
        }
        return false;
    }

    /**
     * Set new filename
     * Example:
     *  Fileupload::setFilename("new file.txt")
     * @param $filename
     * @return bool
     */
    public function setFilename($filename)
    {
        if ($this->isFilename($filename)) {
            $this->filename = $filename;
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function setAutoFilename()
    {
        $this->filename = sha1(mt_rand(1, 9999) . uniqid());
        $this->filename .= time();
        $this->filename .= ".%s";
        return true;
    }

    /**
     * @param $file_size
     * @return bool
     */
    public function setMaxFileSize($file_size)
    {
        $file_size = $this->sizeInBytes($file_size);
        if (is_numeric($file_size) && $file_size > -1) {
            $size = $this->sizeInBytes(ini_get('upload_max_filesize'));

            // Calculate difference
            if ($size < $file_size) {
                // If upload_max_filesize is greatter than $file_size
                return false;
            }

            $this->max_file_size = $file_size;
            return true;
        }
        return false;
    }

    /**
     * @param array $mimes
     * @return bool
     */
    public function setAllowedMimeTypes(array $mimes)
    {
        if (count($mimes) > 0) {
            array_map([$this, 'setAllowMimeType'], $mimes);
            return true;
        }

        return false;
    }

    /**
     * @param callable $callback
     * @return bool
     */
    public function setCallbackInput(callable $callback)
    {
        if (is_callable($callback, false)) {
            $this->callback['input'] = $callback;
            return true;
        }
        return false;
    }

    /**
     * @param callable $callback
     * @return bool
     */
    public function setCallbackOutput(callable $callback)
    {
        if (is_callable($callback, false)) {
            $this->callback['output'] = $callback;
            return true;
        }
        return false;
    }

    /**
     * @param $mime
     * @return bool
     */
    public function setAllowMimeType($mime)
    {
        if (!empty($mime) and is_string($mime)) {
            if (preg_match("#^[-\w+]+/[-\w+.]+$#", $mime)) {
                $this->allowed_mime_types[] = strtolower($mime);
                $this->file["allowed_mime_types"][] = strtolower($mime);
                return true;
            } else {
                return $this->setMimeHelping($mime);
            }
        }
        return false;
    }

    /**
     * @param string $mime
     * @return bool
     */
    public function setMimeHelping(string $mime)
    {
        if (!empty($mime) && is_string($mime)) {
            if (array_key_exists($mime, $this->mime_helping)) {
                return $this->setAllowedMimeTypes($this->mime_helping[$mime]);
            }
        }
        return false;
    }

    /**
     *    Set function to upload file
     *    Examples:
     *        1.- FileUpload::setUploadFunction("move_uploaded_file");
     *        2.- FileUpload::setUploadFunction("copy");
     *
     * @param $function
     * @return bool
     */
    public function setUploadFunction($function)
    {
        if (!empty($function) && (is_array($function) || is_string($function))) {
            if (is_callable($function) && function_exists($function)) {
                $this->upload_function = $function;
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function clearAllowedMimeTypes()
    {
        $this->allowed_mime_types = [];
        $this->file['allowed_mime_types'] = [];
        return true;
    }

    /**
     * @param string $destinationDirectory
     * @param bool $create_if_not_exist
     * @return bool
     */
    public function setDestinationDirectory(string $destinationDirectory, bool $create_if_not_exist)
    {
        $destinationDirectory = realpath($destinationDirectory);
        if (substr($destinationDirectory, -1) !== DIRECTORY_SEPARATOR) {
            $destinationDirectory .= DIRECTORY_SEPARATOR;
        }

        if ($this->isDirPath($destinationDirectory)) {
            if ($this->dirExists($destinationDirectory)) {
                $this->destination_directory = $destinationDirectory;
                if (substr($destinationDirectory, -1) !== DIRECTORY_SEPARATOR) {
                    $this->destination_directory .= DIRECTORY_SEPARATOR;
                }
                chdir($destinationDirectory);
                return true;
            } elseif ($create_if_not_exist === true) {
                if (mkdir($destinationDirectory, 0777, true)) {
                    if ($this->dirExists($destinationDirectory)) {
                        $this->destination_directory = $destinationDirectory;
                        if (substr($this->destination_directory, -1) !== DIRECTORY_SEPARATOR) {
                            $this->destination_directory .= DIRECTORY_SEPARATOR;
                        }
                        chdir($destinationDirectory);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param $fileDestination
     * @return bool
     */
    public function fileExists($fileDestination)
    {
        if ($this->isFilename($fileDestination)) {
            return (file_exists($fileDestination) && is_file($fileDestination));
        }
        return false;
    }

    /**
     * @param $path
     * @return bool
     */
    public function dirExists($path)
    {
        if ($this->isDirPath($path)) {
            return (file_exists($path) && is_dir($path));
        }
        return false;
    }

    /**
     * @param $filename
     * @return bool
     */
    public function isFilename($filename)
    {
        $filename = basename($filename);
        return (!empty($filename) && (is_string($filename) || is_numeric($filename)));
    }

    /**
     * @param $mime
     * @return bool
     */
    public function checkMimeType($mime)
    {
        if (count($this->allowed_mime_types) === 0) {
            return true;
        }
        return in_array(strtolower($mime), $this->allowed_mime_types);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->file['status'];
    }

    public function isDirPath($path)
    {
        if (!empty($path) && (is_string($path) || is_numeric($path))) {
            if (DIRECTORY_SEPARATOR === "/") {
                return (preg_match('/^[^^?"<>|:]*$/', $path) === 1);
            } else {
                return (preg_match('/^[^^?\"<>|:]*$/', $path) === 1);
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function allowOverwriting()
    {
        $this->allowOverwritting = true;
        return true;
    }

    /**
     * @return object
     */
    public function getInfo()
    {
        return (object)$this->file;
    }


    /**
     * @return bool|mixed
     */
    public function save()
    {
        if (count($this->file_array) > 0) {
            if (array_key_exists($this->input, $this->file_array)) {
                // set original filename if not have a new name
                if (empty($this->filename)) {
                    $filenameExploded = explode('.', $this->file_array[$this->input]['name']);
                    $this->filename = $this->destinationFilename . $this->file_array[$this->input]['name'];
                }

                // Replace %s for extension in filename
                // Before: /[\w\d]*(.[\d\w]+)$/i
                // After: /^[\s[:alnum:]\-\_\.]*\.([\d\w]+)$/iu
                // Support unicode(utf-8) characters
                $extension = preg_replace(
                    "/^[\p{L}\d\s\-_.()]*\.([\d\w]+)$/iu",
                    '$1',
                    $this->file_array[$this->input]["name"]
                );
                $this->filename = sprintf($this->filename, $extension);

                // set file info
                $this->file["mime"] = $this->file_array[$this->input]["type"];
                $this->file["tmp"] = $this->file_array[$this->input]["tmp_name"];
                $this->file["original"] = $this->file_array[$this->input]["name"];
                $this->file["size"] = $this->file_array[$this->input]["size"];
                $this->file["sizeFormated"] = $this->sizeFormat($this->file["size"]);
                $this->file["destination"] = $this->filename;
                $this->file["filename"] = $this->getDestinationDirectory() . DIRECTORY_SEPARATOR . $this->filename;
                $this->file["error"] = $this->file_array[$this->input]["error"];


                // Check if exists file
                if ($this->fileExists($this->destination_directory . DIRECTORY_SEPARATOR . $this->filename)) {
                    // Check if overwrite file
                    if ($this->allowOverwritting === false) {
                        return false;
                    }
                }


                // Execute input callback
                if (!empty($this->callback["input"])) {
                    call_user_func($this->callback["input"], (object)$this->file);
                }

                // Check mime type
                if (!$this->checkMimeType($this->file["mime"])) {
                    return false;
                }
                // Check file size
                if ($this->max_file_size > 0) {
                    if ($this->max_file_size < $this->file["size"]) {
                        return false;
                    }
                }
                // Copy tmp file to destination and change status

                chdir($this->destination_directory);

                $this->file["status"] = call_user_func_array(
                    $this->upload_function,
                    array(
                        $this->file_array[$this->input]["tmp_name"],
                        $this->getDestinationDirectory() . DIRECTORY_SEPARATOR . $this->filename
                    )
                );


                // Execute output callback
                if (!empty($this->callback["output"])) {
                    call_user_func($this->callback["output"], (object)$this->file);
                }

                chdir($this->originPath);

                return $this->file["status"];
            }
        }
        return false;
    }

    /**
     * @param $size
     * @param int $precision
     * @return string
     */
    public function sizeFormat($size, $precision = 2)
    {
        $base = log($size) / log(1024);
        $suffixes = array('B', 'K', 'M', 'G');
        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

    /**
     * @param $size
     * @return float|int
     */
    public function sizeInBytes($size)
    {
        $unit = "B";
        $units = array("B" => 0, "K" => 1, "M" => 2, "G" => 3);
        $matches = array();
        preg_match("/(?<size>[\d\.]+)\s*(?<unit>b|k|m|g)?/i", $size, $matches);
        if (array_key_exists("unit", $matches)) {
            $unit = strtoupper($matches["unit"]);
        }
        return (floatval($matches["size"]) * pow(1024, $units[$unit]));
    }

    /*
     * GETTERS AND SETTERS
     */

    /**
     * @return array
     */
    public function getFileArray(): array
    {
        return $this->file_array;
    }

    /**
     * @param array $file_array
     */
    public function setFileArray(array $file_array): void
    {
        $this->file_array = $file_array;
    }

    /**
     * @return bool
     */
    public function isOverwriteFile(): bool
    {
        return $this->overwrite_file;
    }

    /**
     * @param bool $overwrite_file
     */
    public function setOverwriteFile(bool $overwrite_file): void
    {
        $this->overwrite_file = $overwrite_file;
    }

    /**
     * @return array
     */
    public function getCallback(): array
    {
        return $this->callback;
    }

    /**
     * @param array $callback
     */
    public function setCallback(array $callback): void
    {
        $this->callback = $callback;
    }

    /**
     * @return object
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param $file
     */
    public function setFile($file): void
    {
        $this->file = $file;
    }

    /**
     * @return bool
     */
    public function isAllowOverwritting(): bool
    {
        return $this->allowOverwritting;
    }

    /**
     * @param bool $allowOverwritting
     */
    public function setAllowOverwritting(bool $allowOverwritting): void
    {
        $this->allowOverwritting = $allowOverwritting;
    }

    /**
     * @return string
     */
    public function getUploadFunction(): string
    {
        return $this->upload_function;
    }

    /**
     * @return string
     */
    public function getInput(): string
    {
        return $this->input;
    }

    /**
     * @return string
     */
    public function getDestinationDirectory(): string
    {
        return $this->destination_directory;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @return float
     */
    public function getMaxFileSize(): float
    {
        return $this->max_file_size;
    }

    /**
     * @return array
     */
    public function getAllowedMimeTypes(): array
    {
        return $this->allowed_mime_types;
    }

    /**
     * @return array
     */
    public function getMimeHelping(): array
    {
        return $this->mime_helping;
    }

    /**
     * @return string
     */
    public function getDestinationFilename(): string
    {
        return $this->destinationFilename;
    }

    /**
     * @param string $destinationFilename
     */
    public function setDestinationFilename(string $destinationFilename): void
    {
        $this->destinationFilename = $destinationFilename;
    }

    public function __destruct()
    {
        chdir($this->originPath);
    }
}
