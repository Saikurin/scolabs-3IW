<?php


namespace Scolabs\Core\Command;

interface CommandInterface
{
    /**
     * @return mixed
     */
    public function execute();

    /**
     * @param array $args
     * @return mixed
     */
    public function setArgs(array $args);
}
