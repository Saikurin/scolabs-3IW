<?php

namespace Scolabs\Core\Command;

class Receiver
{
    /**
     * @var bool
     */
    private $enableDate = false;

    /**
     * @var array
     */
    private $output = [];

    /**
     * @param string $string
     */
    public function write(string $string)
    {
        if ($this->enableDate) {
            $string = '[' . date('Y-m-d H:i:s') . '] ' . $string;
        }

        $this->output[] = $string;
    }

    /**
     * @return string
     */
    public function getOutput()
    {
        return join("\n", $this->output) . "\n";
    }
    
    public function printOutput()
    {
        $out = fopen("php://output", "w");
        fputs($out, $this->getOutput());
        fclose($out);
    }

    /**
     * @return Receiver
     */
    public function enableDate()
    {
        $this->enableDate = true;

        return $this;
    }

    /**
     * @return Receiver
     */
    public function disableDate()
    {
        $this->enableDate = false;

        return $this;
    }
}
