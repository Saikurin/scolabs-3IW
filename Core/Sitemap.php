<?php


namespace Scolabs\core;

use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use SimpleXMLElement;

/**
 * Class Sitemap
 * @package Scolabs\core
 */
class Sitemap
{

    use UrlTrait, RequestTrait;

    private $urls = [];

    public function __construct()
    {
        $routes = yaml_parse_file(DIRECTORY_ROOT . 'routes.yml');

        $routesFiltered = array_filter($routes, function ($element) {
            return !isset($element['params']) && in_array('GET', $element['method']) && $element['referenced'];
        });

        foreach ($routesFiltered as $route => $value) {
            $this->add($route);
        }
    }

    /**
     * @param string $loc Without end slash
     * @param string $changefreq never by default
     */
    public function add(string $loc, string $changefreq = 'never')
    {
        $startURI = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'];

        $this->urls[] = [
            'loc' => $startURI . $loc,
            'changefreq' => $changefreq
        ];
    }

    public function generate()
    {
        $xml_data = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>');
        $this->array_to_xml($this->urls, $xml_data);
        $xml_data->asXML(DIRECTORY_ROOT . 'sitemap.xml');
    }

    /**
     * @param $data
     * @param $xml_data
     */
    private function array_to_xml($data, &$xml_data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $key = 'url';
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }
}
