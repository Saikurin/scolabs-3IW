<?php


namespace Scolabs\Core;

use BadMethodCallException;
use Exception;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\Traits\RequestTrait;

class FormBuilder
{

    use RequestTrait;

    const INPUT = 'input';
    const TEXTAREA = 'textarea';
    const SELECT = 'select';

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $method;

    /**
     * @var bool
     */
    private $acceptFiles;

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var array
     */
    private $errors = [];

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * FormBuilder constructor.
     * @param string $method
     * @param string $url
     * @param bool $withFile
     * @throws NotSupportedException
     * @throws RequiredMissingException
     * @throws Exception
     */
    public function __construct(string $method, string $url, bool $withFile = false)
    {
        if (in_array($method, ['POST', 'PUT', 'GET', 'PATCH', 'DELETE', 'HEAD'])) {
            $this->method = $method;
            $this->url = $url;
            $this->acceptFiles = $withFile;
            $this->addField('input', [
                'type' => 'hidden',
                'name' => '_csrf',
                'id' => '_csrf',
                'value' => Helpers::generateCsrfToken()
            ]);
        } else {
            throw new BadMethodCallException("La méthode demandé n'est pas autorisé");
        }
    }

    /**
     * @param string $element
     * @param array $options
     * @param array $validators
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function addField(string $element, array $options, array $validators = [])
    {
        if (isset($options['name']) && isset($options['id']) || ($element === self::INPUT && isset($option['type']))) {
            $this->fields[$options['name']] = [
                "HTMLElement" => $element,
                "options" => $options,
                "validators" => $validators
            ];
            switch ($element) {
                case self::INPUT:
                    $this->fields[$options['name']]['render'] = "<input placeholder='" . ($options['placeholder'] ?? '') . "' type='" . $options['type'] . "' name='" . ($options['name'] ?? "") . "' id='" . ($options['id'] ?? "") . "' class='" . ($options['class'] ?? "") . "' value='" . (isset($options['value']) ? $options['value'] : "") . "' />";
                    break;
                case self::TEXTAREA:
                    $this->fields[$options['name']]['render'] = "<textarea placeholder='" . ($options['placeholder'] ?? '') . "' name='" . ($options['name'] ?? "") . "' id='" . ($options['id'] ?? "") . "' rows='" . ($options['rows'] ?? "") . "' cols='" . ($options['cols'] ?? "") . "'>" . ($options['value'] ?? "") . "</textarea>";
                    break;
                case self::SELECT:
                    $this->fields[$options['name']]['render'] = "<select class='" . ($options['class'] ?? '') . "' name='" . $options['name'] . "' id='" . $options['id'] . "' " . ($options['multiple'] ? "multiple='multiple'" : "") . " >";
                    foreach ($this->fields[$options['name']]['options']['values'] as $option) {
                        $this->fields[$options['name']]['render'] .= "<option " . ($option['selected'] ? "selected" : "") . " value='" . $option['value'] . "' >" . $option['text'] . "</option>";
                    }
                    $this->fields[$options['name']]['render'] .= "</select>";
                    break;
                default:
                    throw new NotSupportedException("L'element HTML n'est pas supporté");
            }
            if (isset($options['label'])) {
                $this->fields[$options['name']]['label_render'] = "<label  class='" . ($options['label']['class'] ?? '') . "' for='" . $options['id'] . "'>" . $options['label']['value'] . "</label>";
            }
        } else {
            throw new RequiredMissingException("Les éléments requis du formulaires ne sont pas présents");
        }
    }

    /**
     * @return string
     * @throws NotSupportedException
     */
    public function render(): string
    {
        $form = "<form method='" . $this->method . "' action='" . $this->url . "' " . ($this->acceptFiles ? 'enctype="multipart/form-data"' : '') . ">";
        foreach ($this->fields as $field) {
            if (isset($this->errors[$field['options']['name']])) {
                $form .= "<div class='alert alert-danger'>" . $this->errors[$field['options']['name']] . "</div>";
            }
            if (isset($field['options']['label'])) {
                $form .= "\n\t <label for='" . ($field['options']['id']) . "'>" . $field['options']['label']['value'] . "</label>";
            }
            switch ($field['HTMLElement']) {
                case self::INPUT:
                    $form .= "\n\t<input placeholder='" . ($field['options']['placeholder'] ?? '') . "' type='" . $field['options']['type'] . "' name='" . ($field['options']['name'] ?? "") . "' id='" . ($field['options']['id'] ?? "") . "' class='" . ($field['options']['class'] ?? "") . "' value='" . ($field['options']['value'] ?? "") . "' />";
                    break;
                case self::TEXTAREA:
                    $form .= "\n\t<textarea placeholder='" . ($field['options']['placeholder'] ?? '') . "' name='" . ($field['options']['name'] ?? "") . "' id='" . ($field['options']['id'] ?? "") . "' rows='" . ($field['options']['rows'] ?? "") . "' cols='" . ($field['options']['cols'] ?? "") . "'>" . ($field['options']['value'] ?? "") . "</textarea>";
                    break;
                case self::SELECT:
                    $form .= "\n\t<select class='" . ($field['options']['class'] ?? '') . "' name='" . $field['options']['name'] . "' id='" . $field['options']['id'] . "' " . (isset($field['multiple']) ? "multiple='multiple'" : "") . " >";
                    foreach ($field['options']['values'] as $option) {
                        $form .= "\n\t\t<option " . ($option['selected'] ? "selected" : "") . " value='" . $option['value'] . "' >" . $option['text'] . "</option>";
                    }
                    $form .= "\n\t</select>";
                    break;
                default:
                    throw new NotSupportedException("L'element HTML n'est pas supporté par Scolabs");
            }
        }
        $form .= "</form>";
        return $form;
    }

    /**
     * @param string $style
     * @throws Exception
     */
    public function startForm(string $style = "")
    {
        echo "<form " . ('style="' . $style . '"' ?? '') . " method='" . $this->method . "' action='" . $this->url . "' " . ($this->acceptFiles ? "enctype='multipart/form-data'>" : '>');
        echo '<input type="hidden" name="_csrf" value="'. Helpers::generateCsrfToken() .'"/>';
    }

    public function endForm()
    {
        echo "</form>";
    }

    /**
     * @param string $field
     */
    public function field(string $field)
    {
        echo $this->fields[$field]['render'];
    }

    /**
     * @param string $field
     */
    public function label(string $field)
    {
        echo $this->fields[$field]['label_render'];
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        if($_SESSION['_csrf'] !== $this->getFromPost('_csrf')) {
            http_response_code(500);
            return false;
        }
        $values = $GLOBALS["_" . $this->method];
        foreach ($this->fields as $field) {
            if (isset($field['options']['type'])) {
                if ($field['options']['type'] === 'email') {
                    if (!filter_var($values[$field['options']['name']])) {
                        $this->errors[$field['options']['name']] = "L'adresse email saisie n'est pas correcte";
                    }
                } else {
                    if (count($field['validators']) > 0) {
                        if (!preg_match('/' . $field['validators']['regex'] . '/', $values[$field['options']['name']])) {
                            $this->errors[$field['options']['name']] = $field['validators']['error'];
                        }
                    }
                }
            }
        }
        return count($this->errors) === 0;
    }
}
