<?php


namespace Scolabs\Core;

class Request
{

    /**
     * @param string $url
     * @param array $params
     * @param bool $ajaxCall
     * @return false|string
     */
    public static function get(string $url, array $params, bool $ajaxCall = true)
    {
        $paramsString = http_build_query($params);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url . "?" . $paramsString);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);

        http_response_code(200);

        if ($ajaxCall) {
            header('Content-Type: application/json');
            return $output;
        } else {
            return json_decode($output);
        }
    }
}
