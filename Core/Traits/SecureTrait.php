<?php


namespace Scolabs\Core\Traits;

use Scolabs\Core\EncryptDecrypt;

trait SecureTrait
{

    /**
     * @param $string
     * @return string
     */
    public function encrypt(string $string) : string
    {
        $crypt = new EncryptDecrypt("CKXH2U9RPY3EFD70TLS1ZG4N8WQBOVI6AMJ5");
        return $crypt->encrypt($string);
    }

    /**
     * @param string $token
     * @return string
     */
    public function decrypt(string $token) : string
    {
        $crypt = new EncryptDecrypt("CKXH2U9RPY3EFD70TLS1ZG4N8WQBOVI6AMJ5");
        return $crypt->decrypt($token);
    }
}
