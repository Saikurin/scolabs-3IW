<?php

namespace Scolabs\Core\Traits;

use Scolabs\Core\Exceptions\NotFoundException;

trait UrlTrait
{

    /**
     * @param string $controller
     * @param string $action
     * @param array $params
     * @return int|string|string[]
     * @throws NotFoundException
     */
    public static function getUrl(string $controller, string $action, array $params = [])
    {
        $listOfRoutes = yaml_parse_file("routes.yml");

        foreach ($listOfRoutes as $url => $route) {
            if ($route["controller"] == $controller && $route["action"] == $action) {
                if (isset($params)) {
                    foreach ($params as $param => $value) {
                        $url = str_replace(':' . $param, $value, $url);
                    }
                }
                return $url;
            }
        }

        throw new NotFoundException("La génération de l'URL est impossible");
    }

}