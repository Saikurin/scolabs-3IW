<?php


namespace Scolabs\Core\Traits;

use Scolabs\Core\Helpers;

trait RequestTrait
{
    /**
     * @return bool
     */
    public function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === "POST";
    }

    public function isDelete()
    {
        return $_SERVER['REQUEST_METHOD'] === "DELETE";
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }

    /**
     * @param $field
     * @return array|string
     */
    public function getFromPost(string $field = "")
    {
        if ($field !== "") {
            if (is_array($_POST[$field])) {
                return $_POST[$field];
            }
            return htmlspecialchars(trim(htmlentities($_POST[$field], ENT_QUOTES, "UTF-8")));
        } else {
            return Helpers::cleanArray($_POST);
        }
    }

    public function getFromGet($field = "")
    {
        if ($field !== "") {
            return htmlspecialchars(trim(htmlentities($_GET[$field])));
        } else {
            return Helpers::cleanArray($_GET);
        }
    }

    /**
     * @return bool
     */
    public function isHttps()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
    }
}
