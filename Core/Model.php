<?php

namespace Scolabs\Core;

use JsonSerializable;

class Model implements JsonSerializable
{
    /**
     * @return array
     */
    public function __toArray(): array
    {
        return get_object_vars($this);
    }

    /**
     * @param $row
     * @param string|null $relationClassName
     * @return mixed
     */
    public function hydrate($row, string $relationClassName = null)
    {
        $className = get_class($this);
        $articleObj = new $className();
        if (false !== $row) {
            foreach ($row as $key => $value) {
                $path = explode("\\", $className);
                $classNameWithoutNamespace = strtolower(array_pop($path));
                if ($relationClassName) {
                    $classNameWithoutNamespace = $relationClassName;
                }
                if (false !== strpos($key, ucfirst($classNameWithoutNamespace))) {
                    $key = str_replace(ucfirst($classNameWithoutNamespace) . '__', '', $key);
                    $method = 'set' . str_replace("_", '', ucwords(ucfirst($key), "_"));
                    if ($relation = $articleObj->getRelation($key)) {
                        $tmp = new $relation['class']();
                        $tmp = $tmp->hydrate($row, $relation['name']);
                        $tmp->setId($value);
                        $setter = $relation['setter'];
                        $articleObj->$setter($tmp);
                    } else {
                        if (method_exists($articleObj, $method)) {
                            $articleObj->$method($value);
                        }
                    }
                }
            }
        }
        return $articleObj;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return $this->__toArray();
    }

    /**
     * @param string $key
     * @return array|null
     */
    public function getRelation(string $key): ?array
    {
        $relations = $this->initRelation();

        if (isset($relations[$key])) {
            return $this->initRelation()[$key];
        }
        return null;
    }

    /**
     * @return array
     */
    public function initRelation(): array
    {
        return [];
    }
}
