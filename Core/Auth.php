<?php

namespace Scolabs\Core;

use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Managers\AclManager;
use Scolabs\Managers\AclRoleManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Acl;
use Scolabs\Models\User;

/**
 * Class Auth
 * @package Scolabs\Core
 */
class Auth
{
    /**
     * @var AclManager
     */
    private static $aclManager;

    /**
     * @var UserManager
     */
    private static $userManager;

    /**
     * Initializer
     */
    public static function initialize()
    {
        self::$aclManager = new AclManager();
        self::$userManager = new UserManager();
    }

    /**
     * @return mixed
     */
    public static function getUser()
    {
        if (isset($_SESSION['auth'])) {
            if (time() < $_SESSION['auth']['expires_on']) {
                return unserialize($_SESSION['auth']['user']);
            }
        }
        return null;
    }

    /**
     * @param string $controller
     * @param string $action
     * @return bool
     * @throws NotAllowedException
     */
    public static function checkAccess(string $controller, string $action)
    {
        self::initialize();

        /** @var Acl $acl */
        $acl = self::getAcl($controller, $action);

        if (!is_null($acl->getId())) {
            if (Auth::getUser()) {
                $roleForCurrentUser = Auth::getUser()->getRole();
                $aclRoleManager = new AclRoleManager();
                if (!is_int($aclRoleManager->findOneWithRelation([
                    'id_role' => intval($roleForCurrentUser->getId()),
                    'id_acl' => intval($acl->getId())
                ])->getId())) {
                    throw new NotAllowedException("Vous n'avez pas accès à cette page");
                }
            } else {
                throw new NotAllowedException("Vous n'avez pas accès à cette page");
            }
        }
        return true;
    }


    /**
     * @param string $controller
     * @param string $action
     * @return bool
     */
    public static function hasAccessTo(string $controller, string $action): bool
    {
        try {
            return self::checkAccess($controller . 'Controller', $action . 'Action');
        } catch (NotAllowedException $e) {
            return false;
        }
    }

    /**
     * @param string $username
     * @param string $password
     * @return bool
     */
    public static function loginUser(string $username, string $password)
    {
        self::initialize();
        /** @var User $user */
        $user = self::$userManager->findOneBy(['username' => $username]);
        if (is_int($user->getId()) && password_verify($password, $user->getPassword()) && $user->getState() === 0) {
            $_SESSION['auth']['user'] = serialize(self::$userManager->getUserWithRole($user->getId()));
            $_SESSION['auth']['expires_on'] = time() + (60 * 60 * 24);
            return true;
        }
        return false;
    }

    /**
     * @param string $controller
     * @param string $action
     * @return Model|null Model
     */
    private static function getAcl(string $controller, string $action): ?Model
    {
        return self::$aclManager->findOneBy(['controller' => $controller, 'action' => $action]);
    }

    /**
     * @return void
     */
    public static function onlyVisitors()
    {
        if (isset($_SESSION['auth'])) {
            header("location:javascript://history.go(-1)");
        }
    }
}
