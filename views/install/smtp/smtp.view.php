<section>
    <div style="margin-top: 70px" class="text-center">
        <img width="200" height="200" src="/assets/img/logo.svg" alt="">
    </div>
    <h1 class="text-center">Configuration du server SMTP</h1>

    <div class="container">
        <div class="row card">
            <div class="col-md-12">
                <?= $form->startForm("color: white") ?>
                <div class="row">
                    <div class="col-md-4">
                        <?php $form->label('smtp') ?>
                        <?php $form->field('smtp') ?>
                    </div>
                    <div class="col-md-4">
                        <?php $form->label('port') ?>
                        <?php $form->field('port') ?>
                    </div>
                    <div class="col-md-4">
                        <?php $form->label('user') ?>
                        <?php $form->field('user') ?>
                    </div>
                    <div class="col-md-4">
                        <?php $form->label('password') ?>
                        <?php $form->field('password') ?>
                    </div>
                    <div class="col-md-4">
                        <?php $form->label('secure') ?>
                        <?php $form->field('secure') ?>
                    </div>

                    <div class="col-md-12">
                        <?php $form->field('submit') ?>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="btn-cold-purple" name="skip" value="Passer">
                    </div>
                </div>
                <?= $form->endForm() ?>
            </div>
        </div>
    </div>
</section>
