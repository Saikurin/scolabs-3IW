<?php

use Scolabs\Core\Helpers;

?>
<section>
    <div style="margin-top: 70px" class="text-center">
        <img width="200" height="200" src="assets/img/logo.svg" alt="">
    </div>
    <h1 class="text-center">Bienvenue sur Scolabs. Avant de nous lancer, nous avons besoin de certaines informations sur
        votre base de données. Il va falloir réunir
        les informations suivantes pour continuer.</h1>
    <ul class="text-center">
        <li>- Nom de la base de donnée</li>
        <li>- Nom d'utilisateur MySQL</li>
        <li>- Mot de passe de l'utilisateur</li>
        <li>- Adresse de la base de donnée</li>
        <li>- Préfixe de la table</li>
    </ul>
    <p style="margin-top: 30px;margin-bottom: 50px" class="text-center">Vous devriez normalement avoir reçu ces
        informations de la part de votre hébergeur. Si vous ne les avez pas, il faudra contacter votre hébergeur afin de
        continuer. </p>
    <div class="container"
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h3 style="color: white"><a href="<?= Helpers::getUrl('Install', 'bdd') ?>">Commencer l'installation</a>
                </h3>
            </div>
        </div>
    </div>
    </div>
</section>
