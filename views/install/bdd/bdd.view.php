<section>
    <div style="margin-top: 70px" class="text-center">
        <img width="200" height="200" src="assets/img/logo.svg" alt="">
    </div>
    <h1 class="text-center">Connexion à la base de données</h1>
    <div class="container">
        <div class="row card">
            <div class="col-md-12">
                <?= $form->startForm("color:white; text-align:center; margin-top: 70px"); ?>
                <div class="row card">
                    <div class="col-md-4">
                        <?= $form->label('host') ?><br>
                        <?= $form->field('host') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->label('port') ?><br>
                        <?= $form->field('port') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->label('dbname') ?><br>
                        <?= $form->field('dbname') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->label('user') ?><br>
                        <?= $form->field('user') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->label('password') ?><br>
                        <?= $form->field('password') ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->label('prefixe') ?><br>
                        <?= $form->field('prefixe') ?>
                    </div>
                    <div class="col-md-12">
                        <?= $form->field('submit') ?>
                    </div>
                </div>
                <?= $form->endForm(); ?>
            </div>
        </div>
    </div>
</section>


