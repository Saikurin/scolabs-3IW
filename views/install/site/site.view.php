<section>
    <div style="margin-top: 70px" class="text-center">
        <img width="200" height="200" src="/assets/img/logo.svg" alt="">
    </div>
    <h1 class="text-center">Configuration du site</h1>
    <div class="container"
    <div class="row">
        <div class="col-md-12">
            <?= $form->startForm('color:white; text-align:center') ?>
            <div class="row card">
                <div class="col-md-4">
                    <?= $form->label('title') ?><br>
                    <?= $form->field('title') ?>
                </div>

                <div class="col-md-4">
                    <?= $form->label('school_name') ?><br>
                    <?= $form->field('school_name') ?>
                </div>

                <div class="col-md-4 form-group">
                    <?= $form->label('description') ?><br>
                    <?= $form->field('description') ?>
                </div>
                <div class="col-md-4 form-group">
                    <?= $form->label('adresse') ?><br>
                    <?= $form->field('adresse') ?>
                </div>

                <div class="col-md-12"><br>
                    <input type="submit" value="Sauvegarder" name="submit" id="diffHeight"
                           class="btn btn-dark-purple">
                </div>
            </div>
            <?= $form->endForm() ?>
        </div>
    </div>
</section>