<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des matières</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Professeur</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($subjects as $subject): ?>
                <tr>
                    <td><?= $subject->getName() ?></td>
                    <td><?= strtoupper(substr($subject->getTeacher()->getFirstname(), 0, 1)) . '.' . $subject->getTeacher()->getLastname() ?></td>
                    <td>
                        <?php if (\Scolabs\Core\Auth::hasAccessTo('Subjects', 'edit')): ?>
                            <a class="btn btn-dark-purple"
                               href="<?= $this->getUrl('Subjects', 'edit', ['id' => $subject->getId()]) ?>">Modifier</a>
                        <?php endif ?>
                        <?php if (\Scolabs\Core\Auth::hasAccessTo('Subjects', 'delete')): ?>
                            <form method="POST"
                                  action="<?= $this->getUrl('Subjects', 'delete', ['id' => $subject->getId()]) ?>">
                                <input type="hidden" name="_csrf" id="_csrf"
                                       value="<?= \Scolabs\Core\Helpers::generateCsrfToken() ?>">
                                <input type="hidden" name="id" value="<?= $subject->getId() ?>">
                                <input type="submit" value="Supprimer" class="btn btn-dark-purple">
                            </form>
                        <?php endif ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>