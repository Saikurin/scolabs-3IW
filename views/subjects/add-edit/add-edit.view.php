<?php

use Scolabs\Core\Helpers;

?>
<div class="row">
    <div class="col-md-12 text-center">
        <h1><?= $title ?></h1>
    </div>
    <hr>
    <?php $form->startForm(); ?>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('name') ?>
        <br>
        <span class="input "><?= $form->field('name') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('teacher') ?>
        <br>
        <span class="input "><?= $form->field('teacher') ?></span>
    </div>
    <div class="col-md-12 spacearound">
        <?= $form->field('submit') ?>
    </div>
    <?php $form->endForm(); ?>
</div>



