<div class="row">
    <div class="col-md-12 text-center">
        <h1>Actualité de l'établissement <?= $school_name ?></h1>
    </div>
    <hr>
    <?php foreach ($events as $event): ?>
        <div class="col-md-12">
            <div class="card">
                <div class="card-header d-flex flex-row">
                    <h4 class="card-title"><?= $event->getTitle() ?></h4>
                    <span class="badge badge-bright-lavender" style="align-self: center"><?= date('d/m/Y', strtotime($event->getCreatedAt())) ?></span>
                </div>
                <div class="card-body">
                    <img src="<?= $event->getPicture() ?>" alt="<?= str_replace(' ', '-', $event->getTitle()) ?>"
                         class="img-50">
                    <p><?= wordwrap($event->getContent(), 100) ?></p>
                    <?= (strlen($event->getContent() > 100) ? '<a href="" class="btn btn-dark-turquoise">Voir plus...</a>' : ''); ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>