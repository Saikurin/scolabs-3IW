<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Création d'une news</h1>
    </div>
</div>
<hr>
<?php $form->startForm(); ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->label('title') ?><br>
        <?= $form->field('title') ?>
    </div>
    <div class="col-md-12">
        <?= $form->label('resume') ?>
        <?= $form->field('resume') ?>
    </div>
    <div class="col-md-12">
        <?= $form->label('new') ?>
        <?= $form->field('new') ?>
    </div>
    <div class="col-md-12 my-5">
        <?= $form->label('picture') ?>
        <?= $form->field('picture') ?>
    </div>
    <?= $form->field('submit')  ?>

</div>

<?php $form->endForm(); ?>
