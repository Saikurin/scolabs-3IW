<div class="row">
    <?php if ($event->getPicture()): ?>
        <div class="col-md-6 offset-md-3">
            <img src="<?= $event->getPicture() ?>" class="w-100" alt="">
        </div>
    <?php endif ?>

    <div class="col-md-6 offset-md-3">
        <h1><?= $event->getTitle() ?></h1>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <h4>Résumé</h4>
        <p><?= $event->getResume() ?></p>
    </div>
</div>

<div class="row mt-5">
    <div class="col-md-12">
        <p><?= $event->getContent() ?></p>
    </div>
</div>

<?php if ($event->getState() !== 2): ?>
    <button class="btn btn-dark-purple" id="edit">Modifier</button>

    <div class="modal" id="modal">
        <div class="modal-content">
            <h1>Modifier l'evenement</h1>
            <form action="<?= $this->getUrl('Events', 'edit', ['id' => $event->getId()]) ?>" method="post"
                  enctype="multipart/form-data">
                <input type="hidden" name="token" value="<?= md5($event->getId()) ?>">
                <input type="file" name="picture" id="picture">
                <label for="title">Titre</label>
                <input type="text" name="title" id="title" value="<?= $event->getTitle() ?>">
                <label for="resume">Résume</label>
                <textarea name="resume" id="resume" cols="30" rows="10"><?= $event->getResume() ?></textarea>
                <label for="content">Contenu</label>
                <textarea name="content" id="content" cols="30" rows="10"><?= $event->getContent() ?></textarea>
                <input class="btn btn-dark-purple" type="submit" value="Modifier">
            </form>
        </div>
    </div>
<?php endif ?>