<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des news</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <a href="<?= $this->getUrl('Events', 'new') ?>" class="ml-auto btn btn-dark-purple">Créer un évènement</a>

    </div>

</div>
<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
            <tr>
                <th>Titre</th>
                <th>Résumé</th>
                <th>Date de création</th>
                <th>Statut</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($events as $event): ?>
                <tr>
                    <td><?= $event->getTitle() ?></td>
                    <td><?= $event->getResume() ?></td>
                    <td><?= date('d/m/Y', strtotime($event->getCreatedAt())) ?></td>
                    <td><?php switch ($event->getState()): case 1:
                            echo 'En ligne';
                            break;
                            case 2:
                                echo 'Archivé';
                                break; endswitch; ?></td>
                    <td>
                        <a href="<?= $this->getUrl('Events', 'showadmin', ['id' => $event->getId()]) ?>"
                           class="btn btn-dark-purple">Voir</a>
                        <form action="<?= $this->getUrl('Events', 'delete', ['id' => $event->getId()]) ?>"
                              method="post">
                            <input type="hidden" name="validation" value="<?= $event->getId() ?>">
                            <input type="hidden" name="token" value="<?= md5($event->getTitle()) ?>">
                            <input type="submit" value="Supprimer" class="delete btn btn-dark-turquoise"
                                   data-confirm="Êtes-vous sur de vouloir supprimer cet evenement">
                        </form>
                        <?php if ($event->getState() === 1): ?>
                            <form action="<?= $this->getUrl('Events', 'archive', ['id' => $event->getId()]) ?>"
                                  method="post">
                                <input type="hidden" name="validation" value="<?= $event->getId() ?>">
                                <input type="hidden" name="token" value="<?= md5($event->getTitle()) ?>">
                                <input type="submit" value="Archiver" class="delete btn btn-dark-turquoise"
                                       data-confirm="Êtes-vous sur de vouloir archiver cet evenement">
                            </form>
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>