<?php if (isset($error)): ?>
    <p><?= $error ?></p>
<?php endif ?>


<div class="row displayflexnone spacearound">
        <div class="col-md-12 text-center">
            <h1>Nous contacter</h1>
        </div>
        <hr>

        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('email') ?><br>
            <span class="input "><?= $form->field('email') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('message') ?><br>
            <span class="input  "><?= $form->field('message') ?></span>
        </div>
        <div class="col-md-12 spacearound form-group text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>
    </div>


