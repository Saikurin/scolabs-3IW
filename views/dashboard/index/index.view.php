<?php use Scolabs\Core\Auth;

?>
<div class="row">

    <div class="col-md-12 text-center">
        <h1>Bienvenue <?php //username ?></h1>
        <h2>Accédez rapidement à votre espace utilisateur</h2>
    </div>

    <hr>
    <?php if (Auth::hasAccessTo('Marks', 'classrooms') || Auth::hasAccessTo('Routine', 'list') || Auth::hasAccessTo('Book', 'reserveBook')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Etudiant</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if (Auth::hasAccessTo('Marks', 'classrooms') || Auth::hasAccessTo('Book', 'available')): ?>
                                <a href="<?= $this->getUrl('Marks', 'classrooms') ?>"
                                   class="btn btn-dark-purple btn-block">Voir mes notes</a>
                            <?php endif; ?>
                            <?php if (Auth::hasAccessTo('Routine', 'list')): ?>
                                <a href="<?= $this->getUrl('Routine', 'list') ?>" class="btn btn-dark-purple btn-block">Voir
                                    mes cours</a>
                            <?php endif; ?>
                            <?php if (Auth::hasAccessTo('Book', 'available')): ?>
                                <a href="<?= $this->getUrl('Book', 'available') ?>"
                                   class="btn btn-dark-purple btn-block">Emprunter un livre</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Routine', 'list') || Auth::hasAccessTo('Marks', 'classrooms')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Enseignant</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Routine', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Routine', 'list') ?>" class="btn btn-dark-purple btn-block">Planning</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Marks', 'classrooms')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Marks', 'classrooms') ?>"
                                   class="btn btn-dark-purple btn-block">Note</a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Book', 'create') || Auth::hasAccessTo('Book', 'list')): ?>
        <div class="col-md-4">

            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace CDI</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Book', 'create')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Book', 'create') ?>" class="btn btn-dark-purple btn-block">Ajouter
                                    un livre</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Book', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Book', 'list') ?>" class="btn btn-dark-purple btn-block">Lister
                                    les livres</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Invoices', 'list') || Auth::hasAccessTo('Products', 'index')): ?>
        <div class="col-md-4">

            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Facturation</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Invoices', 'create')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Invoices', 'create') ?>"
                                   class="btn btn-dark-purple btn-block">Créer une facture</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Invoices', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Invoices', 'list') ?>"
                                   class="btn btn-dark-purple btn-block">lister les factures</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Products', 'index')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Products', 'index') ?>"
                                   class="btn btn-dark-purple btn-block">lister les produits</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Products', 'add')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Products', 'add') ?>"
                                   class="btn btn-dark-purple btn-block">Créer un produit</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Events', 'new') || Auth::hasAccessTo('Events', 'list')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Communication</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Events', 'new')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Events', 'new') ?>" class="btn btn-dark-purple btn-block">Créer
                                    une news</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Events', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Events', 'list') ?>" class="btn btn-dark-purple btn-block">Lister
                                    les news</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Admissions', 'register') || Auth::hasAccessTo('Admissions', 'index') || Auth::hasAccessTo('Classrooms', 'index')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Admission</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Admissions', 'register')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Admissions', 'register') ?>"
                                   class="btn btn-dark-purple btn-block">Ajouter un élève</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Admissions', 'index')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Admissions', 'index') ?>"
                                   class="btn btn-dark-purple btn-block">lister les élèves</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Classrooms', 'index')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Classrooms', 'index') ?>"
                                   class="btn btn-dark-purple btn-block">lister les classes</a>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Admissions', 'contact')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Contact</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Admissions', 'contact')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Admissions', 'contact') ?>"
                                   class="btn btn-dark-purple btn-block">Voir les demandes de contact</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Configuration', 'list')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace Configuration</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Configuration', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Configuration', 'list') ?>"
                                   class="btn btn-dark-purple btn-block">Modifier le site</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('User', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('User', 'list') ?>"
                                   class="btn btn-dark-purple btn-block">Lister les utilisateurs</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('User', 'add')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('User', 'add') ?>"
                                   class="btn btn-dark-purple btn-block">Ajouter des utilisateurs</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if (Auth::hasAccessTo('Routine', 'add') || Auth::hasAccessTo('Subjects', 'list')): ?>
        <div class="col-md-4">
            <div class="card d-block">
                <div class="card-header">
                    <h1 class="card-title text-center">Espace gestion de planning</h1>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php if (Auth::hasAccessTo('Routine', 'add')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Routine', 'add') ?>" class="btn btn-dark-purple btn-block">Créer
                                    un planning</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Subjects', 'add')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Subjects', 'add') ?>" class="btn btn-dark-purple btn-block">Créer
                                    une matière</a>
                            </div>
                        <?php endif; ?>
                        <?php if (Auth::hasAccessTo('Subjects', 'list')): ?>
                            <div class="col-md-12">
                                <a href="<?= $this->getUrl('Subjects', 'list') ?>" class="btn btn-dark-purple btn-block">Lister les matières</a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="col-md-4">
        <div class="card d-block">
            <div class="card-header">
                <h1 class="card-title text-center">Espace Profil</h1>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?= $this->getUrl('Profile', 'user') ?>" class="btn btn-dark-purple btn-block">Éditer
                            mon profil</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

