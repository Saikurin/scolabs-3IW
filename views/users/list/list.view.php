<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des utilisateurs</h1>
        <a class="btn btn-dark-purple add-button" href="<?= $this->getUrl("User", "add") ?>">
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
            Ajouter un utilisateur
        </a>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Role</th>
                    <th>Phone</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td class="id"><?= $user->getId(); ?></td>
                        <td><?= htmlspecialchars($user->getFirstname()); ?></td>
                        <td><?= htmlspecialchars($user->getLastname()); ?></td>
                        <td><?= htmlspecialchars($user->getRole()->getName()); ?></td>
                        <td><?= htmlspecialchars($user->getPhone()); ?></td>
                        </td>
                        <td>
                            <form method="post" action="<?= $this->getUrl('User', 'delete', ["id" => $user->getId()]) ?>">
                                <input class="btn btn-dark-purple" type="submit" value="&#128465; Supprimer">
                            </form>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>