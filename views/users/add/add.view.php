<?php

use Scolabs\Core\Helpers;
?>
<?php if (isset($error)) : ?>
    <p><?= $error ?></p>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple" href="<?= Helpers::getUrl("User", "list") ?>">
            < Retour</a> </div> </div> <div>
                <div class="row displayflexnone">
                    <div class="col-md-12 text-center">
                        <h1>Ajouter un Utilisateur</h1>
                    </div>
                    <hr>
                    <div class="form-container">
                        <?php $form->startForm(); ?>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('name') ?><br></label>
                            <div class="input borderinput"><?= $form->field('name') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('firstname') ?><br></label>
                            <div class="input borderinput"><?= $form->field('firstname') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('email') ?><br></label>
                            <div class="input borderinput"><?= $form->field('email') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('role[]') ?><br></label>
                            <div class="input borderinput"><?= $form->field('role[]') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('phone') ?><br></label>
                            <div class="input borderinput"><?= $form->field('phone') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('dob') ?><br></label>
                            <div class="input borderinput"><?= $form->field('dob') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('state[]') ?><br></label>
                            <div class="input borderinput"><?= $form->field('state[]') ?></div>
                        </div>
                        <div class="spacearound text-center">
                            <?= $form->field('submit') ?>
                        </div>
                        <?php $form->endForm(); ?>
                    </div>
                </div>

    </div>
