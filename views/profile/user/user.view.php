<h1 class="text-center">Mon Profil</h1>
<hr>

<button class="btn btn-dark-purple" id="edit">Modifier</button>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3 class="text-center">Mr Théo SIKLI</h3>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <span class="badge badge-dark-turquoise mx-auto"><?= $user->getRole()->getName() ?></span>
                        <span class="badge badge-dark-turquoise mx-auto"><?= floor(abs(strtotime(date('Y-m-d')) - strtotime($user->getDob())) / (365 * 60 * 60 * 24)) ?> ans</span>
                        <span class="badge badge-dark-turquoise mx-auto"><?= $user->getPhone() ?? 'TEL NON RENSEIGNE' ?></span>
                    </div>
                </div>
                <?php if ($user->getRole()->getSlug() === 'student'): ?>
                    <table>
                        <tbody>
                        <tr>
                            <td>Classe</td>
                            <td>3A</td>
                        </tr>
                        <tr>
                            <td>Promotion</td>
                            <td>3IW</td>
                        </tr>
                        </tbody>
                    </table>
                <?php endif ?>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php if (null !== $user->getAddress()): ?>
                            <p><strong>Adresse
                                    :</strong> <a target="_blank"
                                                  href="https://maps.google.fr/?q=<?= str_replace(' ', '+', $user->getAddress() . " ," . $user->getZipCode() . " " . $user->getCity()) ?>"><?= $user->getAddress() . " ," . $user->getZipCode() . " " . $user->getCity() ?></a>
                            </p>
                            <p><strong>Adresse
                                    :</strong> <a class="btn btn-dark-purple" target="_blank"
                                                  href="https://maps.google.fr/?daddr=<?= str_replace(' ', '+', $user->getAddress() . " ," . $user->getZipCode() . " " . $user->getCity()) ?>&saddr=<?= str_replace(' ', '+', $address) ?>">Itinéraire</a>
                            </p>
                        <?php endif ?>
                        <?php if (isset($parents)): ?>
                            <?php foreach ($parents as $parent): ?>
                                <p><strong><?= $parent->getFirstname() . ' ' . strtoupper($parent->getLastname()) ?>
                                        :</strong> <a target="_blank"
                                                      href="https://maps.google.fr/?q=<?= str_replace(' ', '+', $parent->getAddress()) . '+' . $parent->getZipCode() . '+' . str_replace(' ', '+', $parent->getCity()) ?>"><?= $parent->getAddress() . ' , ' . $parent->getZipCode() . ' ' . $parent->getCity() ?></a>
                                </p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr>

<?php if (isset($subjects) && count($subjects) > 0): ?>
    <h2 class="text-center">Mes cours</h2>
    <div class="row">
        <table>
            <thead>
            <tr>
                <th>Matières</th>
                <th>Professeur</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($subjects as $subject): ?>
                <tr>
                    <td><?= strtoupper($subject->getName()) ?></td>
                    <td><?= strtoupper(substr($subject->getTeacher()->getFirstname(), 0, 1)) . '.' . ucfirst($subject->getTeacher()->getLastname()) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif ?>

<div class="modal" id="modal">
    <div class="modal-content">
        <h1>Modifier mon compte</h1>
        <div class="row">
            <?= $form->startForm() ?>
            <div class="col-md-6">
                <?= $form->label('tel') ?>
                <?= $form->field('tel') ?>
            </div>
            <div class="col-md-6">
                <?= $form->label('email') ?>
                <?= $form->field('email') ?>
            </div>
            <div class="col-md-6">
                <?= $form->label('password') ?>
                <?= $form->field('password') ?>
            </div>
            <div class="col-md-6">
                <?= $form->label('repeat-password') ?>
                <?= $form->field('repeat-password') ?>
            </div>
            <div class="col-md-12">
                <?= $form->label('address') ?>
                <?= $form->field('address') ?>
            </div>
            <div class="col-md-12">
                <?= $form->label('zip_code') ?>
                <?= $form->field('zip_code') ?>
            </div>
            <div class="col-md-12">
                <?= $form->label('city') ?>
                <?= $form->field('city') ?>
            </div>
            <div class="col-md-12">
                <?= $form->field('submit') ?>
            </div>
            <?= $form->endForm() ?>
        </div>
    </div>
</div>
