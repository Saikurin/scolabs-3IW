<?php

use Scolabs\Core\Helpers;

?>

<?php if (isset($error)): ?>
    <p><?= $error ?></p>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple" href="<?= Helpers::getUrl("Classrooms", "index") ?>">Retourner à la liste des
            classes</a>
    </div>
</div>

<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1>Ajouter un cours</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('classroom') ?><br>
            <?= $form->field('classroom') ?>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('subject') ?><br>
            <?= $form->field('subject') ?>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('day') ?><br>
            <?= $form->field('day') ?>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('start') ?><br>
            <?= $form->field('start') ?>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('end') ?><br>
            <?= $form->field('end') ?>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('room') ?><br>
            <?= $form->field('room') ?>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>


