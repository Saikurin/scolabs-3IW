$("#day").select2({
    placeholder: {
        id: '', // the value of the option
        text: 'Jour de la semaine'
    },
});

$("#subject").select2({
    placeholder: {
        id: '', // the value of the option
        text: 'Matière'
    },
});

$("#classroom").select2({
    placeholder: {
        id: '', // the value of the option
        text: 'Classe'
    },
});