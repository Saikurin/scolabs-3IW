<?php

use Scolabs\Core\Helpers;

?>

<?php if (isset($error)): ?>
    <p><?= $error ?></p>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple" href="<?= Helpers::getUrl("Classrooms", "index") ?>">Retourner à la liste des
            classes</a>
    </div>
</div>

<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1>Modifier un cours</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('classroom') ?><br>
            <span class="input "><?= $form->field('name') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('subject') ?><br>
            <span class="input  "><?= $form->field('level') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('start') ?><br>
            <span class="input  "><?= $form->field('start') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('end') ?><br>
            <span class="input  "><?= $form->field('end') ?></span>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>


