<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des cours</h1>
    </div>
</div>
<hr>
<?php if (\Scolabs\Core\Auth::hasAccessTo('Routine', 'add')): ?>
    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-dark-purple" href="<?= $this->getUrl("Routine", "add") ?>">Ajouter un cours</a>
        </div>
    </div>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
            <tr>
                <th></th>
                <th>Lundi</th>
                <th>Mardi</th>
                <th>Mercredi</th>
                <th>Jeudi</th>
                <th>Vendredi</th>
                <th>Samedi</th>
            </tr>
            </thead>
            <tbody>
            <!-- For first hour #min_hour -->
            <tr>
                <td><?= $minHour ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . $minHour;
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . $minHour;
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . $minHour;
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . $minHour;
                            })[1]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . $minHour;
                            })[1]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . $minHour;
                            })[1]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . $minHour;
                            })[2]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . $minHour;
                            })[2]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . $minHour;
                            })[2]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . $minHour;
                            })[3]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . $minHour;
                            })[3]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . $minHour;
                            })[3]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . $minHour;
                            })[4]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . $minHour;
                            })[4]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . $minHour;
                            })[4]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . $minHour;
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . $minHour;
                            })[5]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . $minHour;
                            })[5]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . $minHour;
                            })[5]->getClassroom()->getName();
                    }
                    ?></td>
            </tr>
            <!-- Foreach hours between min an max -->
            <?php for ($i = 1; $i < $hours; $i++): ?>
                <tr>
                    <td><?= date('H:i', strtotime($minHour) + (60 * 60 * $i)) ?></td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                // Get room
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                // Get name of classroom
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                // Get room
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                // Get name of classroom
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                // Get room
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                // Get name of classroom
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                // Get room
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                // Get name of classroom
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                // Get room
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                // Get name of classroom
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                    <td><?php
                        if (count(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                            })) > 0) {
                            echo
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getSubject()->getName() . "<br/>" .
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getRoom() . "<br/>" .
                                array_values(array_filter($courses, function ($course) use ($i, $minHour, $days_in_week) {
                                    return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($minHour) + (60 * 60 * $i));
                                }))[0]->getClassroom()->getName();
                        }
                        ?>
                    </td>
                </tr>
            <?php endfor ?>
            <tr>
                <td><?= $maxHour ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[0] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[1] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[2] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[3] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[4] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
                <td><?php
                    if (count(array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                            return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($maxHour));
                        })) > 0) {
                        echo array_filter($courses, function ($course) use ($days_in_week, $maxHour) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getSubject()->getName() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getRoom() . "<br/>" . array_filter($courses, function ($course) use ($maxHour, $days_in_week) {
                                return substr($course->getStart(), 0, 16) === $days_in_week[5] . ' ' . date('H:i', strtotime($maxHour));
                            })[0]->getClassroom()->getName();
                    }
                    ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>