<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des produits</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <a href="<?= $this->getUrl('Products', 'add') ?>" class="btn btn-dark-purple">Ajouter un produit facturable</a>

    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Description</th>
                    <th>Prix Unitaire HT</th>
                    <th>Etat</th>
                    <th>Actions</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $product->getId() ?></td>
                        <td><?= htmlspecialchars_decode($product->getName(), ENT_QUOTES) ?></td>
                        <td><?= htmlspecialchars_decode($product->getDescription(), ENT_QUOTES) ?></td>
                        <td><?= $product->getUnitPrice() ?> <?= $currency_symbol ?></td>
                        <td><?= $product->getAvailability() ?></td>
                        <td><a href="<?= $this->getUrl('Products','edit', ['id' => $product->getId()]) ?>" class="btn btn-dark-turquoise">Editer</a></td>
                        <td><a href="<?= $this->getUrl('Products','delete', ['id' => $product->getId()]) ?>" class="btn btn-dark-purple">Supprimer</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>