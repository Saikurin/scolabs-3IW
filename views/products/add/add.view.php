<?= (isset($error)) ? $error : ''; ?>

<?php

use Scolabs\Core\Helpers;

?>
<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1>Ajouter un produit</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput" >
            <?= $form->label('name') ?>
            <br>
            <span class="input "><?= $form->field('name') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput" >
            <?= $form->label('description') ?>
            <br>
            <span class="input "><?= $form->field('description') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput" >
            <?= $form->label('unit_price') ?>
            <br>
            <span class="input "><?= $form->field('unit_price') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput" >
            <?= $form->label('availability') ?>
            <br>
            <span class="input "><?= $form->field('availability') ?></span>
        </div>
        <div class="col-md-12 spacearound">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>
