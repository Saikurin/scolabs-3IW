<?= (isset($error)) ? $error : ''; ?>

<?php

use Scolabs\Core\Helpers;

?>
<div class="container">
<div class="row displayflexnone spacearound ">
    <div class="col-md-12 text-center">
        <img src="public/img/logo.svg " width="20%">
    </div>
    <?php $form->startForm(); ?>
    <div class="col-md-12 text-center spacearound form-group borderinput" >
        <?= $form->label('username') ?><br>
        <span class="input "><?= $form->field('username') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('password') ?><br>
        <span class="input  "><?= $form->field('password') ?></span>
    </div>
    <div class="col-md-12 spacearound">
        <?= $form->field('submit') ?>
    </div>
    <?php $form->endForm(); ?>
    <div class="col-md-12 text-center">
        <a href="<?= $this->getUrl('Auth', 'forgotpassword') ?>">Reinitaliser votre mot de passe</a>
    </div>
</div>

    <hr>

    <div class="card">
        <div class="card-header">
            <h1 class="card-title text-center">DEMO ONLY</h1>
        </div>
        <div class="card-body demo">
            <button class="btn btn-dark-purple" data-login="admin.admin" data-password="azerty">Administrateur</button>
            <button class="btn btn-dark-purple" data-login="student.student" data-password="azerty">Etudiant</button>
            <button class="btn btn-dark-purple" data-login="prof.prof" data-password="azerty">Professeur</button>
            <button class="btn btn-dark-purple" data-login="parents.parents" data-password="azerty">Parents</button>
            <button class="btn btn-dark-purple" data-login="comptable.comptable" data-password="azerty">Comptable</button>
            <button class="btn btn-dark-purple" data-login="libraire.libraire" data-password="azerty">Libraire</button>
            <button class="btn btn-dark-purple" data-login="communication.communication" data-password="azerty">Chargé de communication</button>
            <button class="btn btn-dark-purple" data-login="admission.admission" data-password="azerty">Admission</button>
            <button class="btn btn-dark-purple" data-login="planning.planning" data-password="azerty">Chargé de planning</button>

        </div>
    </div>

</div>