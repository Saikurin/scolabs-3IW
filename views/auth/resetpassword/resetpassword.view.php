<?= (isset($error)) ? $error : ''; ?>

<?php

use Scolabs\Core\Helpers;

?>
<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <img src="/public/img/logo.svg " width="20%">
        </div>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput" >
            <?= $form->label('email') ?><br>
            <span class="input "><?= $form->field('email') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('password') ?><br>
            <span class="input  "><?= $form->field('password') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('repeat-password') ?><br>
            <span class="input  "><?= $form->field('repeat-password') ?></span>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>