<?php
use Scolabs\Core\Auth;
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scolabs</title>
    <link rel="stylesheet" href="/public/css/scotrap.css">
    <?php $this->loadStyles(); ?>
</head>
<body>
<img src="/assets/img/logo.svg" width="150px" height="150px" style="margin: 0 auto; display: block" alt="">
<div class="container">
    <?php include "views/" . $this->view . "/" . $this->underfile . "/" . $this->underfile . ".view.php"; ?>
</div>
<script type="application/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<?php $this->loadScripts(); ?>
</body>
</html>
