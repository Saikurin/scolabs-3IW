<?php
    use Scolabs\Core\Auth;

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scolabs</title>
    <link rel="stylesheet" href="/public/css/scotrap.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" href="/<?= glob('public/img/logo.*', GLOB_ERR)[0] ?>" />
    <?php $this->loadStyles(); ?>
</head>
<body>
<nav>
    <ul class="menu">
        <li class="logo"><a href="<?= $this->getUrl('Dashboard', 'index') ?>">
                <img width="50" height="50" src="/<?= glob('public/img/logo.*', GLOB_ERR)[0] ?>" alt="">
            </a></li>
        <li class="item"><a href="<?= $this->getUrl('Dashboard', 'index') ?>">Panel d'administration</a></li>
        <?php if (Auth::hasAccessTo('User','list')): ?>
        <li class="item"><a href="<?= $this->getUrl('User', 'list') ?>">Utilisateurs</a></li>
        <?php endif ?>
        <?php if (Auth::hasAccessTo('Book','list')): ?>
          <li class="item"><a href="<?= $this->getUrl('Book', 'list') ?>">CDI</a></li>
        <?php endif ?>
        <?php if (Auth::hasAccessTo('Classrooms','index')): ?>
            <li class="item"><a href="<?= $this->getUrl('Classrooms', 'index') ?>">Classes</a></li>
        <?php endif; ?>
        <?php if (Auth::hasAccessTo('Admissions','index')): ?>
        <li class="item"><a href="<?= $this->getUrl('Admissions', 'index') ?>">Admission</a></li>
        <?php endif; ?>
        <?php if (Auth::hasAccessTo('Marks','classrooms')): ?>
            <li class="item"><a href="<?= $this->getUrl('Marks', 'classrooms') ?>">Notes</a></li>
        <?php endif; ?>
        <?php if (Auth::hasAccessTo('Routine','list')): ?>
        <li class="item"><a href="<?= $this->getUrl('Routine', 'list') ?>">Planning</a></li>
        <?php endif; ?>
        <?php if (Auth::hasAccessTo('Invoices','list')): ?>
        <li class="item"><a href="<?= $this->getUrl('Invoices', 'list') ?>">Facture</a></li>
        <?php endif; ?>
        <?php if (Auth::hasAccessTo('Events','list')): ?>
        <li class="item"><a href="<?= $this->getUrl('Events', 'list') ?>">News</a></li>
        <?php endif; ?>
        <li class="d-sm-none item">|</li>
        <?php if (Auth::hasAccessTo('Configuration','list')): ?>
        <li class="item"><a href="<?= $this->getUrl('Configuration', 'list') ?>"><i class="fa fa-cog" aria-hidden="true"></i></a></li>
        <?php endif; ?>
        <li class="item"><a href="<?= $this->getUrl('Profile', 'user') ?>">Mon profil</a></li>
        <li class="item"><a href="<?= $this->getUrl('Auth', 'logout') ?>">Déconnexion</a></li>
    </ul>

</nav>
<div class="container">
    <?php include "views/" . $this->view . "/" . $this->underfile . "/" . $this->underfile . ".view.php"; ?>
</div>
<script type="application/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<script type="text/javascript" src="/public/js/scotrap.js"></script>
<?php $this->loadScripts(); ?>
</body>
</html>
