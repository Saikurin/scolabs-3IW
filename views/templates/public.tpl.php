<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scolabs</title>
    <link rel="stylesheet" href="/public/css/scotrap.css">
    <link rel="stylesheet" href="/public/css/front.css">
    <?php $this->loadStyles(); ?>
</head>
<body>
<nav>
    <ul class="menu">
        <li class="logo"><a href="/">
                <img width="50" height="50" src="/public/img/logo.svg" alt="">
            </a></li>
        <li class="item"><a href="<?= $this->getUrl('Contact', 'index') ?>">Contact</a></li>


        <?php if (\Scolabs\Core\Auth::getUser()): ?>
            <li class="item"><a href="<?= $this->getUrl('Dashboard', 'index') ?>">Accès au dashboard</a></li>
            <li class="item"><a href="<?= $this->getUrl('Auth', 'logout') ?>">Deconnexion</a></li>
        <?php else: ?>
            <li class="item"><a href="<?= $this->getUrl('Auth', 'login') ?>">Connexion</a></li>
        <?php endif ?>
    </ul>

</nav>
<div class="container">
    <?php include "views/" . $this->view . "/" . $this->underfile . "/" . $this->underfile . ".view.php"; ?>
</div>
<script type="application/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js"></script>
<?php $this->loadScripts(); ?>
</body>
<footer class="text-center">Tous droit réservés
    à <?= (new \Scolabs\Managers\ConfigurationManager())->findOneBy(['conf' => 'title'])->getValue() ?><br><img
            width="50" height="50" src="/public/img/logo.svg" alt=""></footer>
</html>
