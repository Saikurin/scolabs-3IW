<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scolabs</title>
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <link rel="stylesheet" href="/public/css/scotrap.css">
    <?php $this->loadStyles(); ?>
</head>
<body>
<?php include "views/" . $this->view . "/" . $this->underfile . "/" . $this->underfile . ".view.php"; ?>
<script type="application/javascript" src="https://code.jquery.com/jquery-3.5.0.min.js%22%3E</script>
<?php $this->loadScripts(); ?>
</body>
</html>