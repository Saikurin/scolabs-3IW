<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Scolabs</title>
</head>
<body class="text-center">
<h1>Scolabs</h1>

<p>Madame, monsieur <br> Vous venez d'être inscrit en tant que Etudiant sur Scolabs.</p>
<p>Vos identifiants de connexion sont les suivants :</p>
<p>Nom d'utilisateur : <?= $username ?></p>
<p>Mot de passe : <?= $password ?></p>
<p>Si vous n'êtes pas <?= $firstname. ' '.$lastname ?>, veuillez ignorer ce mail.</p>

</body>
</html>