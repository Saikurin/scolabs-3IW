<h1 class="text-center">Mes notes</h1>
<hr>
<table>
    <thead>
    <tr>
        <th>Matière</th>
        <th>Date du contrôle</th>
        <th>Note</th>
        <th>Coefficient</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($marks as $mark): ?>
        <tr>
            <td><?= $mark->getSubject()->getName() ?> (<?= strtoupper(substr($mark->getSubject()->getTeacher()->getFirstname(),0,1)) ?>. <?=  strtoupper($mark->getSubject()->getTeacher()->getLastname()) ?>)</td>
            <td><?= date('d/m/Y',strtotime($mark->getDate())) ?></td>
            <td><?= $mark->getNote() ?>/20</td>
            <td><?= $mark->getCoeff() ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>