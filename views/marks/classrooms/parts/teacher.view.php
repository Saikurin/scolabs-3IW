<h1 class="text-center">Classes présentes au sein de l'école</h1>
<hr>
<table>
    <thead>
        <tr>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($classrooms as $classroom): ?>
            <tr>
                <td><?= $classroom->getName() ?></td>
                <td><a href="<?= $this->getUrl('Marks','attribute', ['name' => str_replace(' ','-',$classroom->getName())]) ?>" class="btn btn-dark-purple">Attribuer des notes</a></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>