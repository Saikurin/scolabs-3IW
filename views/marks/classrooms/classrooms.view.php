<?php if(\Scolabs\Core\Auth::getUser()->getRole()->getSlug() === 'teachers'): ?>
    <?php include 'parts/teacher.view.php' ?>
<?php else: ?>
    <?php include 'parts/student.view.php' ?>
<?php endif ?>
