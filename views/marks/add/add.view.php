<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Attention ! Il n'est pas possible de modifier une note après saisie !</h3>

    </div>
</div>

<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1 class="text-center">Ajouter une note à : <?= $user->getFirstname().' '. strtoupper($user->getLastname()) ?></h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('subject') ?><br>
            <span class="input "><?= $form->field('subject') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('date') ?><br>
            <span class="input  "><?= $form->field('date') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('mark') ?><br>
            <span class="input  "><?= $form->field('mark') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('coeff') ?><br>
            <span class="input  "><?= $form->field('coeff') ?></span>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>


