<h1 class="text-center">Attribution des notes pour la classe <?= $classroom->getName() ?></h1>
<hr><br>
<table>
    <thead>
    <tr>
        <th>Etudiant</th>
        <th>Notes</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($students as $student): ?>
        <tr>
            <td><?= $student->getFirstname() . ' ' . strtolower($student->getLastname()) ?></td>
            <td>
                <button class="btn btn-dark-purple seeMarks" data-id="<?= $student->getId() ?>">Voir les notes</button>
            </td>
            <td>
                <a target="_blank" class="fa-stack add-note"
                   href="<?= $this->getUrl('Marks', 'add', ['id' => $student->getId()]) ?>">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-plus fa-stack-1x fa-inverse"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<div class="modal" id="marks">
    <div class="modal-content">

    </div>
</div>