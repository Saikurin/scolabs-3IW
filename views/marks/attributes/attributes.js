$(".seeMarks").on('click', function () {
    $.ajax({
        type: 'GET',
        url: '/dashboard/ajax/marks/' + $(this).data('id'),
        async: false,
        success: function (data) {
            data = JSON.parse(data);

            const modalContent = $('#marks .modal-content');

            modalContent.html('');
            modalContent.append("<div class='row'><div class='col-md-12 d-flex justify-content-space-between'><h1 style='margin: 0'>Note de l'étudiant " + data.student.firstname + ' ' + data.student.lastname.toUpperCase() + "</h1> <span class='close'><i class=\"fa fa-times-circle fa-2x\" aria-hidden=\"true\"></i></span></div></div><div class='row'><div class='col-md-12'><table><thead><tr><th>Matière</th><th>Note</th><th>Coefficient</th></tr></thead><tbody id='marks-tbody'></tbody></table></div></div>");

            delete data.student;
            console.log(data);

            $.each(data, key => {
                $("#marks-tbody").append('<tr>' +
                    '<td>' + data[key].subject.name + ' (' + data[key].teacher.firstname.substring(0,1).toUpperCase() + '. ' + data[key].teacher.lastname.toUpperCase() + ')</td>' +
                    '<td>' + data[key].note + '/20</td>' +
                    '<td> Coefficient ' + data[key].coeff + '</td>' +
                    '</tr>');
            });


            $("#marks").modal('show')

            modalContent.append('');
        }
    })
})