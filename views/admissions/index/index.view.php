<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Espace Admission</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-group btn-dark-purple spacearound" href="<?= $this->getUrl('Admissions', 'register') ?>">Ajouter un élève</a>
        <p class="spacearound" id="studentsCount">Nombre d'élèves inscrit : <?= $studentsCount ?></p>
    </div>
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prénom</th>
                <th class="displayMobile">Email</th>
                <th class="displayMobile">Status</th>
                <th class="displayMobile">Archiver</th>
                <th class="displayMobile">Refuser</th>
                <th class="displayMobile">Attente</th>
                <th class="displayMobile">Accepter</th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($students as $student): ?>
                <tr>
                    <td class="id"><?= $student->getId() ?></td>
                    <td><?= htmlspecialchars_decode($student->getLastname()) ?></td>
                    <td><?= htmlspecialchars_decode($student->getFirstname()) ?></td>
                    <td><?= htmlspecialchars_decode($student->getEmail()) ?></td>
                    <td>
                        <?php
                        switch($student->getState()){
                            case 1:
                                echo "Awaiting";
                                break;
                            case 2:
                                echo "Denied";
                                break;
                            case 3:
                                echo "Archived";
                                break;
                            case 0:
                                echo "Accepted";
                                break;
                            default:
                                echo "unknown";
                                break;
                        } ?>
                    </td>
                    <td><a class="btn btn-block btn-cold-purple"
                           href="<?= $this->getUrl('Admissions', 'archive', ["id" => $student->getId()]) ?>">Archiver</a></td>
                    <td class="displayMobile"><a class="btn btn-block btn-cold-purple"
                           href="<?= $this->getUrl('Admissions', 'deny', ["id" => $student->getId()]) ?>">Refuser</a></td>
                    <td class="displayMobile"><a class="btn btn-block btn-cold-purple"
                           href="<?= $this->getUrl('Admissions', 'await', ["id" => $student->getId()]) ?>">Attente</a></td>
                    <td class="displayMobile"><a class="btn btn-block btn-cold-purple"
                           href="<?= $this->getUrl('Admissions', 'accept', ["id" => $student->getId()]) ?>">Accepter</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>