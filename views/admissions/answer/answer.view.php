<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Réponse à <?= $message->getEmail() ?></h1>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Message reçu : </h3>
        <p><?php echo htmlspecialchars_decode($message->getMessage()) ?></p>
    </div>
</div>

<?php $form->startForm(); ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->label('object') ?><br>
        <?= $form->field('object') ?>
    </div>
    <div class="col-md-12">
        <?= $form->label('answer') ?>
        <?= $form->field('answer') ?>
    </div>
    <?= $form->field('submit')  ?>

</div>

<?php $form->endForm(); ?>
