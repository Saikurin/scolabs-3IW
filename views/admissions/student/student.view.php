<div class="text-center">
    <section>
        <h1><?= $student->getFirstname() . ' ' . $student->getLastname() ?></h1>
        <h3><?= 'Né(e) le ' . $student->getDob() . ' à ' . $student->getPob() ?></h3>
        <aside class="text-left">
            <p>Adresse : <?= $student->getAddress() ?></p>
            <p>Code postal : <?= $student->getZipCode() ?></p>
            <p>Ville : <?= $student->getCity() ?></p>
        </aside>
        <aside class="text-right">
            <p>Email : <?= $student->getEmail() ?></p>
            <p>Date de création : <?= $student->getCreatedAt() ?></p>
            <p>Status : <?= $student->getStateString() ?></p>
        </aside>
    </section>
    <section>
        <?php foreach($parents as $parent): ?>
        <aside>
            <h1><?= $parent->getFirstname() . ' ' . $parent->getLastname() ?></h1>
            <h3><?= 'Né(e) le ' . $parent->getDob() . ' à ' . $parent->getPob() ?></h3>
            <p>Adresse : <?= $parent->getAddress() ?></p>
            <p>Code postal : <?= $parent->getZipCode() ?></p>
            <p>Ville : <?= $parent->getCity() ?></p>
            <p>Email : <?= $parent->getEmail() ?></p>
            <p>Date de création : <?= $parent->getCreatedAt() ?></p>
            <p>Status : <?= $parent->getStateString() ?></p>
        </aside>
        <?php endforeach; ?>
    </section>
</div>