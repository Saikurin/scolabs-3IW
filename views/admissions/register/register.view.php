<?php

use Scolabs\Core\Helpers;

?>

<?php if (isset($error)): ?>
    <p><?= $error ?></p>
<?php endif ?>


<div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1>Enregistrer un élève</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('lastname') ?><br>
            <span class="input "><?= $form->field('lastname') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('firstname') ?><br>
            <span class="input  "><?= $form->field('firstname') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('dob') ?><br>
            <span class="input  "><?= $form->field('dob') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('pob') ?><br>
            <span class="input  "><?= $form->field('pob') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('email') ?><br>
            <span class="input  "><?= $form->field('email') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('address') ?><br>
            <span class="input  "><?= $form->field('address') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('city') ?><br>
            <span class="input  "><?= $form->field('city') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('zipcode') ?><br>
            <span class="input  "><?= $form->field('zipcode') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('phoneNumber') ?><br>
            <span class="input  "><?= $form->field('phoneNumber') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <label for="classroom">Affectation de classe</label>
            <select name="classroom" id="classroom" class="form-control">
                <?php foreach ($classrooms as $c): ?>
                    <option value="<?= $c->getId() ?>"><?= $c->getName() ?> (<?= $c->getLevel() ?>)</option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="col-md-12 text-center spacearound form-group borderinput">
            <label for="selectParent1">Responsable légal 1 (s'il existe)</label>
            <select name="selectParent1" id="selectParent1" class="form-control">
                <?php foreach ($parents as $p): ?>
                    <option id="<?= $p->getId() ?>"> <?= $p->getFirstname() . ' ' . $p->getLastname() ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <label for="selectParent2">Responsable légal 2 (s'il existe)</label>
            <select name="selectParent2" id="selectParent2" class="form-control">
                <?php foreach ($parents as $p): ?>
                    <option id="<?= $p->getId() ?>"> <?= $p->getFirstname() . ' ' . $p->getLastname() ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>

    <div class="col-md-12 text-center">
        <h2>Parent 1</h2>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentLastname1') ?><br>
        <span class="input  "><?= $form->field('parentLastname1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentFirstname1') ?><br>
        <span class="input  "><?= $form->field('parentFirstname1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentPob1') ?><br>
        <span class="input  "><?= $form->field('parentPob1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentDob1') ?><br>
        <span class="input  "><?= $form->field('parentDob1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentEmail1') ?><br>
        <span class="input  "><?= $form->field('parentEmail1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentAddress1') ?><br>
        <span class="input  "><?= $form->field('parentAddress1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentCity1') ?><br>
        <span class="input  "><?= $form->field('parentCity1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentZipcode1') ?><br>
        <span class="input  "><?= $form->field('parentZipcode1') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentPhoneNumber1') ?><br>
        <span class="input  "><?= $form->field('parentPhoneNumber1') ?></span>
    </div>
    <div class="col-md-12 text-center">
        <h2>Parent 2</h2>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentLastname2') ?><br>
        <span class="input  "><?= $form->field('parentLastname2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentFirstname2') ?><br>
        <span class="input  "><?= $form->field('parentFirstname2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentPob2') ?><br>
        <span class="input  "><?= $form->field('parentPob2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentDob2') ?><br>
        <span class="input  "><?= $form->field('parentDob2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentEmail2') ?><br>
        <span class="input  "><?= $form->field('parentEmail2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentAddress2') ?><br>
        <span class="input  "><?= $form->field('parentAddress2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentCity2') ?><br>
        <span class="input  "><?= $form->field('parentCity2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentZipcode2') ?><br>
        <span class="input  "><?= $form->field('parentZipcode2') ?></span>
    </div>
    <div class="col-md-12 text-center spacearound form-group borderinput">
        <?= $form->label('parentPhoneNumber2') ?><br>
        <span class="input  "><?= $form->field('parentPhoneNumber2') ?></span>
    </div>

    <div class="col-md-12 text-center spacearound form-group borderinput">
        <label for="orphan">Orphelin / Emancipé</label>
        <input type="checkbox" id="orphan" name="orphan" value="orphan">
        <label for="oneParent">Un seul responsable légal</label>
        <input type="checkbox" id="oneParent" name="orphan" value="oneParent">
    </div>
    <div class="col-md-12 spacearound text-center">
        <?= $form->field('submit') ?>
    </div>
    <?php $form->endForm(); ?>



