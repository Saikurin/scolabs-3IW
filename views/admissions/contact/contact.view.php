<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Espace Contacts</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
            <tr>
                <th class="displayMobile">Id</th>
                <th class="displayMobile">Email</th>
                <th class="displayMobile">Message</th>
                <th class="displayMobile">Répondre</th>
                <th class="displayMobile">Supprimer</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($nonViewed as $nv): ?>
                <tr>
                    <td class="id">
                    <?php if ($nv->getViewed() == false){
                        echo '<div class="alert alert-danger">!</div>';
                        } else {
                            echo $nv->getId();
                        }
                    ?>
                    </td>
                    <td><?= $nv->getEmail() ?></td>
                    <td><?php echo htmlspecialchars_decode($nv->getMessage()); ?></td>
                    <td class="displayMobile"><a class="btn btn-block btn-cold-purple"
                                                 href="<?= $this->getUrl('Admissions', 'answerContact', ["id" => $nv->getId()]) ?>">Répondre</a></td>
                    <td class="displayMobile"><a class="btn btn-block btn-cold-purple"
                                                 href="<?= $this->getUrl('Admissions', 'deleteContact', ["id" => $nv->getId()]) ?>">Supprimer</a></td>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>