<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <img src="/public/img/logo.svg" width="100" height="100" alt="logo">
        </div>
        <div class="col-md-12 text-center">
            <h1>Modifier le logo</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('logo') ?><br>
            <span class="input "><?= $form->field('logo') ?></span>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>
    </div>
</div>