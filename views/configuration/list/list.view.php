<div class="row">
    <div class="col-md-12">
        <table>
            <thead>
            <tr>
                <th>Configuration</th>
                <th>Valeur</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($configurations as $configuration): ?>
                <tr>
                    <td><?= $configuration->getDescription() ?></td>
                    <td><?= $configuration->getValue() ?></td>
                    <td>
                        <a class="btn btn-dark-turquoise btn-block"
                           href="<?= $this->getUrl('Configuration', 'edit', ['id' => $configuration->getId()]) ?>">Modifier</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td>Logo du site</td>
                <td><img src="/public/img/logo.svg" width="50" height="50" alt="logo"></td>
                <td>
                    <a class="btn btn-dark-turquoise btn-block"
                       href="<?= $this->getUrl('Configuration', 'editlogo') ?>">Modifier</a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>