<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Edition d'une variable de configuration</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12 ">
        <form action="" method="POST">
            <input type="hidden" name="token" value="<?= md5($configuration->getDescription()) ?>">
            <label for="value" class="text-center"><?= $configuration->getDescription() ?></label>
            <input type="text" name="value" id="value" value="<?= $configuration->getValue() ?>"/>
            <input type="submit" value="Modifier" class="btn btn-dark-purple btn-block">
        </form>
    </div>
</div>