$.ajax({
    type: "GET",
    url: "/dashboard/ajax/books-available",
    success: function (data) {
        var authorize = data.authorize;
        console.log(data);
        data['books'].map(el => {

            if (authorize) {
                var appendHTML =
                    "<div class='col-md-3'>" +
                    "<div class='card'>" +
                    "<div class='card-image'>" +
                    "<img src='" + (el.informations.imageLinks.thumbnail ?? '') + "' alt=''>" +
                    "</div>" +
                    "<div class='card-body'>" +
                    "<h3>" + el.informations.title + " " + (el.informations.subtitle ?? '') + "</h3>" +
                    "</div>" +
                    "<div class='card-footer'>" +
                    "<form method='post' action='/dashboard/reserve-book'>" +
                    "<input type='hidden' name='token' value='" + el.token + "'>" +
                    "<button class='text-center btn btn-dark-purple' type='submit'>Réserver ce livre</button>" +
                    "</form>" +
                    "</div>" +
                    "</div>" +
                    "</div>"
            } else {
                var appendHTML =
                    "<div class='col-md-3'>" +
                    "<div class='card'>" +
                    "<div class='card-image'>" +
                    "<img src='" + (el.informations.imageLinks.thumbnail ?? '') + "' alt=''>" +
                    "</div>" +
                    "<div class='card-body'>" +
                    "<h3>" + el.informations.title + " " + (el.informations.subtitle ?? '') + "</h3>" +
                    "</div>" +
                    "</div>" +
                    "</div>"
            }

            $(".books").append(appendHTML);
        });
    }
})

$(".reserve").on('click', function (event) {
    event.preventDefault();
    console.log('reserve')
})