<?php use Scolabs\Core\Auth;

?>
<div class="row">
    <?php if (Auth::hasAccessTo('Book', 'list')): ?>
        <div class="col-md-12">
            <a class="btn btn-dark-purple" href="<?= $this->getUrl("Book", "list") ?>">Tous les livres</a>
        </div>
    <?php endif; ?>
    <div class="col-md-12">
        <h1 class="text-center">Livres disponible au CDI</h1>
        <small>Les livres ne sont présent qu'en un seul exemplaire</small>
    </div>
</div>
<h5>Le temps de reservation d'un livre est de <?= $maxDays ?> jours</h5>
<div class="row books"></div>