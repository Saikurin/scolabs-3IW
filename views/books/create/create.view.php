<?php
if (isset($error)): ?>
<div class="alert alert-danger">
    <?= $error ?>
</div>

<?php endif ?>

<div class="container">
    <div class="row displayflexnone spacearound ">
        <div class="col-md-12 text-center">
            <h1>Enregistrer un livre</h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('isbn') ?><br>
            <span class="input "><?= $form->field('isbn') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('price') ?><br>
            <span class="input  "><?= $form->field('price') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound ">
            <button class="btn btn-cold-purple" id="check">Verifier l'identité du livre</button>

        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>
<div id="checkInfosResult">
</div>