$(document).ready(function () {
    $("#classesTable").DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/French.json"
        },
        select: true
    });

    $(".delete").on('click', function (event) {
        console.log("here");
        $.ajax({
            type: "DELETE",
            url: $(this).data('url'),
            success: function (data) {
                window.location.reload();
            }
        });
    });
});

