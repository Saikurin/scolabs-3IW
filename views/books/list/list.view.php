<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des livres</h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple" href="<?= $this->getUrl("Book", "create") ?>">Ajouter un livre</a>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nom du livre</th>
                <th>auteur</th>
                <th>isbn</th>
                <th>status</th>
                <th></th>
                <th></th>

            </tr>
            </thead>
            <tbody>
            <?php foreach ($books as $book): ?>
                <tr>
                    <td class="id"><?= $book->getId(); ?></td>
                    <td><?= $book->getInformations()->title; ?></td>
                    <td><?= implode(", ", $book->getInformations()->authors); ?></td>
                    <td><?= htmlspecialchars($book->getIsbn()); ?></td>
                    <td><?= htmlspecialchars($book->getStatus()); ?></td>
                    <td><a class="btn btn-dark-turquoise" href="<?= $this->getUrl("Book", "edit", ["id" => $book->getId()]) ?>">Modifier</a>
                    </td>
                    <td>
                        <form action="" method="post">
                            <input type="hidden" name="validation" value="<?= $book->getId() ?>">
                            <input type="hidden" name="token" value="<?= md5($book->getIsbn()) ?>">
                            <input type="submit" value="Supprimer" class="delete btn btn-dark-purple">
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
