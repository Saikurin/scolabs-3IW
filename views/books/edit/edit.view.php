    <div class="row displayflexnone spacearound">
        <div class="col-md-12 text-center">
            <h1>Modifier le livre : <?= $book->getInformations()->title ?></h1>
        </div>
        <hr>
        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('isbn') ?><br>
            <span class="input "><?= $form->field('isbn') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('price') ?><br>
            <span class="input  "><?= $form->field('price') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound ">
            <button type="button" id="check" class="btn btn-dark-purple">Vérifier les infos</button>

        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>
    </div>

    <div id="checkInfosResult">
    </div>