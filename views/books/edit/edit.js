$("#check").on('click', function(event) {
    event.preventDefault();
    var valueOfISBN = $("#isbn").val().replace(/-/g, "").trim();
    if(valueOfISBN) {
        $.ajax({
            type: "GET",
            url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + valueOfISBN,
            success: function(data) {
                let divResult = document.getElementById("checkInfosResult");
                divResult.className = "text-center";
                if(data.totalItems === 1) {
                    var informations = data.items[0].volumeInfo
                    $("#submit").prop("disabled", false);
                    divResult.innerHTML = '<img src="' + informations["imageLinks"]["thumbnail"] + '" alt="' + informations["title"] + '"/>'
                        + '<h1>' + informations["title"] + '</h1>'
                        + '<h2>' + informations["authors"][0] + '</h2>'
                        + '<p>' + informations["description"] + '</p>';
                } else {
                    let divError = document.getElementById("checkInfosResult");
                    divError.classList.add("alert");
                    divError.classList.add("alert-danger");
                    divError.innerHTML = "L'ISBN est incorrect";
                }
            }
        })
    } else {
       let divError = document.getElementById("checkInfosResult");
       divError.classList.add("alert");
       divError.classList.add("alert-danger");
       divError.innerHTML = "L'ISBN est vide";
    }
})