<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des classes</h1>
    </div>
</div>
<hr>
<?php if(\Scolabs\Core\Auth::hasAccessTo('Classrooms','add')): ?>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple add-button" href="<?= $this->getUrl("Classrooms", "add") ?>">
            <i class="fa fa-plus-circle" aria-hidden="true"></i>
            Ajouter une classe
        </a>
    </div>
</div>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Niveau</th>
                <?php if(\Scolabs\Core\Auth::hasAccessTo('Classrooms','edit')): ?>
                <th></th>
                <?php endif ?>
                <?php if(\Scolabs\Core\Auth::hasAccessTo('Classrooms','delete')): ?>
                <th></th>
                <?php endif ?>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($classrooms as $classroom) : ?>
                    <tr>
                        <td class="id"><?= $classroom->getId(); ?></td>
                        <td><?= htmlspecialchars($classroom->getName()); ?></td>
                        <td><?= htmlspecialchars($classroom->getLevel()); ?></td>
                        <td><a class="btn btn-dark-turquoise" href="<?= $this->getUrl("Classrooms", "edit", ["id" => $classroom->getId()]) ?>">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            Modifier
                        </a>
                        </td>
                        <td>
                            <form method="post" action="<?= $this->getUrl('Classrooms', 'delete', ["id" => $classroom->getId()]) ?>">
                                <input type="hidden" name="token" value="<?= md5($classroom->getName()) ?>">
                                <input class="btn btn-dark-purple" type="submit" value="&#128465; Supprimer">
                            </form>
                        </td>
                        <td><a class="btn btn-dark-purple" href="<?= $this->getUrl('Classrooms', 'listStudents', ["id" =>
                        $classroom->getId()]) ?>">Liste des élèves</a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>