<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des étudiants de la classe <?= $class->getName() ?></h1>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <table id="classesTable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) : ?>
                    <tr>
                        <td class="id"><?= $user->getId(); ?></td>
                        <td><?= htmlspecialchars($user->getFirstname()); ?></td>
                        <td><?= htmlspecialchars($user->getLastname()); ?></td>
                        <td><?= htmlspecialchars($user->getPhone()); ?></td>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>