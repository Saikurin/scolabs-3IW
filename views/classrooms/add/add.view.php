<?php

use Scolabs\Core\Helpers;

?>

<?php if (isset($error)) : ?>
    <p><?= $error ?></p>
<?php endif ?>
<div class="row">
    <div class="col-md-12">
        <a class="btn btn-dark-purple" href="<?= Helpers::getUrl("Classrooms", "index") ?>">
            < Retour</a> </div> </div> <div>
                <div class="row displayflexnone">
                    <div class="col-md-12 text-center">
                        <h1>Ajouter une classe</h1>
                    </div>
                    <hr>
                    <div class="form-container">
                        <?php $form->startForm(); ?>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('name') ?><br></label>
                            <div class="input borderinput"><?= $form->field('name') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('level') ?><br></label>
                            <div class="input borderinput"><?= $form->field('level') ?></div>
                        </div>
                        <div class="text-center spacearound form-group">
                            <label><?= $form->label('students[]') ?><br></label>
                            <div class="input borderinput"><?= $form->field('students[]') ?></div>
                        </div>
                        <div class="spacearound text-center">
                            <?= $form->field('submit') ?>
                        </div>
                        <?php $form->endForm(); ?>
                    </div>
                </div>

    </div>