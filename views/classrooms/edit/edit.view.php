<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Modifier la classe : <?= $classroom->getName() ?></h1>
    </div>
</div>
<hr>
<div class="container">
    <div class="row displayflexnone spacearound ">

        <?php $form->startForm(); ?>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('name') ?><br>
            <span class="input "><?= $form->field('name') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('level') ?><br>
            <span class="input  "><?= $form->field('level') ?></span>
        </div>
        <div class="col-md-12 text-center spacearound form-group borderinput">
            <?= $form->label('students[]') ?><br>
            <span class="input  "><?= $form->field('students[]') ?></span>
        </div>
        <div class="col-md-12 spacearound text-center">
            <?= $form->field('submit') ?>
        </div>
        <?php $form->endForm(); ?>

    </div>

</div>