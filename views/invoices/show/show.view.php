<div class="row">
    <div class="col-md-4">
        <a href="<?= $this->getUrl('Invoices','list') ?>" id="back" class="btn btn-dark-purple">Retour à la liste des factures</a>
    </div>
    <div class="col-md-4 offset-md-4">
        <button class="btn btn-dark-purple" id="print" onclick="window.print()">Imprimer</button>
    </div>
</div>
<div class="invoice-box mt-5 mb-5">
    <table cellpadding="0" cellspacing="0">
        <tr class="top">
            <td colspan="12">
                <table>
                    <tr>
                        <td class="title">
                            <img src="/assets/img/logo.svg" style="width:100%; max-width:300px;">
                        </td>

                        <td>
                            Facture numéro #: <?= $invoice->getId() ?><br>
                            Crée le : <?= date('d/m/Y H:i:s', strtotime($invoice->getCreatedAt())) ?><br>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="information">
            <td colspan="12">
                <table>
                    <tr>
                        <td>
                            <?= $school_name ?><br>
                            <?= $address ?><br>
                            <?= $zip_code.','.$city ?><br>
                        </td>

                        <td>
                            <?= $invoice->getCustomer()->getFirstname() . " " . strtoupper($invoice->getCustomer()->getLastname()) ?>
                            <br>
                            <?= $invoice->getCustomer()->getAddress() . ", " . $invoice->getCustomer()->getZipCode() . " ". $invoice->getCustomer()->getCity() ?>
                            <br>
                            <?= $invoice->getCustomer()->getPhone() ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td>Article</td>
            <td>Prix Unitaire TTC</td>
            <td>Quantité</td>
            <td>Prix Total TTC</td>
        </tr>

        <?php $total = 0;
        foreach ($products as $invoice): $total += round(($invoice->getProduct()->getUnitPrice() * $invoice->getQuantity()) * ($invoice->getVat() / 100 + 1), 2); ?>
            <tr class="item <?= (!next($products)) ? "last" : "" ?>">
                <td><?= $invoice->getProduct()->getName() ?></td>
                <td><?= $currencySymbol . " " . round(($invoice->getProduct()->getUnitPrice()) * ($invoice->getVat() / 100 + 1), 2) ?>
                <td><?= $invoice->getQuantity() ?></td>
                <td><?= $currencySymbol . " " . round(($invoice->getProduct()->getUnitPrice() * $invoice->getQuantity()) * ($invoice->getVat() / 100 + 1), 2) ?>
                </td>
            </tr>
        <?php endforeach; ?>

        <tr class="total">

            <td class="bordercolorwhite">
                Total: <?= $currencySymbol . $total ?>
            </td>
        </tr>
    </table>
</div>