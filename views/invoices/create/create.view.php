<div class="row">
    <h1 class="text-center">Création de la facture numéro <?= $number ?></h1>
</div>
<hr>

<form action="" method="post">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="chooseStudent">Etudiant</label>
                <select name="student" id="chooseStudent" class="form-control">
                    <option></option>
                    <?php foreach ($students as $student): ?>
                        <option value="<?= $student->getId() ?>"><?= strtoupper($student->getLastName()) ?>  <?= $student->getFirstname() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="chooseParent">Payeur (si différent)</label>
                <select name="parent" id="chooseParent" class="form-control">
                    <option value=""></option>
                    <?php foreach ($parents as $parent): ?>
                        <option value="<?= $parent->getId() ?>"><?= strtoupper($parent->getLastName()) ?>  <?= $parent->getFirstname() ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <select id="products">
                <?php foreach ($products as $product): ?>
                    <option value="<?= $product->getId() ?> - <?= $product->getName() ?> - <?= $product->getUnitPrice() ?>"><?= $product->getName() ?>
                        (<?= floatval($product->getUnitPrice()) ?> €)</option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-3">
            <button class="btn btn-dark-purple" type="button" id="addProduct">Ajouter le produit</button>
        </div>
    </div>
    <h4>TVA par défault : <span id="vat"><?= $vat ?></span>%</h4>
    <table id="productsTableInvoices">
        <thead>
        <tr>
            <th>Name</th>
            <th>Quantité</th>
            <th>Prix Unitaire HT</th>
            <th>TVA</th>
            <th>Total HT</th>
            <th>Total TTC</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot class="text-center">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>0,00€</td>
            <td>0,00€</td>
            <td></td>
        </tr>
        </tfoot>
    </table>
    <button type="submit" class="btn btn-dark-purple">Création de la facture</button>
</form>
