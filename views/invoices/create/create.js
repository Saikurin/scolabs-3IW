const student = $("#chooseStudent").select2({
    placeholder: 'Choisir un étudiant',
    allowClear: true,
});

const parent = $("#chooseParent").select2({
    placeholder: 'Merci de choisir un étudiant',
    allowClear: true,
});

const products = $("#products").select2({
    placeholder: "Choisir un produit"
});

const table = $("#productsTableInvoices").DataTable({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/French.json"
    },

})

function uploadTotal() {
    let total = 0;
    let totalTTC = 0;

    table.rows().every(function (rowIndex, tableLoop, rowLoop) {
        let row = $(this.node());

        let quantity = row[0].childNodes[1].childNodes[0].value;
        let price = row.find('td').eq(2)[0].textContent;
        let vat = row[0].childNodes[3].childNodes[0].value;

        total += (quantity * price);
        totalTTC += ((quantity * price) + (quantity * price * vat / 100));

        // Total HT per article
        row[0].childNodes[4].childNodes[0].textContent = (quantity * price).toFixed(2) + "€";
        // Total TTC per article
        row[0].childNodes[5].childNodes[0].textContent = ((quantity * price) + (quantity * price * vat / 100)).toFixed(2) + "€";
    });

    table.column(4).footer().textContent = total.toFixed(2) + "€";
    table.column(5).footer().textContent = totalTTC.toFixed(2) + "€";
}

$("#addProduct").on('click', function () {
    const VAT = [20, 10, 5.5, 2.1];
    VAT.splice(VAT.indexOf($("#vat").val()), 1);

    const optionsVAT = VAT.map(el => {
        return '<option value="' + el + '">' + el + '</option>'
    });

    const currentArticle = products.val().split(' - ');
    /**
     * 0 => ID
     * 1 => Nom
     * 2 => Prix
     */

    $("#products option[value='" + products.val() + "']").remove();
    table.row.add([
        currentArticle[1] + '<input type="hidden" name="ids[]" value="' + currentArticle[0] + '"/>',
        '<input value="1" name="quantities[]" type="number"/>',
        currentArticle[2],
        '<select name="vat[]" class="vat">' +
        optionsVAT +
        '</select>',
        currentArticle[2],
        (parseFloat(currentArticle[2]) + (parseFloat(currentArticle[2]) * parseInt($("#vat").html()) / 100)).toFixed(2),
        '<button type="button" class="btn btn-dark-purple"><i class="fa fa-trash fa-2x"></i></button>'
    ]).draw(false);

    $(".vat").on('change', function () {
        $(this).parent().parent()[0].childNodes[5].textContent =
            (parseFloat(
                $(this).parent().parent()[0].childNodes[2].textContent
            ) +
            ((parseFloat(
                    $(this).parent().parent()[0].childNodes[2].textContent) *
                    parseFloat($(this).val()) / 100)
            )).toFixed(2);
        uploadTotal();
    })

    $("table tbody tr td input").on('input', function () {
        let quantity = $(this).val();
        if (parseInt(quantity) <= 0) {
            $(this).val(1);
        }
        uploadTotal()
    })

    $("table tbody tr td button").on('click', function () {
        table.row($(this).parents('tr')).remove().draw();
        uploadTotal();
    })

    uploadTotal()
});