<?php if(\Scolabs\Core\Auth::hasAccessTo('Invoices','create')): ?>
<div class="row">
    <div class="col-md-12">
        <h1 class="text-center">Liste des factures</h1>
    </div>
</div>
    <hr>
<div class="row">
    <a href="<?= $this->getUrl('Invoices', 'create') ?>" class="btn btn-dark-purple">Créer une facture</a>
</div>
<?php endif ?>

<table id="invoices" class="mt-5">
    <thead>
    <tr>
        <th>Facture numéro</th>
        <th>Créé le</th>
        <th>Eleve</th>
        <th>Payeur</th>
        <th>Etat</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($invoices as $invoice): ?>

        <tr>
            <td>#<?= $invoice->getId() ?></td>
            <td><?= date('d/m/Y H:i:s', strtotime($invoice->getCreatedAt())) ?></td>
            <td><?= $invoice->getStudent()->getFirstname() . ' ' . strtoupper($invoice->getStudent()->getLastname()) ?></td>
            <td><?= $invoice->getCustomer()->getFirstname() . ' ' . strtoupper($invoice->getCustomer()->getLastname()) ?></td>
            <td><?php switch ($invoice->getState()): case 1: ?>Payé<?php break; ?>
                <?php case 2: ?>Archivé<?php break; ?>
                <?php case 3: ?>Annulée<?php endswitch; ?></td>
            <td>
                <a href="<?= $this->getUrl('Invoices', 'show', ['id' => $invoice->getId()]) ?>"
                   class="btn btn-dark-purple">Voir la facture</a>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
</table>