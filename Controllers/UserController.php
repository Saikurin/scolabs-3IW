<?php

namespace Scolabs\Controllers;

use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Managers\UserManager;
use Scolabs\Core\View;
use Scolabs\Core\Helpers;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\FormBuilder;
use Scolabs\Managers\RoleManager;
use Scolabs\Models\User;
use Scolabs\Models\Role;
use Scolabs\Core\Mailer;
use PHPMailer\PHPMailer\Exception;
use Scolabs\Core\Exceptions\NotFoundException;


/**
 * Class UserController
 * @package Scolabs\controllers
 */
class UserController
{
    use UrlTrait;
    use RequestTrait;

    /**
     * @var UserManager
     */
    private $manager;

    /**
     * @var RoleManager
     */

    private $roleManager;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->manager = new UserManager();
        $this->roleManager = new RoleManager();
    }

    public function listAction()
    {
        $view = new View("users.list", "admin");
        $view->assign("users", $this->manager->getAllUsersWithRole());
        $view->addJS('datatables.min.js');
    }

    public function addAction(){

        $form = new FormBuilder("POST", $this->getUrl('User', 'add'));
        $view = new View("users.add", "admin");
        $roles = [];
        foreach ($this->roleManager->findAll() as $key => $role) {
            $roles[] = [
                'selected' => false,
                'value' => $role->getId(),
                'text' => $role->getName()
            ];
        }

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'name',
            'class' => 'form-control',
            'id' => 'name',
            'label' => [
                'class' => '',
                'value' => 'Nom'
            ]
        ], [
            'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
            'error' => 'Le nom ne peut-être vide'
        ]);
        
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'firstname',
            'class' => 'form-control',
            'id' => 'firstname',
            'label' => [
                'class' => '',
                'value' => 'Prénom'
            ]
        ], [
            'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
            'error' => 'Le prénom ne peut-être vide'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'date',
            'name' => 'dob', 
            'class' => 'form-control',
            'id' => 'dob',
            'placeholder' => 'Date de naissance',
            'label' => [
                'value' => 'Date de naissance'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'email',
            'name' => 'email',
            'class' => 'form-control',
            'id' => 'email',
            'placeholder' => 'Adresse e-mail',
            'label' => [
                'value' => 'Adresse e-mail'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'tel',
            'name' => 'phone',
            'class' => 'form-control',
            'id' => 'phone',
            'placeholder' => '0612345678',
            'label' => [
                'value' => 'Numéro de téléphone'
            ]
        ], [
            'regex' => '(0|(\\+33)|(0033))[1-9][0-9]{8}',
            'error' => 'Numéro de téléphone incorrect'
        ]);

        $form->addField(
            FormBuilder::SELECT,
            [
                'name' => 'role[]',
                'id' => 'role',
                'label' => [
                    'value' => 'Role'
                ],
                'values' => $roles,
                'multiple' => false,
                'class' => 'd-block w-100'
            ]
        );

        $form->addField(
            FormBuilder::SELECT,
            [
                'name' => 'state[]',
                'id' => 'state',
                'label' => [
                    'value' => 'Etat'
                ],
                'values' => [
                    [
                        'selected' => 'true',
                        'value' => '0',
                        'text' => 'Activé'
                    ],
                    [
                        'selected' => 'false',
                        'value' => '1',
                        'text' => 'En attente',
                    ],
                    [
                        'selected' => 'false',
                        'value' => '2',
                        'text' => 'Refusé',
                    ],
                    [
                        'selected' => 'false',
                        'value' => '',
                        'text' => 'Archivé',
                    ],
                ],
                'multiple' => false,
                'class' => 'd-block w-100'
            ]
        );

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Ajouter',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                if (!is_null($this->manager->findOneBy(['email' => $this->getFromPost('email')])->getId())) {
                    throw new NotFoundException("L'email saisi est déjà utilisé");
                } else {
                    $user = new User();
                    $role = new Role();
                    $password = Helpers::generateRandomString(8);
                    $user->setLastname($this->getFromPost('name'));
                    $user->setFirstname($this->getFromPost('firstname'));
                    $user->setPassword(password_hash($password, PASSWORD_BCRYPT));
                    $username = Helpers::generateUsername($user->getFirstname(), 
                    $user->getLastname());
                    $user->setUsername($username);
                    $user->setDob($this->getFromPost('dob'));
                    $user->setPhone($this->getFromPost('phone'));
                    $user->setEmail($this->getFromPost('email'));
                    $newRole = $this->roleManager->findBy(['id' => $this->getFromPost('role')[0]]);
                    $role->setId($newRole[0]['Role__id']);
                    $role->setName($newRole[0]['Role__name']);
                    $role->setSlug($newRole[0]['Role__slug']);
                    $user->setRole($role);
                    $user->setCreatedAt(date('Y-m-d H:i:s'));
                    $user->setState($this->getFromPost('state')[0]);
                    $this->manager->save($user);
                    $this->generateMail($user->getRole()->getName(), $user->getEmail(), $user->getFirstname(), $user->getLastname(), $user->getUsername(), $password);
                    header('Location: ' . $this->getUrl('User', 'list'));
                }
            }
        }
        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');
        $view->assign('form', $form);
    }

    public function deleteAction(int $id)
    {
        if ($this->isPost()) {
            /** @var User $user */
            $user = $this->manager->find($id);
            $this->manager->delete($id);
            Helpers::redirect($this->getUrl('User', 'list'));
        }
        throw new NotAllowedException('Unauthorized');
    }
    
    private function generateMail($role, $email, $firstname, $lastname, $username, $password){
        $mailer = new Mailer();

        $mailer->withTemplate("register",
            [
                "role" => $role,
                "email" => $email,
                "firstname" => $firstname,
                "lastname" => $lastname,
                "username" => $username,
                "password" => $password
            ]
        );

        $subject = "Inscription " . $role . " sur Scolabs";

        try {
            $mailer->send("nomLycee@scolabs.fr", $subject, [$email]);
        } catch (Exception $e) {
            echo "Erreur";
        }
    }
}
