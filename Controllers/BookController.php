<?php


namespace Scolabs\Controllers;

use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\Exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Helpers;
use Scolabs\Core\Request;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\SecureTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\BookBorrowingManager;
use Scolabs\Managers\BookManager;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Book;
use Scolabs\Models\Configuration;
use Scolabs\Models\User;

/**
 * Class BookController
 * @package Scolabs\Controllers
 */
class BookController
{
    use UrlTrait;
    use RequestTrait;
    use SecureTrait;

    /**
     * @var BookManager
     */
    private $manager;

    /**
     * @var BookBorrowingManager
     */
    private $bookBorrowingManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * BookController constructor.
     */
    public function __construct()
    {
        $this->manager = new BookManager();
        $this->bookBorrowingManager = new BookBorrowingManager();
        $this->configurationManager = new ConfigurationManager();
        $this->userManager = new UserManager();
    }

    /**
     * @throws NotFoundException
     */
    public function listAction()
    {
        if ($this->isPost()) {
            $bookId = $this->getFromPost('validation');
            $bookIsbn = $this->getFromPost('token');
            /** @var Book $book */
            $book = $this->manager->find($bookId);
            if (is_int($book->getId()) && md5($book->getIsbn()) === $bookIsbn) {
                $this->manager->delete($bookId);
                header('Location:' . $this->getUrl('Book', 'list'));
            }
        }
        $view = new View('books.list', 'admin');
        $books = $this->manager->findAll();
        foreach ($books as $key => $book) {
            $curl = curl_init("https://www.googleapis.com/books/v1/volumes?q=isbn:" . $book->getIsbn());
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 80);
            $response = (array)json_decode(curl_exec($curl));
            $book->setInformations($response['items'][0]->volumeInfo);
        }
        $view->addJS('datatables.min.js');
        $view->assign('books', $books);
    }

    /**
     * @param int $id
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function editAction(int $id)
    {
        /** @var Book $book */
        $book = $this->manager->find($id);

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, "https://www.googleapis.com/books/v1/volumes?q=isbn:" . $book->getIsbn());
        curl_setopt($curl, CURLOPT_TIMEOUT, 80);
        $response = (array)json_decode(curl_exec($curl));
        $book->setInformations($response['items'][0]->volumeInfo);
        curl_close($curl);


        $form = new FormBuilder("POST", $this->getUrl('Book', 'edit', ['id' => intval($book->getId())]));
        $view = new View('books.edit', 'admin');

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'isbn',
            'class' => 'form-control',
            'id' => 'isbn',
            'placeholder' => 'ISBN',
            'value' => $book->getIsbn(),
            'label' => [
                'value' => 'ISBN'
            ]
        ], [
            'regex' => '^([0-9A-Za-z]{10,13})$',
            'error' => 'ISBN incorrect'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'price',
            'class' => 'form-control',
            'id' => 'price',
            'placeholder' => 'Prix du livre',
            'value' => $book->getPrice(),
            'label' => [
                'value' => 'Prix du livre'
            ]
        ], [
            'regex' => '^[+-]?([0-9]*[.])?[0-9]+$',
            'error' => 'Prix non valide'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Modifier',
            'name' => 'submit',
            'disabled' => true,
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $book->setIsbn($this->getFromPost('isbn'));
                $book->setPrice($this->getFromPost('price'));

                $this->manager->save($book);
                header('Location: ' . $this->getUrl('Book', 'list'));
            }
        }

        if (empty($book)) {
            throw new NotFoundException("Le livre n'existe pas");
        } else {
            $view->assign('book', $book);
            $view->assign('form', $form);
        }
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function createAction()
    {
        $view = new View('books.create', 'admin');
        $form = new FormBuilder('POST', $this->getUrl('Book', 'create'));

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'isbn',
            'class' => 'form-control',
            'id' => 'isbn',
            'placeholder' => 'ISBN',
            'label' => [
                'value' => 'ISBN'
            ]
        ], [
            'regex' => '^([0-9A-Za-z]{10,13})$',
            'error' => 'ISBN incorrect'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'price',
            'class' => 'form-control',
            'id' => 'price',
            'placeholder' => 'Prix du livre',
            'label' => [
                'value' => 'Prix du livre'
            ]
        ], [
            'regex' => '^[+-]?([0-9]*[.])?[0-9]+$',
            'error' => 'Prix non valide'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Ajouter',
            'id' => 'submit',
            'name' => 'submit',
            'disabled' => true
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $manager = new BookManager();

                $exist = $manager->count([
                    'isbn' => $this->getFromPost('isbn')
                ]);

                if ($exist > 0) {
                    $view->assign('error', 'Le livre existe déià');
                } else {
                    $book = new Book();
                    $book->setIsbn($this->getFromPost('isbn'));
                    $book->setPrice($this->getFromPost('price'));
                    $book->setStatus('DISPONIBLE');
                    $manager->save($book);
                    Helpers::redirect($this->getUrl('Book', 'list'));
                }
            }
        }

        $view->assign('form', $form);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteAction(int $id)
    {
        if ($this->isDelete()) {
            $this->manager->delete($id);
        }
        return true;
    }

    public function availableAction()
    {
        $view = new View('books.available', 'admin');
        $view->assign('maxDays', intval($this->configurationManager->findOneBy(['conf'=>'limit_borrowing_book'])->getValue()));
    }

    /**
     * @throws NotFoundException
     */
    public function reserveBookAction()
    {
        $decrypt = $this->decrypt($this->getFromPost('token'));
        $explodedDecrypt = explode(" - ", $decrypt);
        $id = $explodedDecrypt[0];
        /** @var Book $book */
        $book = $this->manager->find($id);
        if (!is_null($book)) {
            $response = (array)Request::get(
                "https://www.googleapis.com/books/v1/volumes",
                [
                    "q" => "isbn:" . $book->getIsbn(),
                ],
                false
            );
            if ($response['items'][0]->volumeInfo->title === $explodedDecrypt[1]) {
                /** @var Configuration $limit */
                $limit = $this->configurationManager->findOneBy(['conf' => 'limit_borrowing_book']);
                // TODO: Change to get current User
                /** @var User $currentUser */
                $currentUser = $this->userManager->find(1);
                $this->bookBorrowingManager->reserveBook(
                    $currentUser,
                    $book,
                    intval($limit->getValue())
                );
                $book->setStatus('NON DISPONIBLE');
                $this->manager->save($book);
                header('Location: ' . $this->getUrl('Book', 'available'));
            }
        }
    }


    /**
     * AJAX CALL
     */

    public function getAvailableBooksAction()
    {
        $manager = new BookManager();
        $books = $manager->getBooksAvailable();

        // TODO: Update it when get id user
        $idUser = 1;
        $numberOfBooksForUser = $this->bookBorrowingManager->count([
            'id_user' => $idUser
        ]);
        $numberMaxBorrowingBooks = intval($this->configurationManager->findOneBy(['conf'=>'limit_borrowing_book'])->getValue());


        $booksArray = ['books' => [], 'authorize' => $numberMaxBorrowingBooks > $numberOfBooksForUser];
        foreach ($books as $key => $book) {
            $response = (array)Request::get(
                "https://www.googleapis.com/books/v1/volumes",
                [
                    "q" => "isbn:" . $book->getIsbn(),
                ],
                false
            );

            $booksArray['books'][$key] = $book->__toArray();
            $booksArray['books'][$key]['informations'] = $response['items'][0]->volumeInfo;
            $booksArray['books'][$key]['token'] = $this->encrypt($book->getId() . " - " . $booksArray['books'][$key]['informations']->title);
        }

        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($booksArray);
    }

    public function getBooksAction()
    {
        $manager = new BookManager();
        $books = $manager->findAll();
        $booksArray = [];
        foreach ($books as $key => $book) {
            $response = (array)Request::get(
                "https://www.googleapis.com/books/v1/volumes",
                [
                    "q" => "isbn:" . $book->getIsbn(),
                ],
                false
            );

            $booksArray[$key] = $book->__toArray();
            $booksArray[$key]['informations'] = $response['items'][0]->volumeInfo;
        }

        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($booksArray);
    }
}
