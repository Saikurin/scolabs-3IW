<?php


namespace Scolabs\Controllers;

use DateInterval;
use DatePeriod;
use DateTime;
use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ClassroomManager;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\CourseManager;
use Scolabs\Managers\StudentClassroomManager;
use Scolabs\Managers\SubjectManager;
use Scolabs\Models\Configuration;
use Scolabs\Models\Course;
use Scolabs\Models\Studentclassrooms;
use Scolabs\Models\Subject;
use Scolabs\Models\User;

/**
 * Class RoutineController
 * @package Scolabs\controllers
 */
class RoutineController
{
    use UrlTrait;
    use RequestTrait;

    /**
     * @var ClassroomManager
     */
    private $classroomManager;

    /**
     * @var SubjectManager
     */
    private $subjectManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var CourseManager
     */
    private $courseManager;

    /**
     * @var StudentClassroomManager
     */
    private $studentClassroomManager;

    /**
     * ClassroomsController constructor.
     */
    public function __construct()
    {
        $this->classroomManager = new ClassroomManager();
        $this->subjectManager = new SubjectManager();
        $this->configurationManager = new ConfigurationManager();
        $this->courseManager = new CourseManager();
        $this->studentClassroomManager = new StudentClassroomManager();
    }

    public function listAction()
    {
        $view = new View('routine.list', 'admin');
        $minHour = $this->configurationManager->findOneBy(['conf' => 'min_hour'])->getValue();
        $maxHour = $this->configurationManager->findOneBy(['conf' => 'max_hour'])->getValue();
        $courses = [];

        if(Auth::getUser()->getRole()->getSlug() === 'teachers') {
            $subjects = $this->subjectManager->findBy(['id_teacher' => Auth::getUser()->getId()], [], Subject::class);

            foreach ($subjects as $subject) {
                foreach ($this->courseManager->findBySubjectWithRelation($subject->getId()) as $course) {
                    if (date('W', strtotime($course->getStart())) === date('W')) {
                        $courses[] = $course;
                    }
                }
            }
        } elseif (Auth::getUser()->getRole()->getSlug() === 'students') {
            $classroom = $this->studentClassroomManager->getClassroomByStudent(Auth::getUser()->getId());
            foreach ($this->courseManager->findByClassroomWithRelation($classroom->getId()) as $course) {
                if (date('W', strtotime($course->getStart())) === date('W')) {
                    $courses[] = $course;
                }
            }
        }

        $view->assign('hours', $this->differenceInHours($minHour, $maxHour));
        $view->assign('minHour', $minHour);
        $view->assign('maxHour', $maxHour);
        $view->assign('courses', $courses);
        $view->assign('days_in_week', $this->daysInWeek(date('W')));
    }

    /**
     * @param string $startdate
     * @param string $enddate
     * @return float|int
     */
    private function differenceInHours(string $startdate, string $enddate)
    {
        $starttimestamp = strtotime($startdate);
        $endtimestamp = strtotime($enddate);
        return abs($endtimestamp - $starttimestamp) / 3600;
    }

    /**
     * @param int $weekNum
     * @return array
     */
    private function daysInWeek(int $weekNum)
    {
        $result = array();
        $datetime = new DateTime('00:00:00');
        $datetime->setISODate((int)$datetime->format('o'), $weekNum, 1);
        $interval = new DateInterval('P1D');
        $week = new DatePeriod($datetime, $interval, 6);

        foreach ($week as $day) {
            $result[] = $day->format('Y-m-d');
        }
        return $result;
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function addAction()
    {
        $view = new View("routine.add", "admin");
        $form = new FormBuilder("POST", $this->getUrl("Routine", "add"));

        $form->addField(FormBuilder::SELECT, [
            'name' => 'classroom',
            'class' => 'form-control',
            'id' => 'classroom',
            'label' => [
                'class' => '',
                'value' => 'classe'
            ],
            'values' => array_map(function ($item) {
                return [
                    "selected" => false,
                    "value" => $item->getId(),
                    "text" => $item->getName()
                ];
            }, $this->classroomManager->findAll()),
            "multiple" => false
        ]);

        $form->addField('select', [
            'name' => 'subject',
            'class' => 'form-control',
            'id' => 'subject',
            'label' => [
                'class' => '',
                'value' => 'Matière'
            ],
            'values' => array_map(function ($item) {
                return [
                    "selected" => false,
                    "value" => $item->getId(),
                    "text" => $item->getName()
                ];
            }, $this->subjectManager->findBy([
                "id_teacher" => Auth::getUser()->getId()
            ], [], Subject::class)),
            "multiple" => false
        ]);

        setlocale(LC_TIME, "fr_FR");

        $form->addField(FormBuilder::SELECT, [
            'name' => 'day',
            'id' => 'day',
            'label' => [
                'value' => 'Jour du cours'
            ],
            'values' => array_map(function ($item) {
                return [
                    'selected' => 'false',
                    'value' => $item,
                    'text' => strftime('%A', strtotime($item))
                ];
            }, $this->daysInWeek(date('W'))),
            'multiple' => false
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'time',
            'name' => 'start',
            'class' => 'form-control',
            'id' => 'start',
            'label' => [
                'class' => '',
                'value' => 'Début du cours'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'time',
            'name' => 'end',
            'class' => 'form-control',
            'id' => 'end',
            'label' => [
                'class' => '',
                'value' => 'fin du cours'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'string',
            'name' => 'room',
            'class' => 'form-control',
            'id' => 'room',
            'label' => [
                'class' => '',
                'value' => 'Salle de cours'
            ]
        ]);


        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Ajouter',
            'name' => 'submit',
            'id' => 'submit'
        ]);


        if ($this->isPost()) {
            if ($form->isValid()) {

                $classroom = $this->classroomManager->find($this->getFromPost('classroom'));
                if(is_null($classroom->getId())) {
                    throw new RequiredMissingException('La classe n\'est pas renseignée correctement');
                }
                $subject = $this->subjectManager->find($this->getFromPost('subject'));
                if(is_null($subject->getId())) {
                    throw new RequiredMissingException('La matière n\'est pas renseignée correctement');
                }

                $course = new Course();
                $course->setStart($this->getFromPost('day'). ' '.$this->getFromPost('start'));
                $course->setEnd($this->getFromPost('day'). ' '.$this->getFromPost('end'));
                $course->setRoom($this->getFromPost('room'));
                $course->setClassroom($classroom);
                $course->setSubject($subject);
                $this->courseManager->save($course);

                header('Location: ' . $this->getUrl('Routine', 'list'));
            }
        }

        $view->assign("form", $form);
        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');
    }

    public function editAction(int $id)
    {
        /** @var Course $routine */
        $routine = $this->manager->find($id);

        $view = new View("routine.edit", "admin");
        $form = new FormBuilder("POST", $this->getUrl("Routine", "edit"));

        $form->addField('select', [
            'name' => 'classroom',
            'class' => 'form-control',
            'id' => 'classroom',
            'label' => [
                'class' => '',
                'value' => 'classe'
            ],
            'values' => array_map(function ($item) {
                return [
                    "selected" => false,
                    "value" => $item->getId(),
                    "text" => $item->getName()
                ];
            }, $this->classroomManager->findAll()),
            "multiple" => false
        ]);

        $form->addField('select', [
            'name' => 'subject',
            'class' => 'form-control',
            'id' => 'subject',
            'label' => [
                'class' => '',
                'value' => 'Matière'
            ],
            'values' => array_map(function ($item) {
                return [
                    "selected" => false,
                    "value" => $item->getId(),
                    "text" => $item->getName()
                ];
            }, $this->subjectManager->findBy([
                "id_teacher" => Auth::getUser()->getId()
            ], [], Subject::class)),
            "multiple" => false
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'datetime',
            'name' => 'start',
            'class' => 'form-control',
            'id' => 'start',
            'label' => [
                'class' => '',
                'value' => 'Début du cours'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'datetime',
            'name' => 'end',
            'class' => 'form-control',
            'id' => 'end',
            'label' => [
                'class' => '',
                'value' => 'fin du cours'
            ]
        ]);


        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Modifier',
            'name' => 'submit',
            'id' => 'submit'
        ]);


        if ($this->isPost()) {
            if ($form->isValid()) {
                $classroom->setName($this->getFromPost('name'));
                $classroom->setLevel($this->getFromPost('level'));
                $this->manager->save($classroom);
                foreach ($this->getFromPost('students') as $studentID) {
                    if (!$this->studentClassroomManager->findBy(['id_student' => $studentID])) {
                        $studentClassroom = new Studentclassrooms();
                        $studentClassroom->setClassroom($classroom);
                        /** @var User $student */
                        $student = $this->userManager->find($studentID);
                        $studentClassroom->setStudent($student);
                        $studentClassroom->setClassroom($classroom);
                        $this->studentClassroomManager->save($studentClassroom);
                    }
                }
                header('Location: ' . $this->getUrl('Routine', 'list'));
            }
        }

        if (empty($classroom)) {
            throw new NotFoundException("La classe n'existe pas");
        } else {
            $view->assign('classroom', $classroom);
            $view->assign('form', $form);
            $view->assign('students', $students);
            $view->addCSS('select2.min.css');
            $view->addCSS('select2-bootstrap.min.css');
            $view->addJS('select2.min.js');
        }
    }

    public function deleteAction(int $id)
    {
        if ($this->isPost()) {
            $routine = $this->manager->find($id);
            if ($routine && md5($routine->getName()) === $this->getFromPost('token')) {
                foreach ($this->studentClassroomManager->findByWithStudent($id) as $item) {
                    $this->studentClassroomManager->deleteByIdStudent($item->getId());
                }
                $this->manager->delete($id);
                Helpers::redirect($this->getUrl('Routine', 'list'));
            }
        }
        throw new NotAllowedException('Unauthorized');
    }
}
