<?php


namespace Scolabs\Controllers;

use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FileManager;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Models\Configuration;

/**
 * Class ConfigurationController
 * @package Scolabs\Controllers
 */
class ConfigurationController
{
    use UrlTrait, RequestTrait;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * ConfigurationController constructor.
     */
    public function __construct()
    {
        $this->configurationManager = new ConfigurationManager();
    }

    public function listAction()
    {
        $configurations = $this->configurationManager->findAll();
        $view = new View('configuration.list', 'admin');
        $view->assign('configurations', $configurations);
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function editAction(int $id)
    {
        /** @var Configuration $config */
        $config = $this->configurationManager->find($id);
        if ($this->isPost() && md5($config->getDescription()) === $this->getFromPost('token')) {
            $config->setValue($this->getFromPost('value'));
            $this->configurationManager->save($config);
            header('Location:' . $this->getUrl('Configuration', 'list'));
        }
        $view = new View('configuration.edit', 'admin');
        $view->assign('configuration', $config);
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function editlogoAction()
    {
        $form = new FormBuilder('POST', $this->getUrl('Configuration', 'editlogo'), true);
        $form->addField('input', [
            'type' => 'file',
            'id' => 'logo',
            'name' => 'logo',
            'label' => [
                'value' => 'Logo de l\'établissement'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Modifier',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid() && !empty($_FILES['logo']['name'])) {
                $fileupload = new FileManager();
                $fileupload->setInput('logo');

                $fileupload->setAllowedMimeTypes([
                    'image/svg+xml'
                ]);


                $fileupload->setDestinationFilename('logo');
                $fileupload->setDestinationDirectory(getcwd() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'img', true);

                $fileupload->allowOverwriting();
                $fileupload->save();

                chdir(DIRECTORY_ROOT);
                header('Location:' . $this->getUrl('Configuration', 'list'));
            }
        }
        $view = new View('configuration.editlogo', 'admin');
        $view->assign('form', $form);
    }
}
