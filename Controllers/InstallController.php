<?php


namespace Scolabs\Controllers;

use PDOException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Helpers;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Configuration;
use Scolabs\Models\User;

class InstallController
{
    use UrlTrait;
    use RequestTrait;

    public function indexAction()
    {
        new View('install.index', 'install');
    }

    /**
     * @throws NotSupportedException
     * @throws NotFoundException
     */
    public function bddAction()
    {
        $view = new View('install.bdd', 'install');

        $form = new FormBuilder("POST", $this->getUrl('Install', 'bdd'));

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'host',
            'name' => 'host',
            'value' => DB_HOST ?? '',
            'label' => [
                'value' => 'Hôte de la base de données'
            ],
            'class' => 'form-control'
        ], ['regex' => '^[\w]+$', 'error' => 'L\'hote saisie est vide']);


        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'port',
            'name' => 'port',
            'value' => DB_PORT ?? '',
            'label' => [
                'value' => 'Port'
            ],
            'class' => 'form-control'
        ], ['regex' => '^[0-9]+$', 'error' => 'Le port ne peut être valide']);


        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'dbname',
            'name' => 'dbname',
            'label' => [
                'class' => '',
                'value' => 'Nom de la base de donneés'
            ],
            'value' => DB_NAME ?? '',
            'class' => 'form-control'
        ], ['regex' => '^[\w]+$', 'error' => 'La base de donnée saisie est vide']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'user',
            'name' => 'user',
            'label' => [
                'class' => '',
                'value' => 'Nom d\'utilisateur'
            ],
            'value' => DB_USER ?? '',
            'class' => 'form-control'
        ], ['regex' => '^[\w]+$', 'error' => 'L\'utilisateur est obligatoire']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'password',
            'id' => 'password',
            'name' => 'password',
            'label' => [
                'class' => '',
                'value' => 'Mot de passe du compte'
            ],
            'class' => 'form-control'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'prefixe',
            'name' => 'prefixe',
            'label' => [
                'class' => '',
                'value' => 'Prefixe des tables'
            ],
            'placeholder' => 'sco_',
            'class' => 'form-control',
            'value' => DB_PREFIXE ?? '',
        ], ['regex' => '^[a-zA-Z_-]+$', 'error' => 'Le préfixe ne peut être que des lettres']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'name' => 'submit',
            'class' => 'btn-cold-purple',
            'value' => 'Sauvegarder',
        ]);

        $view->assign('form', $form);

        if ($this->isPost()) {
            if ($form->isValid()) {
                try {
                    new \PDO('mysql:host=' . $this->getFromPost('host') . ';dbname=' . $this->getFromPost('dbname'), $this->getFromPost('user'), $this->getFromPost('password'));
                    $text = file_get_contents(".env");
                    $lines = explode("\n", $text);
                    foreach ($lines as $key => $line) {
                        $data = explode("=", $line);
                        switch ($data[0]) {
                            case 'DB_PREFIXE':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('prefixe');
                                break;
                            case 'DB_USER':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('user');
                                break;
                            case 'DB_PWD':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('password');
                                break;
                            case 'DB_HOST':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('host');
                                break;
                            case 'DB_NAME':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('dbname');
                                break;
                            case 'DB_PORT':
                                $lines[$key] = $data[0] . "=" . $this->getFromPost('port');
                                break;
                            default:
                                break;
                        }
                    }

                    unlink(".env");
                    file_put_contents(".env", trim(implode("\n", $lines)));

                    $_SESSION['install']['bdd']['validate'] = true;
                    Helpers::redirect(Helpers::getUrl('Install', 'smtp'));
                } catch (PDOException $exception) {
                    $view->assign('errors', $exception->getMessage());
                }
            } else {
                $view->assign('errors', $form->getErrors());
            }
        }
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     */
    public function smtpAction()
    {
        $form = new FormBuilder('POST', $this->getUrl('Install', 'smtp'));
        $view = new View("install.smtp", "install");

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'text',
                'name' => 'smtp',
                'placeholder' => 'Serveur SMTP',
                'id' => 'smtp',
                'label' => [
                    'value' => 'Serveur SMTP'
                ]
            ],
            ['regex' => '^[\w]+$', 'error' => 'Le serveur SMTP ne respecte pas les règles de sécurité']
        );

        $form->addField(
            FormBuilder::INPUT,
            [

                'type' => 'number',
                'name' => 'port',
                'placeholder' => '25',
                'id' => 'port',
                'label' => [
                    'value' => 'Port'
                ],
                ['regex' => '^[0-9]+$', 'error' => 'Le Port SMTP est requis']
            ]
        );

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'text',
                'name' => 'user',
                'placeholder' => 'email@password.com',
                'id' => 'user',
                'label' => [
                    'value' => 'Nom d\'utilisateur'
                ],
                ['regex' => '^[a-zA-Z@.-_]+$', 'error' => 'Le nom d\'utilisateur ne respecte pas les règles de sécurité']
            ]
        );

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'password',
                'name' => 'password',
                'id' => 'password',
                'placeholder' => '*******',
                'label' => [
                    'value' => 'Mot de passe'
                ]
            ]
        );

        $form->addField(
            FormBuilder::SELECT,
            [
                'name' => 'secure',
                'id' => 'secure',
                'label' => [
                    'value' => 'Sécurité'
                ],
                'values' => [
                    [
                        'selected' => true,
                        'value' => 'tls',
                        'text' => 'TLS'
                    ],
                    [
                        'selected' => false,
                        'value' => 'ssl',
                        'text' => 'SSL'
                    ]
                ]
            ]
        );

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'submit',
                'id' => 'submit',
                'name' => 'submit',
                'class' => 'btn-cold-purple',
                'value' => 'Enregister'
            ]
        );

        if ($this->isPost()) {
            if ($this->getFromPost('submit')) {
                if ($form->isValid()) {
                    $text = file_get_contents(".env");
                    $lines = explode("\n", $text);
                    $lines[array_search('MAILER_URL', $lines)] = 'MAILER_URL=' . $this->getFromPost('smtp');
                    $lines[array_search('MAILER_PORT', $lines)] = 'MAILER_PORT=' . $this->getFromPost('port');
                    $lines[array_search('MAILER_USERNAME', $lines)] = 'MAILER_USERNAME=' . $this->getFromPost('user');
                    $lines[array_search('MAILER_PASSWORD', $lines)] = 'MAILER_PASSWORD=' . $this->getFromPost('password');
                    $lines[array_search('MAILER_SECURE', $lines)] = 'MAILER_SECURE=' . $this->getFromPost('secure');
                    unlink(".env");
                    file_put_contents(".env", trim(implode("\n", $lines)));
                    $_SESSION['install']['smtp']['validate'] = true;
                } else {
                    $view->assign('errors', $form->getErrors());
                }
            } else {
                $_SESSION['install']['smtp']['validate'] = true;
                Helpers::redirect($this->getUrl('Install', 'site'));
            }
        }

        $view->assign("form", $form);
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     */
    public function siteAction()
    {
        $view = new View('install.site', 'install');
        $form = new FormBuilder('POST', $this->getUrl('Install', 'site'));

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'text',
                'name' => 'title',
                'id' => 'title',
                'placeholder' => 'Titre du site',
                'label' => [
                    'value' => 'Titre du site'
                ]
            ],
            [
                'regex' => '^[\w]+$',
                'error' => 'Le titre du site ne peut être vide'
            ]
        );

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'text',
                'name' => 'school_name',
                'id' => 'school_name',
                'placeholder' => "Nom de l'etablissement",
                'label' => [
                    'value' => 'Nom de l\'établissement'
                ]
            ],
            [
                'regex' => '^[\w]+$',
                'error' => 'Le nom de l\'établissement ne peut être vide'
            ]
        );

        $form->addField(
            FormBuilder::TEXTAREA,
            [
                'name' => 'description',
                'id' => 'description',
                'placeholder' => 'Description du site',
                'label' => [
                    'value' => 'Description du site'
                ]
            ]
        );

        $form->addField(
            FormBuilder::INPUT,
            [
                'type' => 'text',
                'name' => 'adresse',
                'id' => 'adresse',
                'label' => [
                    'value' => 'Adresse de l\'etablissement'
                ],
                'placeholder' => 'Adresse de l etablissement'
            ]
        );

        $view->assign('form', $form);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $conf = new Configuration();
                $confManager = new ConfigurationManager();
                foreach ($this->getFromPost() as $key => $value) {
                    $conf->setConf($key);
                    $conf->setValue($value);
                    $confManager->save($conf);
                }
                $_SESSION['install']['site']['validate'] = true;
                Helpers::redirect($this->getUrl('Install', 'admin'));
            }
        }
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     */
    public function adminAction()
    {
        $view = new View('install.admin', 'install');
        $form = new FormBuilder("POST", $this->getUrl('Install', 'admin'));

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'lastname',
            'id' => 'lastname',
            'placeholder' => 'Nom de famille',
            'label' => [
                'value' => 'Nom de famille'
            ]
        ], ['regex' => '^[\w]+$', 'error' => 'Le nom n\'est pas renseigné correctement']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'firstname',
            'id' => 'firstname',
            'placeholder' => 'Prénom',
            'label' => [
                'value' => 'Prénom'
            ]
        ], ['regex' => '^[\w]+$', 'error' => 'Le prénom n\'est pas renseigné correctement']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'email',
            'name' => 'email',
            'id' => 'email',
            'placeholder' => 'email@email.com',
            'label' => [
                'value' => 'Adresse e-mail'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'password',
            'name' => 'password',
            'id' => 'password',
            'placeholder' => '******',
            'label' => [
                'value' => 'Mot de passe'
            ]
        ], ['regex' => '^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*().@%&]).{8,}$', 'error' => 'Le mot de passe n\'est pas assez robuste']);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'name' => 'submit',
            'value' => 'S inscrire en tant qu administrateur'
        ]);

        $view->assign('form', $form);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $userManager = new UserManager();
                $user = $userManager->findBy(['email' => $this->getFromPost('email')]);
                if (!$user) {
                    $user = new User();
                    $user->hydrate($this->getFromPost());
                    $user->setPassword(password_hash($this->getFromPost('password'), PASSWORD_BCRYPT));
                    $userManager->save($user);
                    $text = file_get_contents(".env");
                    $lines = explode("\n", $text);
                    $lines[array_search('INSTALLED=false', $lines)] = 'INSTALLED=true';
                    unlink(".env");
                    file_put_contents(".env", trim(implode("\n", $lines)));
                    unset($_SESSION['install']);
                    Helpers::redirect($this->getUrl('Auth', 'login'));
                } else {
                    $view->assign('errors', 'Impossible de s inscrire');
                }
            } else {
                $view->assign('errors', $form->getErrors());
            }
        }
    }
}