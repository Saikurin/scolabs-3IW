<?php


namespace Scolabs\controllers;

use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ContactManager;
use Scolabs\Models\Contact;
use Scolabs\Core\Helpers;

/**
 * Class ContactController
 * @package Scolabs\controllers
 */
class ContactController
{
    use UrlTrait, RequestTrait;

    public function indexAction()
    {
        $form = new FormBuilder("POST", $this->getUrl('Contact', 'send'));
        $form->addField(FormBuilder::INPUT, [
            'type' => 'email',
            'name' => 'email',
            'class' => 'form-control',
            'id' => 'email',
            'placeholder' => 'jean@dupont.fr',
            'label' => [
                'value' => 'Adresse e-mail'
            ]
        ], [
            'error' => 'Le format de l\'email est incorrect'
        ]);
        $view = new View('contact.ask', 'public');
        $view->assign('form', $form);
        $form->addField(FormBuilder::TEXTAREA, [
            'rows' => '5',
            'cols' => '50',
            'name' => 'message',
            'class' => 'form-control',
            'id' => 'message',
            'label' => [
                'value' => 'Votre message'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'name' => 'submit',
            'class' => 'btn',
            'id' => 'submit',
            'disabled' => true,
            'value' => 'Envoyer'
        ]);
    }

    public function sendAction(){
        $contactManager = new ContactManager(); // mettre un $this pour ça
        $contact = new Contact();

        $email = $this->getFromPost('email');
        $message = $this->getFromPost('message');
        $contact->setEmail($email);
        $contact->setMessage($message);
        $contact->setViewed(0);
        $contact->setAnswered(0);
        $contactManager->save($contact);

        Helpers::redirect(Helpers::getUrl('Events', 'index'));

        /* Créer un modèle permettant de gérer une table en base de données :
            - id, email, message, viewed(boolean), answered(boolean) DONE
           Créer un manager pour les requêtes SQL d'insertion et de visualisation :
            - Lors de l'insertion, on l'ajoute en BDD DONE
            - Lors de la visualisation par un membre de l'admission, on passe 'viewed' à true et on modifie la valeur
               des messages non-lus.
           Créer une page permettant aux responsables des admissions de visualiser les messages envoyés
            Dans le lien qui renvoie vers cette page, rajouter un petit cercle avec le nombre de messages reçus NON LUS
            - /contact/view/ : permets de lister la totalité des messages reçus
            - /contact/view/:id : permets de visualiser un seul et unique message et d'y répondre en envoyant un mail.
            - Si un membre de l'admission y réponds, on passe 'answered' à true.
        */
    }

}
