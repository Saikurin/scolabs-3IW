<?php


namespace Scolabs\controllers;

use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\CourseManager;
use Scolabs\Managers\StudentClassroomManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\User;

/**
 * Class ProfileController
 * @package Scolabs\controllers
 */
class ProfileController
{
    use UrlTrait, RequestTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var StudentClassroomManager
     */
    private $studentClassroomManager;

    /**
     * @var CourseManager
     */
    private $coursesManager;

    public function __construct()
    {
        $this->userManager = new UserManager();
        $this->configurationManager = new ConfigurationManager();
        $this->studentClassroomManager = new StudentClassroomManager();
        $this->coursesManager = new CourseManager();
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function userAction()
    {
        /** @var User $user */
        $user = $this->userManager->findWithRelation(Auth::getUser()->getId());

        if ($this->isPost()) {
            if (!empty($this->getFromPost('tel'))) {
                $user->setPhone($this->getFromPost('tel'));
            }
            if (!empty($this->getFromPost('email'))) {
                $user->setEmail($this->getFromPost('email'));
            }
            if (!empty($this->getFromPost('address'))) {
                $user->setAddress($this->getFromPost('address'));
            }
            if (!empty($this->getFromPost('zip_code'))) {
                $user->setZipCode($this->getFromPost('zip_code'));
            }
            if (!empty($this->getFromPost('city'))) {
                $user->setCity($this->getFromPost('city'));
            }
            if (!empty($this->getFromPost('password'))) {
                if ($this->getFromPost('password') === $this->getFromPost('repeat-password')) {
                    $user->setPassword(password_hash($this->getFromPost('password'), PASSWORD_BCRYPT));
                }
            }
            $this->userManager->save($user);
            header('Location:' . $this->getUrl('Profile', 'user'));
        }

        $form = new FormBuilder('POST', $this->getUrl('Profile', 'user'));
        $form->addField(
            'input',
            [
                'type' => 'tel',
                'id' => 'tel',
                'name' => 'tel',
                'value' => $user->getPhone() ?? '',
                'placeholder' => '0603020102',
                'label' => [
                    'value' => 'Numéro de téléphone'
                ]
            ]
        );

        $form->addField('input', [
            'type' => 'text',
            'id' => 'address',
            'name' => 'address',
            'value' => $user->getAddress() ?? '',
            'placeholder' => 'Adresse postale',
            'label' => [
                'value' => 'Adresse postale'
            ]
        ]);

        $form->addField('input', [
            'type' => 'text',
            'id' => 'zip_code',
            'name' => 'zip_code',
            'value' => $user->getZipCode() ?? '',
            'placeholder' => 'Code postal',
            'label' => [
                'value' => 'Code postal'
            ]
        ]);

        $form->addField('input', [
            'type' => 'text',
            'id' => 'city',
            'name' => 'city',
            'value' => $user->getCity() ?? '',
            'placeholder' => 'Ville',
            'label' => [
                'value' => 'Ville'
            ]
        ]);

        $form->addField('input', [
            'type' => 'email',
            'id' => 'email',
            'name' => 'email',
            'value' => $user->getEmail(),
            'placeholder' => 'Adresse email',
            'label' => [
                'value' => 'Adresse email'
            ]
        ]);

        $form->addField('input', [
            'type' => 'password',
            'id' => 'password',
            'name' => 'password',
            'value' => '',
            'placeholder' => 'Nouveau mot de passe',
            'label' => [
                'value' => 'Nouveau mot de passe'
            ]
        ]);

        $form->addField('input', [
            'type' => 'password',
            'id' => 'repeat-password',
            'name' => 'repeat-password',
            'value' => '',
            'placeholder' => 'Confirmation nouveau mot de passe',
            'label' => [
                'value' => 'Confirmation nouveau mot de passe'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Modifier',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        $view = new View('profile.user', 'admin');
        if ($user->getRole()->getSlug() === 'students') {
            $classroom = $this->studentClassroomManager->getClassroomByStudent(Auth::getUser()->getId());
            $courses = $this->coursesManager->findByClassroomWithRelation($classroom->getId());
            $subjects = [];
            foreach ($courses as $cours) {
                $subjects[] = $cours->getSubject();
            }
            $view->assign('parents', $this->userManager->getParentsByIdStudent($user->getId()));
            $view->assign('subjects', $subjects);
        }



        $address = $this->configurationManager->findOneBy(['conf' => 'address'])->getValue() . ' ,' . $this->configurationManager->findOneBy(['conf' => 'zip_code'])->getValue() . ' ' . $this->configurationManager->findOneBy(['conf' => 'city'])->getValue();
        $view->assign('address', $address);
        $view->assign('user', $user);
        $view->assign('form', $form);
        $view->assign('parents', $this->userManager->getParentsByIdStudent($user->getId()));
    }
}
