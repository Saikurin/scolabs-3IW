<?php


namespace Scolabs\controllers;

use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\Exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\ProductManager;
use Scolabs\Models\Product;

/**
 * Class ProductsController
 * @package Scolabs\controllers
 */
class ProductsController
{
    use RequestTrait;
    use UrlTrait;

    /**
     * @var ProductManager
     */
    private $manager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * ProductsController constructor.
     */
    public function __construct()
    {
        $this->manager = new ProductManager();
        $this->configurationManager = new ConfigurationManager();
    }

    public function indexAction()
    {
        $view = new View('products.list', 'admin');
        $view->assign('currency_symbol', $this->configurationManager->findOneBy(['conf' => 'currency_symbol'])->getValue());
        $view->assign('products', $this->manager->findAll());
        $view->addCSS('datatables.min.css');
        $view->addJS('datatables.min.js');
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function addAction()
    {
        $form = new FormBuilder('POST', $this->getUrl('Products', 'add'));
        $form->addField('input', [
            'type' => 'text',
            'id' => 'name',
            'name' => 'name',
            'placeholder' => 'Nom du produit',
            'label' => [
                'value' => 'Produit'
            ]
        ], [
            'regex' => '[\s\S]',
            'error' => 'Le nom est obligatoire'
        ]);
        $form->addField('textarea', [
            'id' => 'description',
            'name' => 'description',
            'placeholder' => 'Description du produit',
            'label' => [
                'value' => 'Description du produit'
            ]
        ], [
            'regex' => '[\s\S]',
            'error' => 'La description est obligatoire'
        ]);

        $form->addField('input', [
            'type' => 'text',
            'id' => 'unit_price',
            'name' => 'unit_price',
            'placeholder' => 'Prix unitaire du produit',
            'label' => [
                'value' => 'Prix unitaire HT'
            ]
        ], [
            'regex' => '[-+]?[0-9]*\.?[0-9]+',
            'error' => 'Le prix unitaire HT est obligatoire'
        ]);

        $form->addField('select', [
            'id' => 'availability',
            'name' => 'availability',
            'label' => [
                'value' => 'Disponibilité'
            ],
            'multiple' => false,
            'values' => [
                [
                    'selected' => true,
                    'value' => 'DISPONIBLE',
                    'text' => 'DISPONIBLE'
                ],
                [
                    'selected' => false,
                    'value' => 'NON DISPONIBLE',
                    'text' => 'NON DISPONIBLE'
                ]
            ]
        ], [
            'regex' => '[\s\S]',
            'error' => 'L\'état est obligatoire'
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Ajouter',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $product = new Product();
                $product->setName($this->getFromPost('name'));
                $product->setDescription($this->getFromPost('description'));
                $product->setAvailability($this->getFromPost('availability'));
                $product->setUnitPrice($this->getFromPost('unit_price'));
                $this->manager->save($product);
                header('Location: ' . $this->getUrl('Products', 'index'));
            }
        }
        $view = new View('products.add', 'admin');
        $view->assign('form', $form);
    }

    /**
     * @param int $id
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function editAction(int $id)
    {
        /** @var Product $product */
        $product = $this->manager->find($id);
        if (is_int($product->getId())) {
            $form = new FormBuilder('POST', $this->getUrl('Products', 'edit', ['id' => $id]));
            $form->addField('input', [
                'type' => 'text',
                'id' => 'name',
                'name' => 'name',
                'value' => $product->getName(),
                'placeholder' => 'Nom du produit',
                'label' => [
                    'value' => 'Produit'
                ]
            ], [
                'regex' => '[\s\S]',
                'error' => 'Le nom est obligatoire'
            ]);
            $form->addField('textarea', [
                'id' => 'description',
                'name' => 'description',
                'placeholder' => 'Description du produit',
                'value' => $product->getUnitPrice(),
                'label' => [
                    'value' => 'Description du produit'
                ]
            ], [
                'regex' => '[\s\S]',
                'error' => 'La description est obligatoire'
            ]);

            $form->addField('input', [
                'type' => 'text',
                'id' => 'unit_price',
                'name' => 'unit_price',
                'value' => $product->getUnitPrice(),
                'placeholder' => 'Prix unitaire du produit',
                'label' => [
                    'value' => 'Prix unitaire HT'
                ]
            ], [
                'regex' => '[-+]?[0-9]*\.?[0-9]+',
                'error' => 'Le prix unitaire HT est obligatoire'
            ]);

            $form->addField('select', [
                'id' => 'availability',
                'name' => 'availability',
                'label' => [
                    'value' => 'Disponibilité'
                ],
                'multiple' => false,
                'values' => [
                    [
                        'selected' => ($product->getAvailability() === 'DISPONIBLE'),
                        'value' => 'DISPONIBLE',
                        'text' => 'DISPONIBLE'
                    ],
                    [
                        'selected' => ($product->getAvailability() === 'NON DISPONIBLE'),
                        'value' => 'NON DISPONIBLE',
                        'text' => 'NON DISPONIBLE'
                    ]
                ]
            ], [
                'regex' => '[\s\S]',
                'error' => 'L\'état est obligatoire'
            ]);

            $form->addField('input', [
                'type' => 'submit',
                'class' => 'btn btn-dark-purple btn-block',
                'value' => 'Modifier',
                'name' => 'submit',
                'id' => 'submit'
            ]);

            if ($this->isPost()) {
                if ($form->isValid()) {
                    $product->setName($this->getFromPost('name'));
                    $product->setDescription($this->getFromPost('description'));
                    $product->setAvailability($this->getFromPost('availability'));
                    $product->setUnitPrice($this->getFromPost('unit_price'));
                    $product->setId($id);
                    $this->manager->save($product);
                    header('Location: ' . $this->getUrl('Products', 'index'));
                }
            }
            $view = new View('products.add', 'admin');
            $view->assign('form', $form);
        }
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function deleteAction(int $id)
    {
        $this->manager->delete($id);
        header('Location: ' . $this->getUrl('Products', 'index'));
    }
}
