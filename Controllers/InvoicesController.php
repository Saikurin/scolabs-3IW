<?php


namespace Scolabs\controllers;

use Exception;
use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\InvoiceManager;
use Scolabs\Managers\InvoiceProductManager;
use Scolabs\Managers\ProductManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Invoice;
use Scolabs\Models\Invoiceproduct;
use Scolabs\Models\Product;
use Scolabs\Models\User;

/**
 * Class InvoicesController
 * @package Scolabs\controllers
 */
class InvoicesController
{
    use RequestTrait;
    use UrlTrait;

    /**
     * @var InvoiceManager
     */
    private $manager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var InvoiceProductManager
     */
    private $invoiceProductManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * InvoicesController constructor.
     */
    public function __construct()
    {
        $this->manager = new InvoiceManager();
        $this->userManager = new UserManager();
        $this->productManager = new ProductManager();
        $this->invoiceProductManager = new InvoiceProductManager();
        $this->configurationManager = new ConfigurationManager();
    }

    public function listAction()
    {
        $view = new View('invoices.list', 'admin');
        $view->assign('invoices', $this->manager->findWithRelations());
        $view->addJS('datatables.min.js');
    }

    /**
     * @param int $id
     */
    public function showAction(int $id)
    {
        $invoice = $this->manager->findOneWithRelations($id);
        $view = new View('invoices.show', 'admin');
        $view->assign('school_name', $this->configurationManager->findOneBy(['conf' => 'title'])->getValue());
        $view->assign('address', $this->configurationManager->findOneBy(['conf' => 'address'])->getValue());
        $view->assign('zip_code', $this->configurationManager->findOneBy(['conf' => 'zip_code'])->getValue());
        $view->assign('city', $this->configurationManager->findOneBy(['conf' => 'city'])->getValue());
        $view->assign('invoice', $invoice);
        $view->assign('products', $this->invoiceProductManager->getProductsByInvoice($id));
        $view->assign('currencySymbol', $this->configurationManager->findOneBy(['conf' => 'currency_symbol'])->getValue());
    }

    public function ownedAction()
    {
        $invoices = $this->manager->getInvoicesByUser(Auth::getUser()->getId()) ?? [];
        $view = new View('invoices.list', 'admin');
        $view->assign('invoices', $invoices);
    }

    /**
     * @throws Exception
     */
    public function createAction()
    {
        $_SESSION['token'] = bin2hex(random_bytes(32));

        if ($this->isPost()) {
            if ($_POST['student'] !== '' && $_POST['parent'] !== '') {
                /** @var User $student */
                $student = $this->userManager->find($this->getFromPost('student'));
                /** @var User $customer */
                $customer = $this->userManager->find($this->getFromPost('parent'));

                $invoice = new Invoice();
                $invoice->setState(1);
                $invoice->setCreator(Auth::getUser());
                $invoice->setUpdator(Auth::getUser());
                $invoice->setCustomer($customer);
                $invoice->setStudent($student);
                $invoice->setCreatedAt(date('Y-m-d H:i:s'));
                $invoice->setUpdatedAt(date('Y-m-d H:i:s'));

                $this->manager->save($invoice);


                /** @var Invoice $lastInvoice */
                $lastInvoice = $this->manager->getLastInvoice();
                $posts = $this->getFromPost();
                foreach ($posts['ids'] as $itterator => $product) {
                    /** @var Product $product */
                    $product = $this->productManager->find((int)$product);
                    $ip = new Invoiceproduct();
                    $ip->setInvoice($lastInvoice);
                    $ip->setProduct($product);
                    $ip->setQuantity($posts['quantities'][$itterator]);
                    $ip->setVat($posts['vat'][$itterator]);
                    $this->invoiceProductManager->save($ip);
                }
            }
            header('Location: ' . $this->getUrl('Invoices', 'list'));
        }

        $view = new View('invoices.create', 'admin');

        $view->addCSS('select2.min.css');
        $view->addCSS('datatables.min.css');
        $view->addJS('select2.min.js');
        $view->addJS('datatables.min.js');

        $view->assign('token', $_SESSION['token']);
        $view->assign('students', $this->userManager->getStudents());
        $view->assign('number', $this->manager->getNewInvoiceNumber());
        $view->assign('products', $this->productManager->getAllAvailable());
        $view->assign('vat', $this->configurationManager->findOneBy(['conf' => 'default_vat'])->getValue());
        $view->assign('parents', $this->userManager->getParents());
    }
}
