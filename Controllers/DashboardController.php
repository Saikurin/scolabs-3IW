<?php

namespace Scolabs\Controllers;

use Scolabs\Core\Auth;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\UserManager;

/**
 * Class DashboardController
 * @package Scolabs\Controllers
 */
class DashboardController
{
    use UrlTrait;
    use RequestTrait;

    /**
     * @var UserManager
     */
    private $manager;

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->manager = new UserManager();
    }

    public function indexAction()
    {
        $view = new View("dashboard.index", "admin");
    }
}
