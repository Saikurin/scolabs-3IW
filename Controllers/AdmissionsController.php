<?php

namespace Scolabs\Controllers;

use PHPMailer\PHPMailer\Exception;
use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\Helpers;
use Scolabs\Core\Traits\SecureTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\View;
use Scolabs\Managers\ClassroomManager;
use Scolabs\Managers\ContactManager;
use Scolabs\Managers\RoleManager;
use Scolabs\Managers\StudentClassroomManager;
use Scolabs\Managers\UserManager;
use Scolabs\Managers\UsersParentManager;
use Scolabs\Models\Classroom;
use Scolabs\models\Role;
use Scolabs\Models\Studentclassrooms;
use Scolabs\Models\User;
use Scolabs\models\UsersParent;
use Scolabs\Core\Mailer;

/**
 * Class AdmissionsController
 * @package Scolabs\Controllers
 */
class AdmissionsController
{
    use UrlTrait;
    use RequestTrait;
    use SecureTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var RoleManager
     */
    private $roleManager;

    /**
     * @var ContactManager
     */
    private $contactManager;

    /**
     * @var StudentClassroomManager
     */
    private $studentClassroomManager;

    /**
     * @var ClassroomManager
     */
    private $classroomManager;

    /**
     * AdmissionsController constructor.
     */
    public function __construct()
    {
        $this->userManager = new UserManager();
        $this->roleManager = new RoleManager();
        $this->contactManager = new ContactManager();
        $this->studentClassroomManager = new StudentClassroomManager();
        $this->classroomManager = new ClassroomManager();
    }

    public function indexAction()
    {
        $studentsCount = $this->userManager->count(["id_role" => $this->roleManager->findOneBy(["slug" => "students"])->getId()]);
        $students = $this->userManager->getStudents();
        $parents = [];
        foreach ($students as $key => $student) {
            if ($this->userManager->getParents($key)) {
                $parents[$key] = $this->userManager->getParents($key);
            }
        }

        $view = new View('admissions.index', 'admin');
        $view->addJS('datatables.min.js');
        $view->addCSS('jquery.dataTables.min.css');
        $view->addCSS('datatables.min.css');
        $view->assign("userManager", $this->userManager);
        $view->assign("students", $students);
        $view->assign("nbPages", ceil($studentsCount / 10));
        $view->assign("studentsCount", $studentsCount);
        $view->assign("parents", $parents); // if $students[1] exists then $parents[1] = parents of $students[1].
    }

    /**
     * @param int $id
     */
    public function studentAction(int $id)
    {
        // Cette fonction permets de récupérer toutes les informations d'un seul utilisateur, de les afficher et de les modifier.

        $student = $this->userManager->find($id);
        $parents = $this->userManager->getParents($id);

        $view = new View('admissions.student', 'admin');
        $view->addJS('datatables.min.js');
        $view->addCSS("student.css");
        $view->assign("student", $student);
        $view->assign("parents", $parents);

    }

    public function contactAction(){

        $nonViewed = $this->contactManager->findAll();
        $view = new View('admissions.contact', 'admin');
        $view->addJS('datatables.min.js');
        $view->addCSS('datatables.min.css');
        $view->assign('nonViewed', $nonViewed);
    }

    /**
     * @param int $id
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function answerContactAction(int $id){
        $message = $this->contactManager->findOneBy(['id' => $id]);
        $message->setViewed(true);
        $this->contactManager->save($message);

        $form = new FormBuilder('POST', $this->getUrl('Admissions', 'sendAnswer', ['id' => $id]));

        $form->addField('input', [
            'type' => 'text',
            'id' => 'object',
            'name' => 'object',
            'class' => 'text-center',
            'label' => [
                'value' => 'Objet'
            ]
        ]);

        $form->addField('textarea', [
            'id' => 'answer',
            'name' => 'answer',
            'label' => [
                'value' => 'Contenu',
                'class' => 'text-center'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'id' => 'submit',
            'name' => 'submit',
            'value' => 'Répondre',
            'class' => 'btn btn-dark-purple btn-block'
        ]);

        $view = new View('admissions.answer', 'admin');
        $view->assign('message', $message);
        $view->assign('form', $form);
        $view->addJS('tinymce.min.js');
        $view->addLinkJS('https://cdn.tiny.cloud/1/og72vfgkb3s6k2rp6564ejrme3tqs9qh635eae96kjbzy0eh/tinymce/5/tinymce.min.js');
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function deleteContactAction(int $id){
        $this->contactManager->delete($id);
        Helpers::redirect($this->getUrl('Admissions', 'contact'));
    }

    public function sendAnswerAction($id){
        $contact = $this->contactManager->findOneBy(['id' => $id]);
        $answer = html_entity_decode(htmlspecialchars_decode($this->getFromPost('answer')));
        $object = html_entity_decode(htmlspecialchars_decode($this->getFromPost('object')));

        $mailer = new Mailer();
        $mailer->withTemplate('answerContact', [
            'answerMessage' => $answer,
            'object' => $object
        ]);
        try {
            $mailer->send("waerosis@gmail.com", $object, [$contact->getEmail()]);
        } catch (Exception $e) {
            echo $e;
        }

        $contact->setAnswered(true);
        $this->contactManager->save($contact);

        Helpers::redirect(Helpers::getUrl('Admissions', 'contact'));
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function registerAction()
    {
        $parents = $this->userManager->getParents();
        $classrooms = $this->classroomManager->findAll();

        $form = new FormBuilder("POST", $this->getUrl('Admissions', 'newStudent'));
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'lastname',
            'class' => 'form-control',
            'id' => 'lastname',
            'placeholder' => 'Dupont',
            'label' => [
                'value' => 'Nom'
            ]
        ], [
            'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$', // Que des lettres/caractères spéciaux
            'error' => 'Le format du nom est incorrect'
        ]);
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'firstname',
            'class' => 'form-control',
            'id' => 'firstname',
            'placeholder' => 'Jean',
            'label' => [
                'value' => 'Prénom'
            ]
        ], [
            'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
            'error' => 'Le format du prénom est incorrect'
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'pob',
            'class' => 'form-control',
            'id' => 'pob',
            'placeholder' => 'Ville de naissance',
            'label' => [
                'value' => 'Ville de naissance'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'date',
            'name' => 'dob',
            'class' => 'form-control',
            'id' => 'dob',
            'placeholder' => 'Date de naissance',
            'label' => [
                'value' => 'Date de naissance'
            ]
        ]);
        $form->addField(FormBuilder::INPUT, [
            'type' => 'email',
            'name' => 'email',
            'class' => 'form-control',
            'id' => 'email',
            'placeholder' => 'Adresse e-mail',
            'label' => [
                'value' => 'jean@dupont.fr'
            ]
        ]);
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'address',
            'class' => 'form-control',
            'id' => 'address',
            'placeholder' => '101 Rue Saint Honoré',
            'label' => [
                'value' => 'Adresse'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'city',
            'class' => 'form-control',
            'id' => 'city',
            'placeholder' => 'Ville',
            'label' => [
                'value' => 'Ville'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'zipcode',
            'class' => 'form-control',
            'id' => 'zipcode',
            'placeholder' => 'Code postal',
            'label' => [
                'value' => 'Code postal'
            ]
        ]);
        $form->addField(FormBuilder::INPUT, [
            'type' => 'tel',
            'name' => 'phoneNumber',
            'class' => 'form-control',
            'id' => 'phoneNumber',
            'placeholder' => '0612345678',
            'label' => [
                'value' => 'Numéro de téléphone'
            ]
        ], [
            'regex' => '(0|(\\+33)|(0033))[1-9][0-9]{8}',
            'error' => 'Numéro de téléphone incorrect'
        ]);


        // ----------------------------------- PARENT 1 FORM -----------------------------------

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentFirstname1',
            'class' => 'form-control',
            'id' => 'parentFirstname1',
            'placeholder' => 'Prénom',
            'label' => [
                'value' => 'Prénom du responsable légal 1'
            ],
            [
                'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
                'error' => 'Le format du prénom est incorrect'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentLastname1',
            'class' => 'form-control',
            'id' => 'parentLastname1',
            'placeholder' => 'Nom',
            'label' => [
                'value' => 'Nom du responsable légal 1'
            ],
            [
                'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
                'error' => 'Le format du nom est incorrect'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentPob1',
            'class' => 'form-control',
            'id' => 'parentPob1',
            'placeholder' => 'Ville de naissance',
            'label' => [
                'value' => 'Ville de naissance du responsable légal 1'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'date',
            'name' => 'parentDob1',
            'class' => 'form-control',
            'id' => 'parentDob1',
            'placeholder' => 'Date de naissance',
            'label' => [
                'value' => 'Date de naissance du responsable légal 1'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentAddress1',
            'class' => 'form-control',
            'id' => 'parentAddress1',
            'placeholder' => 'Adresse',
            'label' => [
                'value' => 'Adresse du responsable légal 1'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentCity1',
            'class' => 'form-control',
            'id' => 'parentCity1',
            'placeholder' => 'Ville',
            'label' => [
                'value' => 'Ville du responsable légal 1'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentZipcode1',
            'class' => 'form-control',
            'id' => 'parentZipcode1',
            'placeholder' => 'Code postal',
            'label' => [
                'value' => 'Code postal du responsable légal 1'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'tel',
            'name' => 'parentPhoneNumber1',
            'class' => 'form-control',
            'id' => 'parentPhoneNumber1',
            'placeholder' => '0612345678',
            'label' => [
                'value' => 'Numéro de téléphone'
            ]
        ], [
            'regex' => '(0|(\\+33)|(0033))[1-9][0-9]{8}', // Numéro commençant par 0/+33 OU par 0033 suivi de 1 chiffre allant de 1 à 9 et 8 chiffres allant de 0 à 9
            'error' => 'Numéro de téléphone incorrect'
        ]);
        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'email',
            'name' => 'parentEmail1',
            'class' => 'form-control',
            'id' => 'parentEmail1',
            'placeholder' => 'Adresse e-mail',
            'label' => [
                'value' => 'Adresse e-mail du responsable légal 1'
            ]
        ]);

        // ----------------------------------- PARENT 2 FORM -----------------------------------

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentFirstname2',
            'class' => 'form-control',
            'id' => 'parentFirstname2',
            'placeholder' => 'Prénom',
            'label' => [
                'value' => 'Prénom du responsable légal 2'
            ],
            [
                'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
                'error' => 'Le format du nom est incorrect'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentLastname2',
            'class' => 'form-control',
            'id' => 'parentLastname2',
            'placeholder' => 'Nom',
            'label' => [
                'value' => 'Nom du responsable légal 2'
            ],
            [
                'regex' => '^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$',
                'error' => 'Le format du nom est incorrect'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentPob2',
            'class' => 'form-control',
            'id' => 'parentPob2',
            'placeholder' => 'Ville de naissance',
            'label' => [
                'value' => 'Ville de naissance du responsable légal 2'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'date',
            'name' => 'parentDob2',
            'class' => 'form-control',
            'id' => 'parentDob2',
            'placeholder' => 'Date de naissance',
            'label' => [
                'value' => 'Date de naissance du responsable légal 2'
            ]
        ]);


        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentAddress2',
            'class' => 'form-control',
            'id' => 'parentAddress2',
            'placeholder' => 'Adresse',
            'label' => [
                'value' => 'Adresse du responsable légal 2'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentCity2',
            'class' => 'form-control',
            'id' => 'parentCity2',
            'placeholder' => 'Ville',
            'label' => [
                'value' => 'Ville du responsable légal 2'
            ]
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'text',
            'name' => 'parentZipcode2',
            'class' => 'form-control',
            'id' => 'parentZipcode2',
            'placeholder' => 'Code postal',
            'label' => [
                'value' => 'Code postal du responsable légal 2'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'tel',
            'name' => 'parentPhoneNumber2',
            'class' => 'form-control',
            'id' => 'parentPhoneNumber2',
            'placeholder' => '0612345678',
            'label' => [
                'value' => 'Numéro de téléphone'
            ]
        ], [
            'regex' => '(0|(\\+33)|(0033))[1-9][0-9]{8}',
            'error' => 'Numéro de téléphone incorrect'
        ]);

        $form->addField(FORMBUILDER::INPUT, [
            'type' => 'email',
            'name' => 'parentEmail2',
            'class' => 'form-control',
            'id' => 'parentEmail2',
            'placeholder' => 'Adresse e-mail',
            'label' => [
                'value' => 'Adresse e-mail du responsable légal 2'
            ]
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'name' => 'submit',
            'class' => 'btn',
            'id' => 'submit',
            'disabled' => true,
            'value' => 'Envoyer'
        ]);

        $view = new View('admissions.register', 'admin');
        $view->assign("form", $form);
        $view->assign("parents", $parents);
        $view->assign("classrooms", $classrooms);

    }

    /**
     * @throws NotFoundException
     */
    public function newStudentAction()
    {
        if (!is_null($this->userManager->findOneBy(['email' => $this->getFromPost('email')])->getId())) {
            throw new NotFoundException("L'email saisi est déjà utilisé");
        } else {
            $studentPassword = Helpers::generateRandomString(8);

            /** @var Role $role */
            $studentRole = $this->roleManager->findOneBy(['slug' => 'students']);
            $parentRole = $this->roleManager->findOneBy(['slug' => 'parents']);

            // Saving student

            $student = new User();
            $student->setEmail($this->getFromPost('email'));
            $student->setPassword(password_hash($studentPassword, PASSWORD_BCRYPT));
            $student->setFirstname($this->getFromPost('firstname'));
            $student->setLastname($this->getFromPost('lastname'));
            $student->setAddress($this->getFromPost('address'));
            $student->setCity($this->getFromPost('city'));
            $student->setZipCode($this->getFromPost('zipcode'));
            $student->setPhone($this->getFromPost('phoneNumber'));
            $student->setPob($this->getFromPost('pob'));
            $student->setDob($this->getFromPost('dob'));
            $student->setCreatedAt(date('Y-m-d H:i:s'));
            $student->setState(1);
            $student->setRole($studentRole);
            $studentUsername = Helpers::generateUsername($this->getFromPost('firstname'), $this->getFromPost('lastname'));
            $student->setUsername($studentUsername);
            $this->userManager->save($student);
            $this->generateMail($studentRole->getName(), $student->getEmail(), $student->getFirstname(), $student->getLastname(), $student->getUsername(), $studentPassword);

            $studentId = $this->userManager->lastInsertId();
            $student->setId($studentId);

            $studentClassroom = new Studentclassrooms();
            $classroom = new Classroom();
            $classroomId = $this->getFromPost('classroom');
            $classroom->setId($classroomId);
            $studentClassroom->setStudent($student);
            $studentClassroom->setClassroom($classroom);

            $this->studentClassroomManager->save($studentClassroom);

            if (!isset($_POST['orphan'])) { // S'il n'est pas orphelin / pupil de l'état
                if (isset($_POST['parentFirstname1']) && $_POST['parentFirstname1'] !== "") {
                    $parentPassword1 = Helpers::generateRandomString(8);

                    $parent1 = new User();
                    $parent1->setEmail($this->getFromPost('parentEmail1'));
                    $parent1->setPassword(password_hash($parentPassword1, PASSWORD_BCRYPT));
                    $parent1->setFirstname($this->getFromPost('parentFirstname1'));
                    $parent1->setLastname($this->getFromPost('parentLastname1'));
                    $parent1->setPob($this->getFromPost('parentPob1'));
                    $parent1->setDob($this->getFromPost('parentDob1'));
                    $parent1->setAddress($this->getFromPost('parentAddress1'));
                    $parent1->setCity($this->getFromPost('parentCity1'));
                    $parent1->setZipCode($this->getFromPost('parentZipcode1'));
                    $parent1->setPhone($this->getFromPost('parentPhoneNumber1'));
                    $parent1->setCreatedAt(date('Y-m-d H:i:s'));
                    $parent1->setState(1);
                    $parent1->setRole($parentRole);
                    $parentUsername1 = Helpers::generateUsername($this->getFromPost('parentFirstname1'), $this->getFromPost('parentLastname1'));
                    $parent1->setUsername($parentUsername1);
                    $this->userManager->save($parent1);
                    $parent1Id = $this->userManager->lastInsertId();
                    $this->generateMail($parentRole->getName(), $parent1->getEmail(), $parent1->getFirstname(), $parent1->getLastname(), $parent1->getUsername(), $parentPassword1);
                } else {
                    $selectParent1 = explode(" ", $this->getFromPost('selectParent1'));
                    $selectParent1 = $this->userManager->findOneBy(['firstname' => $selectParent1[0], 'lastname' => $selectParent1[1]]);
                    $parent1Id = $selectParent1->getId();
                }

                // Parent #2
                if (!isset($_POST['oneParent'])) { // S'il n'a pas qu'un seul parent
                    if (isset($_POST['parentFirstname2']) && $_POST['parentFirstname2'] !== "") {
                        $parentPassword2 = Helpers::generateRandomString(8);

                        $parent2 = new User();
                        $parent2->setEmail($this->getFromPost('parentEmail2'));
                        $parent2->setPassword(password_hash($parentPassword2, PASSWORD_BCRYPT));
                        $parent2->setFirstname($this->getFromPost('parentFirstname2'));
                        $parent2->setLastname($this->getFromPost('parentLastname2'));
                        $parent2->setPob($this->getFromPost('parentPob2'));
                        $parent2->setDob($this->getFromPost('parentDob2'));
                        $parent2->setAddress($this->getFromPost('parentAddress2'));
                        $parent2->setCity($this->getFromPost('parentCity2'));
                        $parent2->setZipCode($this->getFromPost('parentZipcode2'));
                        $parent2->setPhone($this->getFromPost('parentPhoneNumber2'));
                        $parent2->setCreatedAt(date('Y-m-d H:i:s'));
                        $parent2->setState(1);
                        $parent2->setRole($parentRole);
                        $parentUsername2 = Helpers::generateUsername($this->getFromPost('parentFirstname2'), $this->getFromPost('parentLastname2'));
                        $parent2->setUsername($parentUsername2);
                        $this->userManager->save($parent2);
                        $parent2Id = $this->userManager->lastInsertId();

                        $this->generateMail($parentRole->getName(), $parent2->getEmail(), $parent2->getFirstname(), $parent2->getLastname(), $parent2->getUsername(), $parentPassword2);

                    } else {
                        $selectParent2 = explode(" ", $this->getFromPost('selectParent2'));
                        $selectParent2 = $this->userManager->findOneBy(['firstname' => $selectParent2[0], 'lastname' => $selectParent2[1]]);
                        $parent2Id = $selectParent2->getId();
                    }
                }
            }
            $studentParentsManager = new UsersParentManager();
            if (isset($parent1Id)){
                $studentParents1 = new UsersParent();
                $studentParents1->setIdStudent($studentId);
                $studentParents1->setIdParent($parent1Id);
                $studentParentsManager->save($studentParents1);
            }
            if (isset($parent2Id)){
                $studentParents2 = new UsersParent();
                $studentParents2->setIdStudent($studentId);
                $studentParents2->setIdParent($parent2Id);
                $studentParentsManager->save($studentParents2);
            }

            header('Location:' . $this->getUrl('Admissions', 'index'));
        }
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function archiveAction(int $id)
    {
        $this->changeState(3, $id);
        header('Location: ' . $this->getUrl('Admissions', 'index'));
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function denyAction(int $id)
    {
        $this->changeState(2, $id);
        header('Location: ' . $this->getUrl('Admissions', 'index'));
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function awaitAction(int $id)
    {
        $this->changeState(1, $id);
        header('Location: ' . $this->getUrl('Admissions', 'index'));
    }

    /**
     * @param int $id
     * @throws NotFoundException
     */
    public function acceptAction(int $id)
    {
        $this->changeState(0, $id);
        header('Location: ' . $this->getUrl('Admissions', 'index'));
    }

    /**
     * @param int $state
     * @param int $id
     */


    private function changeState(int $state, int $id)
    {
        /** @var User $student */
        $student = $this->userManager->find($id);
        $student->setState($state);
        $this->userManager->save($student);
    }

    private function generateMail($role, $email, $firstname, $lastname, $username, $password){
        $mailer = new Mailer();

        $mailer->withTemplate("register",
            [
                "role" => $role,
                "email" => $email,
                "firstname" => $firstname,
                "lastname" => $lastname,
                "username" => $username,
                "password" => $password
            ]
        );

        $subject = "Inscription " . $role . " sur Scolabs";

        try {
            $mailer->send("nomLycee@scolabs.fr", $subject, [$email]);
        } catch (Exception $e) {
            echo "Erreur";
        }
    }
}
