<?php


namespace Scolabs\controllers;

use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\core\exceptions\RequiredMissingException;
use Scolabs\Core\FileManager;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\EventManager;
use Scolabs\Models\Events;

/**
 * Class EventsController
 * @package Scolabs\controllers
 */
class EventsController
{
    use UrlTrait, RequestTrait;

    /**
     * @var EventManager
     */
    private $eventsManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    public function __construct()
    {
        $this->eventsManager = new EventManager();
        $this->configurationManager = new ConfigurationManager();
    }

    public function indexAction()
    {
        $view = new View('news.index', 'public');
        $view->assign('school_name', $this->configurationManager->findOneBy(['conf' => 'title'])->getValue());
        $view->assign('events', $this->eventsManager->getLastFiveNews());
    }

    public function listAction()
    {
        $view = new View('news.list', 'admin');
        $view->addJS('datatables.min.js');
        $view->addCSS('datatables.min.css');
        $view->assign('events', $this->eventsManager->findAll());
    }

    /**
     * @param int $id
     */
    public function showadminAction(int $id)
    {
        $view = new View('news.showadmin', 'admin');
        $view->assign('event', $this->eventsManager->findByIdWithRelation($id));
        $view->addJS('tinymce.min.js');
        $view->addLinkJS('https://cdn.tiny.cloud/1/og72vfgkb3s6k2rp6564ejrme3tqs9qh635eae96kjbzy0eh/tinymce/5/tinymce.min.js');
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function newAction()
    {
        $form = new FormBuilder('POST', $this->getUrl('Events', 'new'), true);

        $form->addField('input', [
            'type' => 'text',
            'id' => 'title',
            'name' => 'title',
            'class' => 'text-center',
            'label' => [
                'value' => 'Titre de l\'évènement'
            ]
        ]);

        $form->addField('textarea', [
            'id' => 'resume',
            'name' => 'resume',
            'class' => 'text-center',
            'label' => [
                'value' => 'Resume de l\'évènement'
            ]
        ]);

        $form->addField('input', [
            'type' => 'file',
            'id' => 'picture',
            'name' => 'picture',
            'class' => 'text-center',
            'label' => [
                'value' => 'Image de l\'évènement'
            ]
        ]);

        $form->addField('textarea', [
            'id' => 'new',
            'name' => 'new',
            'label' => [
                'value' => 'Contenu',
                'class' => 'text-center'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'id' => 'submit',
            'name' => 'submit',
            'value' => 'Créer',
            'class' => 'btn btn-dark-purple btn-block'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $event = new Events();
                $file = new FileManager();

                $file->setAllowedMimeTypes([
                    'image/jpeg',
                    'image/png',
                    'image/svg+xml'
                ]);

                $file->setInput('picture');
                $file->allowOverwriting();
                $file->save();

                $event->setTitle(html_entity_decode(htmlspecialchars_decode($this->getFromPost('title'))));
                $event->setContent(html_entity_decode(htmlspecialchars_decode($this->getFromPost('new'))));
                $event->setCreatedAt(date('Y-m-d H:i:s'));
                $event->setUpdatedAt(date('Y-m-d H:i:s'));
                if (!empty($_FILES['picture']['name'])) {
                    $event->setPicture(DIRECTORY_SEPARATOR . DATA_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $_FILES['picture']['name']);
                }
                $event->setResume(html_entity_decode(htmlspecialchars_decode($this->getFromPost('resume'))));
                $event->setCreator(Auth::getUser());
                $event->setState(1);
                $this->eventsManager->save($event);
                chdir(DIRECTORY_ROOT);
                header('Location:' . $this->getUrl('Events', 'list'));
                return true;
            }
        } else {
            $view = new View('news.create', 'admin');
            $view->assign('form', $form);
            $view->addJS('tinymce.min.js');
            $view->addLinkJS('https://cdn.tiny.cloud/1/og72vfgkb3s6k2rp6564ejrme3tqs9qh635eae96kjbzy0eh/tinymce/5/tinymce.min.js');
        }
    }

    /**
     * @param int $idEvent
     * @throws NotFoundException
     */
    public function deleteAction(int $idEvent)
    {
        if ($this->getFromPost('validation') == $idEvent) {
            if (md5($this->eventsManager->find($idEvent)->getTitle()) === $this->getFromPost('token')) {
                $this->eventsManager->delete($idEvent);
            }
        }
        header('Location: ' . $this->getUrl('Events', 'list'));
    }

    /**
     * @param int $idEvent
     * @throws NotFoundException
     */
    public function archiveAction(int $idEvent)
    {
        if ($this->getFromPost('validation') == $idEvent) {
            /** @var Events $event */
            $event = $this->eventsManager->find($idEvent);
            if (md5($event->getTitle()) === $this->getFromPost('token')) {
                $event->setState(2);
                $this->eventsManager->save($event);
            }
        }
        header('Location: ' . $this->getUrl('Events', 'list'));
    }

    /**
     * @param int $idEvent
     * @throws NotFoundException
     * @throws NotAllowedException
     */
    public function editAction(int $idEvent)
    {
        if (md5($idEvent) !== $this->getFromPost('token')) {
            throw new NotAllowedException('Une erreur est survenue');
        }
        /** @var Events $event */
        $event = $this->eventsManager->find($idEvent);
        if (!empty($_FILES['picture']['name'])) {
            $file = new FileManager();

            $file->setAllowedMimeTypes([
                'image/jpeg',
                'image/png',
                'image/svg+xml'
            ]);

            $file->setInput('picture');
            $file->allowOverwriting();
            $file->save();
            $event->setPicture(DIRECTORY_SEPARATOR . DATA_DIR . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . $_FILES['picture']['name']);
        }

        $event->setUpdatedAt(date('Y-m-d H:i:s'));
        $event->setResume($this->getFromPost('resume'));
        $event->setTitle($this->getFromPost('title'));
        $event->setContent($_POST['content']);
        $this->eventsManager->save($event);
        header('Location: ' . $this->getUrl('Events', 'list'));
    }
}
