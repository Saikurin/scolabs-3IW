<?php


namespace Scolabs\controllers;


use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\SubjectManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Subject;
use Scolabs\Models\User;

/**
 * Class SubjectsController
 * @package Scolabs\controllers
 */
class SubjectsController
{

    use UrlTrait, RequestTrait;

    /**
     * @var SubjectManager
     */
    private $subjectManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * SubjectsController constructor.
     */
    public function __construct()
    {
        $this->subjectManager = new SubjectManager();
        $this->userManager = new UserManager();
    }

    public function listAction()
    {
        $view = new View('subjects.list', 'admin');
        $view->assign('subjects', $this->subjectManager->findWithRelation());
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function addAction()
    {
        $form = new FormBuilder('POST', $this->getUrl('Subjects', 'add'));
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'name',
            'name' => 'name',
            'class' => 'form-control',
            'value' => '',
            'placeholder' => 'Nom de la matière',
            'label' => [
                'value' => 'Nom de la matière'
            ]
        ]);
        $form->addField(FormBuilder::SELECT, [
            'id' => 'teacher',
            'name' => 'teacher',
            'class' => 'form-control',
            'values' => array_map(function ($teacher) {
                return [
                    'text' => $teacher->getFirstname() . ' ' . $teacher->getLastname(),
                    'selected' => false,
                    'value' => $teacher->getId()
                ];
            }, $this->userManager->getTeachers()),
            'multiple' => false,
            'label' => [
                'value' => 'Professeur'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'id' => 'submit',
            'name' => 'submit',
            'value' => 'Créer',
            'class' => 'btn btn-dark-purple btn-block'
        ]);

        if ($this->isPost()) {
            $subject = new Subject();
            $subject->setName($this->getFromPost('name'));
            $subject->setTeacher($this->userManager->find($this->getFromPost('teacher')));
            $this->subjectManager->save($subject);
            header('Location:' . $this->getUrl('Subjects', 'list'));
        }

        $view = new View('subjects.add-edit', 'admin');
        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');
        $view->assign('form', $form);
        $view->assign('title','Ajout d\'une matière');
    }

    /**
     * @param int $id
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function editAction(int $id)
    {
        if (is_null($this->subjectManager->find($id)->getId())) {
            throw new NotFoundException('La matière choisie n\'existe pas');
        }
        /** @var Subject $subject */
        $subject = $this->subjectManager->findByIdWithRelation($id);

        $form = new FormBuilder('POST', $this->getUrl('Subjects', 'edit', ['id' => $id]));
        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'id' => 'name',
            'name' => 'name',
            'class' => 'form-control',
            'value' => $subject->getName(),
            'label' => [
                'value' => 'Nom de la matière'
            ]
        ]);
        $form->addField(FormBuilder::SELECT, [
            'id' => 'teacher',
            'name' => 'teacher',
            'class' => 'form-control',
            'values' => array_map(function ($teacher) use ($subject) {
                return [
                    'text' => $teacher->getFirstname() . ' ' . $teacher->getLastname(),
                    'selected' => $teacher->getId() === $subject->getTeacher()->getId(),
                    'value' => $teacher->getId()
                ];
            }, $this->userManager->getTeachers()),
            'multiple' => false,
            'label' => [
                'value' => 'Professeur'
            ]
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'id' => 'submit',
            'name' => 'submit',
            'value' => 'Modifier',
            'class' => 'btn btn-dark-purple btn-block'
        ]);

        if ($this->isPost() && $form->isValid()) {
            /** @var User $teacher */
            $teacher = $this->userManager->findWithRelation($this->getFromPost('teacher'));
            if (is_null($teacher->getId())) {
                throw new NotFoundException('Le professeur choisi n\'est pas trouvé');
            }
            $subject->setTeacher($teacher);
            $subject->setName($this->getFromPost('name'));
            $this->subjectManager->save($subject);
            header('Location:' . $this->getUrl('Subjects', 'list'));
        }

        $view = new View('subjects.add-edit', 'admin');
        $view->assign('subject', $subject);
        $view->assign('form', $form);
        $view->assign('title','Modification d\'une matière '.$subject->getName());
        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');
    }

    /**
     * @param int $idSubject
     * @throws NotFoundException
     */
    public function deleteAction(int $idSubject)
    {
        $this->subjectManager->delete($idSubject);
        header('Location:' . $this->getUrl('Subjects', 'list'));
    }
}