<?php

namespace Scolabs\Controllers;

use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Helpers;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ClassroomManager;
use Scolabs\Managers\StudentClassroomManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Classroom;
use Scolabs\Models\Studentclassrooms;
use Scolabs\Models\User;

/**
 * Class ClassroomsController
 * @package Scolabs\Controllers
 */
class ClassroomsController
{
    use UrlTrait;
    use RequestTrait;

    /**
     * @var ClassroomManager
     */
    private $manager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var StudentClassroomManager
     */
    private $studentClassroomManager;

    /**
     * ClassroomsController constructor.
     */
    public function __construct()
    {
        $this->manager = new ClassroomManager();
        $this->userManager = new UserManager();
        $this->studentClassroomManager = new StudentClassroomManager();
    }

    /**
     * List of all classrooms
     */
    public function indexAction()
    {
        $view = new View("classrooms.list", "admin");
        $view->assign("classrooms", $this->manager->findAll());
        $view->addJS('datatables.min.js');
    }

    public function listStudentsAction(int $id)
    {
        $view = new View ("classrooms.students", "admin");
        $view->assign("users",
        $this->studentClassroomManager->findByWithStudent($id));
        $view->assign("class",
        $this->manager->find($id));
        $view->addJS('datatables.min.js');
    }

    /**
     * @param int $id
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws NotAllowedException
     * @throws RequiredMissingException
     */
    public function editAction(int $id)
    {
        /** @var Classroom $classroom */
        $classroom = $this->manager->find($id);

        $students = [];
        foreach ($this->studentClassroomManager->findByWithStudent($id) as $key => $student) {
            $students[] = [
                'selected' => true,
                'value' => $student->getId(),
                'text' => $student->getFirstname() . ' ' . strtoupper($student->getLastname())
            ];
        }

        foreach ($this->userManager->getStudentsWithoutClassroom() as $student) {
            $students[] = [
                'selected' => false,
                'value' => $student->getId(),
                'text' => $student->getFirstname() . ' ' . strtoupper($student->getLastname())
            ];
        }

        $form = new FormBuilder("POST", $this->getUrl('Classrooms', 'edit', ['id' => intval($classroom->getId())]));
        $view = new View('classrooms.edit', 'admin');

        $form->addField('input', [
            'type' => 'text',
            'name' => 'name',
            'class' => 'form-control',
            'id' => 'name',
            'value' => $classroom->getName(),
            'label' => [
                'class' => '',
                'value' => 'Nom de la classe'
            ]
        ], [
            'regex' => '^[\w]+$',
            'error' => 'Le nom ne peut-être vide'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'level',
            'class' => 'form-control',
            'id' => 'level',
            'value' => $classroom->getLevel(),
            'label' => [
                'class' => '',
                'value' => 'Niveau de la classe'
            ]
        ], [
            'regex' => '^[\w]+$',
            'error' => 'Le niveau de la classe ne peut-être vide'
        ]);

        $form->addField(
            FormBuilder::SELECT,
            [
                'name' => 'students[]',
                'id' => 'students',
                'label' => [
                    'value' => 'Etudiants'
                ],
                'values' => $students,
                'multiple' => true,
                'class' => 'd-block w-100'
            ]
        );

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Modifier',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $classroom->setName($this->getFromPost('name'));
                $classroom->setLevel($this->getFromPost('level'));
                $this->manager->save($classroom);
                foreach ($this->getFromPost('students') as $studentID) {
                    if (!$this->studentClassroomManager->findBy(['id_student' => $studentID])) {
                        $studentClassroom = new Studentclassrooms();
                        $studentClassroom->setClassroom($classroom);
                        /** @var User $student */
                        $student = $this->userManager->find($studentID);
                        $studentClassroom->setStudent($student);
                        $studentClassroom->setClassroom($classroom);
                        $this->studentClassroomManager->save($studentClassroom);
                    }
                }
                header('Location: ' . $this->getUrl('Classrooms', 'index'));
            }
        }

        if (empty($classroom)) {
            throw new NotFoundException("La classe n'existe pas");
        } else {
            $view->assign('classroom', $classroom);
            $view->assign('form', $form);
            $view->assign('students', $students);
            $view->addCSS('select2.min.css');
            $view->addCSS('select2-bootstrap.min.css');
            $view->addJS('select2.min.js');
        }
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws NotAllowedException
     * @throws RequiredMissingException
     */
    public function addAction()
    {
        $form = new FormBuilder("POST", $this->getUrl('Classrooms', 'add'));
        $view = new View("classrooms.add", "admin");

        $students = [];
        foreach ($this->userManager->getStudents() as $key => $student) {
            $students[] = [
                'selected' => false,
                'value' => $student->getId(),
                'text' => $student->getFirstname() . ' ' . strtoupper($student->getLastname())
            ];
        }

        $form->addField('input', [
            'type' => 'text',
            'name' => 'name',
            'class' => 'form-control',
            'id' => 'name',
            'label' => [
                'class' => '',
                'value' => 'Nom de la classe'
            ]
        ], [
            'regex' => '^[\w]+$',
            'error' => 'Le nom ne peut-être vide'
        ]);

        $form->addField(FormBuilder::INPUT, [
            'type' => 'text',
            'name' => 'level',
            'class' => 'form-control',
            'id' => 'level',
            'label' => [
                'class' => '',
                'value' => 'Niveau de la classe'
            ]
        ], [
            'regex' => '^[\w]+$',
            'error' => 'Le niveau de la classe ne peut-être vide'
        ]);

        $form->addField(
            FormBuilder::SELECT,
            [
                'name' => 'students[]',
                'id' => 'students',
                'label' => [
                    'value' => 'Etudiants'
                ],
                'values' => $students,
                'multiple' => true,
                'class' => 'd-block w-100'
            ]
        );

        $form->addField(FormBuilder::INPUT, [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple',
            'value' => 'Ajouter',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                $classroom = new Classroom();
                $classroom->setName($this->getFromPost('name'));
                $classroom->setLevel($this->getFromPost('level'));
                $this->manager->save($classroom);

                /** @var Classroom $classroom */
                $classroom = $this->manager->getLastCreated();

                foreach ($this->getFromPost('students') as $studentId) {
                    $studentClassroom = new Studentclassrooms();
                    $studentClassroom->setClassroom($classroom);
                    /** @var User $student */
                    $student = $this->userManager->find($studentId);
                    $studentClassroom->setStudent($student);
                    $studentClassroom->setClassroom($classroom);
                    $this->studentClassroomManager->save($studentClassroom);
                }

                header('Location: ' . $this->getUrl('Classrooms', 'index'));
            }
        }

        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');

        $view->assign('form', $form);
    }

    /**
     * @param int $id
     * @return void
     * @throws NotAllowedException
     * @throws NotFoundException
     */
    public function deleteAction(int $id)
    {
        if ($this->isPost()) {
            /** @var Classroom $classroom */
            $classroom = $this->manager->find($id);
            if ($classroom && md5($classroom->getName()) === $this->getFromPost('token')) {
                foreach ($this->studentClassroomManager->findByWithStudent($id) as $item) {
                    $this->studentClassroomManager->deleteByIdStudent($item->getId());
                }
                $this->manager->delete($id);
                Helpers::redirect($this->getUrl('Classrooms', 'index'));
            }
        }
        throw new NotAllowedException('Unauthorized');
    }
}
