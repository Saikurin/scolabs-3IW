<?php


namespace Scolabs\controllers;

use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\Exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ConfigurationManager;
use Scolabs\Managers\PasswordResetManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Configuration;
use Scolabs\Models\Passwordreset;
use Scolabs\Models\User;

/**
 * Class AuthController
 * @package Scolabs\controllers
 */
class AuthController
{
    use UrlTrait;
    use RequestTrait;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var ConfigurationManager
     */
    private $configurationManager;

    /**
     * @var PasswordResetManager
     */
    private $passwordResetManager;

    /**
     * AuthController constructor.
     */
    public function __construct()
    {
        $this->userManager = new UserManager();
        $this->configurationManager = new ConfigurationManager();
        $this->passwordResetManager = new PasswordResetManager();
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function loginAction()
    {
        Auth::onlyVisitors();
        $form = new FormBuilder("POST", $this->getUrl('Auth', 'login'));

        $form->addField('input', [
            'type' => 'text',
            'id' => 'username',
            'name' => 'username',
            'placeholder' => "Saisissez votre nom utilisateur",
            'label' => [
                'value' => 'Nom d\'utilisateur'
            ],
        ], [
            'regex' => '[\s\S]',
            'error' => 'Le pseudo est obligatoire pour se connecter'
        ]);

        $form->addField('input', [
            'type' => 'password',
            'id' => 'password',
            'name' => 'password',
            'placeholder' => 'Saisissez votre mot de passe',
            'label' => [
                'value' => 'Mot de passe'
            ]
        ], [
            'regex' => '[\s\S]',
            'error' => 'Le mot de passe est obligatoire pour se connecter'
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Se connecter',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                if(isset($_POST['username']) && isset($_POST['password'])) {
                    if(Auth::loginUser($this->getFromPost('username'), $this->getFromPost('password'))){
                        header('Location:' . $this->getUrl('Dashboard', 'index'));
                    }
                }
            }
        }

        $view = new View('auth.login', 'account');
        $view->assign('form', $form);
    }

    /**
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     * @throws Exception
     */
    public function forgotpasswordAction()
    {
        Auth::onlyVisitors();
        $form = new FormBuilder('POST', $this->getUrl('Auth', 'forgotpassword'));
        $form->addField('input', [
            'type' => 'email',
            'id' => 'email',
            'name' => 'email',
            'placeholder' => "Saisissez votre adresse email",
            'label' => [
                'value' => 'Adresse email'
            ],
        ]);

        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Reinitialiser son mot de passe',
            'name' => 'submit',
            'id' => 'submit'
        ]);

        if ($this->isPost()) {
            if ($form->isValid()) {
                /** @var User $user */
                $user = $this->userManager->findOneBy(['email' => $this->getFromPost('email'), 'state' => 0]);
                if (is_int($user->getId())) {
                    if (null !== MAILER_URL && !empty(MAILER_URL)) {
                        /** @var Configuration $emailMaster */
                        $emailMaster = $this->configurationManager->findOneBy(['conf' => 'master_email_address']);
                        /** @var Configuration $nameMaster */
                        $nameMaster = $this->configurationManager->findOneBy(['conf' => 'master_email_name']);
                        /** @var Configuration $titleOfSite */
                        $titleOfSite = $this->configurationManager->findOneBy(['conf' => 'title']);

                        $key = md5(2418 * 2);
                        $addKey = substr(md5(uniqid(rand(), 1)), 3, 10);
                        $token = $key . $addKey;

                        $expDate = date('Y-m-d H:i:s', strtotime("+ 24 hours"));
                        $resetPassword = new Passwordreset();
                        $resetPassword->setUser($user);
                        $resetPassword->setExpDate($expDate);
                        $resetPassword->setToken($token);
                        $this->passwordResetManager->save($resetPassword);
                        $url = $_SERVER['HTTP_ORIGIN'] . ':' . $_SERVER['SERVER_PORT'] . $this->getUrl('Auth', 'setnewpassword', ['token' => $token]);

                        $output = '<p>Cher utilisateur de ' . $titleOfSite->getValue() . ',</p>';
                        $output .= '<p>Merci de cliquer sur le lien suivant pour réinitialiser votre mot de passe.</p>';
                        $output .= '<p>-------------------------------------------------------------</p>';
                        $output .= '<p><a href="' . $url . '" target="_blank">' . $url . '</a></p>';
                        $output .= '<p>-------------------------------------------------------------</p>';
                        $output .= '<p>Ce lien expire après 24h pour des raisons de sécurité</p>';
                        $output .= '<p>Si vous n\'êtes pas à l\'origine de cette démarche, nous vous conseillons de vous connecter afin de changer votre mot de passe au plus vite</p>';
                        $output .= '<p>Merci,</p>';
                        $output .= '<p>L\'équipe ' . $titleOfSite->getValue() . '</p>';

                        $mailer = new PHPMailer();
                        $mailer->isSMTP();
                        $mailer->Host = MAILER_URL;
                        $mailer->SMTPAuth = MAILER_SMTP;
                        $mailer->Username = MAILER_USERNAME;
                        $mailer->Password = MAILER_PASSWORD;
                        $mailer->SMTPSecure = MAILER_SECURE;
                        $mailer->Port = MAILER_PORT;

                        $mailer->setFrom($emailMaster->getValue(), $nameMaster->getValue());
                        $mailer->addAddress($user->getEmail(), $user->getFirstname() . ' ' . strtoupper($user->getLastname()));

                        $mailer->isHTML(true);
                        $mailer->Subject = $titleOfSite->getValue() . ' - Reinitialisation du mot de passe';
                        $mailer->Body = $output;
                        $mailer->send();
                        header('Location:' . $this->getUrl('Auth', 'login'));
                    } else {
                        throw new NotFoundException("Le serveur SMTP n'est pas configuré");
                    }
                }
            }
        }

        $view = new View('auth.forgotpassword', 'account');
        $view->assign('form', $form);
    }

    /**
     * @param string $token
     * @return bool
     * @throws NotAllowedException
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function setnewpasswordAction(string $token)
    {
        Auth::onlyVisitors();
        if (isset($token)) {
            /** @var Passwordreset $passwordReset */
            $passwordReset = $this->passwordResetManager->findWithRelation($token);
            if (!is_null($passwordReset->getId()) && date('Y-m-d H:i:s') < $passwordReset->getExpDate()) {
                $view = new View('auth.resetpassword', 'account');
                $form = new FormBuilder('POST', $this->getUrl('Auth', 'setnewpassword', ['token' => $token]));
                $form->addField('input', [
                    'type' => 'email',
                    'id' => 'email',
                    'name' => 'email',
                    'class' => 'form-control',
                    'label' => [
                        'value' => 'Saisir l\'adresse e-mail lié au compte'
                    ]
                ]);
                $form->addField('input', [
                    'type' => 'password',
                    'id' => 'password',
                    'name' => 'password',
                    'class' => 'form-control',
                    'label' => [
                        'value' => 'Nouveau mot de passe'
                    ]
                ]);
                $form->addField('input', [
                    'type' => 'password',
                    'id' => 'repeat-password',
                    'name' => 'repeat-password',
                    'class' => 'form-control',
                    'label' => [
                        'value' => 'Nouveau mot de passe à nouveau'
                    ]
                ]);
                $form->addField('input', [
                    'type' => 'submit',
                    'id' => 'submit',
                    'name' => 'submit',
                    'value' => 'Réinitialiser',
                    'class' => 'btn btn-dark-purple'
                ]);
                if ($this->isPost()) {
                    if ($form->isValid()) {
                        $user = $passwordReset->getUser();
                        if (
                            ($this->getFromPost('password') === $this->getFromPost('repeat-password')) &&
                            ($user->getEmail() === $this->getFromPost('email')) &&
                            ($user->getState() === 0)
                        ) {
                            $user->setPassword(password_hash($this->getFromPost('password'), PASSWORD_BCRYPT));
                            $this->userManager->save($user);
                            $passwordReset->setExpDate(date('Y-m-d H:i:s'));
                            $this->passwordResetManager->save($passwordReset);
                            Auth::loginUser($user->getUsername(), $this->getFromPost('password'));
                            header('Location:' . $this->getUrl('Dashboard', 'index'));
                        } else {
                            $view->assign('error', 'Connexion au compte impossible');
                        }
                    }
                }
                $view->assign('form', $form);
                return true;
            }
        }
        throw new NotAllowedException("Vous n'avez pas les droits pour accéder à cette page");
    }

    public function logoutAction()
    {
        unset($_SESSION['auth']);
        header('Location: /');
    }
}
