<?php

namespace Scolabs\controllers;

use Scolabs\Core\Auth;
use Scolabs\Core\Exceptions\NotAllowedException;
use Scolabs\Core\Exceptions\NotFoundException;
use Scolabs\Core\Exceptions\NotSupportedException;
use Scolabs\Core\exceptions\RequiredMissingException;
use Scolabs\Core\FormBuilder;
use Scolabs\Core\Traits\RequestTrait;
use Scolabs\Core\Traits\UrlTrait;
use Scolabs\Core\View;
use Scolabs\Managers\ClassroomManager;
use Scolabs\Managers\MarkManager;
use Scolabs\Managers\StudentClassroomManager;
use Scolabs\Managers\SubjectManager;
use Scolabs\Managers\UserManager;
use Scolabs\Models\Classroom;
use Scolabs\Models\Mark;
use Scolabs\Models\Subject;
use Scolabs\Models\User;

class MarksController
{

    use UrlTrait;
    use RequestTrait;

    /**
     * @var MarkManager
     */
    private $markManager;

    /**
     * @var ClassroomManager
     */
    private $classroomsManager;

    /**
     * @var StudentClassroomManager
     */
    private $studentClassroomManager;

    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var SubjectManager
     */
    private $subjectManager;

    /**
     * MarksController constructor.
     */
    public function __construct()
    {
        $this->markManager = new MarkManager();
        $this->classroomsManager = new ClassroomManager();
        $this->studentClassroomManager = new StudentClassroomManager();
        $this->userManager = new UserManager();
        $this->subjectManager = new SubjectManager();
    }

    public function classroomsAction()
    {
        $view = new View('marks.classrooms', 'admin');
        if (Auth::getUser()->getRole()->getSlug() === 'teachers') {
            $view->assign('classrooms', $this->classroomsManager->findAll());
        } else {
            $view->assign('marks', $this->markManager->findWithRelationById(Auth::getUser()->getId()));
        }
        $view->addJS('datatables.min.js');
        $view->addCSS('datatables.min.css');
    }

    /**
     * @param string $name
     * @throws NotFoundException
     */
    public function attributeAction(string $name)
    {
        /** @var Classroom $classroom */
        $classroom = $this->classroomsManager->findOneBy(['name' => str_replace('-', ' ', $name)]);
        $students = $this->studentClassroomManager->findByWithStudent($classroom->getId());
        foreach ($students as $key => $student) {
            $students[$key]->marks = $this->markManager->findBy(['id_student' => $student->getId()], [], Mark::class);
        }
        if (!is_int($classroom->getId())) {
            throw new NotFoundException('La classe recherchée n\'est pas trouvée');
        }
        $view = new View('marks.attributes', 'admin');
        $view->assign('classroom', $classroom);
        $view->assign('students', $students);
    }

    /**
     * @param int $idStudent
     * @throws NotFoundException
     * @throws NotSupportedException
     * @throws RequiredMissingException
     */
    public function addAction(int $idStudent)
    {
        /** @var User $user */
        $user = $this->userManager->find($idStudent);

        if (!is_int($user->getId())) {
            throw new NotFoundException('Une erreur est survenue');
        }

        $form = new FormBuilder('POST', $this->getUrl('Marks', 'add', ['id' => $idStudent]));

        $form->addField('input', [
            'type' => 'text',
            'id' => 'mark',
            'name' => 'mark',
            'placeholder' => 'Note sur 20',
            'class' => 'form-control',
            'label' => [
                'value' => 'Note'
            ]
        ], [
            'regex' => '\d',
            'error' => 'La note saisie n\'est pas correct'
        ]);
        $form->addField('input', [
            'type' => 'text',
            'id' => 'coeff',
            'name' => 'coeff',
            'placeholder' => 'Coefficient',
            'class' => 'form-control',
            'label' => [
                'value' => 'Coefficient'
            ]
        ], [
            'regex' => '\d',
            'error' => 'Le coefficient saisi n\'est pas correct'
        ]);
        $form->addField('input', [
            'type' => 'date',
            'id' => 'date',
            'name' => 'date',
            'class' => 'form-control',
            'label' => [
                'value' => 'Date du controle'
            ]
        ], [
            'regex' => '\d{4}-\d{2}-\d{2}',
            'error' => 'Le coefficient saisi n\'est pas correct'
        ]);
        $form->addField('input', [
            'type' => 'submit',
            'class' => 'btn btn-dark-purple btn-block',
            'value' => 'Ajouter la note',
            'name' => 'submit',
            'id' => 'submit'
        ]);
        $form->addField('select', [
            'name' => 'subject',
            'class' => 'form-control',
            'id' => 'subject',
            'label' => [
                'class' => '',
                'value' => 'Matière'
            ],
            'values' => array_map(function ($item) {
                return [
                    "selected" => false,
                    "value" => $item->getId(),
                    "text" => $item->getName()
                ];
            }, $this->subjectManager->findBy(['id_teacher' => Auth::getUser()->getId()], [], Subject::class)),
            "multiple" => false
        ]);


        if ($this->isPost() && $form->isValid()) {
            /** @var Subject $subject */
            $subject = $this->subjectManager->findByIdWithRelation($this->getFromPost('subject'));
            if (!is_int($subject->getId()) || !$subject->getTeacher()->getId() === Auth::getUser()->getId()) {
                throw new RequiredMissingException('La matière n\'est pas renseigné');
            }
            $mark = new Mark();
            $mark->setStudent($user);
            $mark->setDate($this->getFromPost('date'));
            $mark->setNote($this->getFromPost('mark'));
            $mark->setCoeff($this->getFromPost('coeff'));
            $mark->setSubject($subject);
            $this->markManager->save($mark);
            echo "<script>window.close();</script>";
        }

        $view = new View('marks.add', 'admin');
        $view->assign('form', $form);
        $view->assign('user', $user);

        $view->addCSS('select2.min.css');
        $view->addCSS('select2-bootstrap.min.css');
        $view->addJS('select2.min.js');
    }

    /**
     * @param int $id_student
     * @throws NotAllowedException
     */
    public function marksAction(int $id_student)
    {
        if (!$this->isAjax()) {
            http_response_code(401);
            throw new NotAllowedException('La route n\'est pas autorisée');
        }
        $marks = $this->markManager->findWithRelationById($id_student);

        $jsonArray = [];

        foreach ($marks as $key => $mark) {
            $jsonArray[$key] = $mark->__toArray();
            $jsonArray[$key]['subject'] = $mark->getSubject()->__toArray();
            $jsonArray[$key]['teacher'] = $mark->getSubject()->getTeacher()->__toArray();
        }

        $jsonArray['student'] = $this->userManager->find($id_student)->__toArray();

        echo json_encode($jsonArray);
    }
}