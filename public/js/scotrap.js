$.fn.extend({
  modal: function modal(event) {
    var modalWindow = this;

    switch (event) {
      case "show":
        modalWindow.css("display", "block");
        break;

      case "hide":
        modalWindow.css("display", "none");
        break;
    }

    $(".close").on('click', function () {
      modalWindow.modal('hide');
    });
    $(document).on('keydown', function (event) {
      if (event.key === 'Escape') {
        modalWindow.modal('hide');
      }
    });
  }
});
