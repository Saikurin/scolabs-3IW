<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Configuration;

class ConfigurationManager extends Manager
{
    /**
     * ConfigurationManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Configuration::class, 'configurations');
    }
}
