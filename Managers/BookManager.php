<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Book;

/**
 * Class BookManager
 * @package Scolabs\Managers
 */
class BookManager extends Manager
{

    /**
     * BookManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Book::class, "books");
    }

    /**
     * @return array
     */
    public function getBooksAvailable()
    {
        return $this->findBy([
            'status' => 'DISPONIBLE'
        ], null, Book::class);
    }
}
