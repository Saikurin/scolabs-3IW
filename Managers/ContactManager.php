<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Contact;

/**
 * Class ContactManager
 * @package Scolabs\managers
 */
class ContactManager extends Manager
{

    /**
     * ContactManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Contact::class, 'contact');
    }

    public function getNonViewedContacts(): array
    {
        return
            $this->getQueryBuilder()
                ->where('viewed = :viewed')
                ->setParameter('viewed', 'false')
                ->getQuery()
                ->getArrayResult(Contact::class);
    }

}
