<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Classroom;
use Scolabs\Models\User;

/**
 * Class ClassroomManager
 * @package Scolabs\Managers
 */
class ClassroomManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Classroom::class, 'classrooms');
    }

    /**
     * @return Model|null
     */
    public function getLastCreated()
    {
        return $this->getQueryBuilder()
            ->orderBy('id', 'desc')
            ->getQuery()
            ->getOneOrNullResult(Classroom::class);
    }
}
