<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Aclrole;

class AclRoleManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Aclrole::class, 'acl_roles');
    }

    /**
     * @param array $conditions
     * @return Model|null
     */
    public function findOneWithRelation(array $conditions)
    {
        return $this->getQueryBuilder()
            ->leftJoin('roles', 'Role', 'id_role', 'id', true)
            ->leftJoin('acl', 'Acl', 'id_acl', 'id', true)
            ->where('Role.id = :r')
            ->where('Acl.id = :a')
            ->setParameter('a', $conditions['id_acl'])
            ->setParameter('r', $conditions['id_role'])
            ->getQuery()
            ->getOneOrNullResult(Aclrole::class);
    }
}
