<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Invoiceproduct;

/**
 * Class InvoiceProductManager
 * @package Scolabs\managers
 */
class InvoiceProductManager extends Manager
{
    /**
     * InvoiceProductManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Invoiceproduct::class, 'invoices_products');
    }

    /**
     * @param int $idInvoice
     * @return array
     */
    public function getProductsByInvoice(int $idInvoice)
    {
        return $this->getQueryBuilder()
            ->where('id_invoice = :id')
            ->leftJoin('invoices', 'Invoice', 'id_invoice', 'id')
            ->leftJoin('products', 'Product', 'id_product', 'id')
            ->setParameter('id', $idInvoice)
            ->getQuery()
            ->getArrayResult(Invoiceproduct::class);
    }
}
