<?php


namespace Scolabs\Managers;


use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Subject;

class SubjectManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Subject::class,'subjects');
    }

    /**
     * @param $id
     * @return Model
     */
    public function findByIdWithRelation($id)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users','Teacher','id_teacher','id')
            ->where('Subject.id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getOneOrNullResult(Subject::class);
    }

    /**
     * @return array
     */
    public function findWithRelation()
    {
        return $this->getQueryBuilder()
            ->leftJoin('users','Teacher','id_teacher','id')
            ->getQuery()
            ->getArrayResult(Subject::class);
    }

}