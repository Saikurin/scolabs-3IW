<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\User;

/**
 * Class UserManager
 * @package Scolabs\Managers
 */
class UserManager extends Manager
{
    /**
     * UserManager constructor.
     */
    public function __construct()
    {
        parent::__construct(User::class, 'users');
    }

    /**
     * @return array Array of users
     */

     public function getAllUsersWithRole(): array
     {
         return
            $this->getQueryBuilder()
                ->leftJoin('roles', 'Role', 'id_role', 'id')
                ->getQuery()
                ->getArrayResult(User::class);
     }

    /**
     * @return array Array of student
     */
    public function getStudents(): array
    {
        return
            $this->getQueryBuilder()
                ->leftJoin('roles', 'Role', 'id_role', 'id')
                ->where('Role.slug = :slug')
                ->setParameter('slug', 'students')
                ->getQuery()
                ->getArrayResult(User::class);
    }

    /**
     * @return array Array of student
     */
    public function getStudentsWithoutClassroom(): array
    {
        return
            $this->getQueryBuilder()
                ->leftJoin('roles', 'Role', 'id_role', 'id')
                ->leftJoin('students_classrooms', 'su', 'User.id', 'id_student')
                ->where('Role.slug = :slug')
                ->where('su.id_student IS NULL')
                ->setParameter('slug', 'students')
                ->getQuery()
                ->getArrayResult(User::class);
    }

    public function getUserWithRole(int $id)
    {
        return $this->getQueryBuilder()
            ->leftJoin('roles', 'Role', 'id_role', 'id')
            ->where('User.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult(User::class);
    }

    /**
     * @return array Array of student
     */
    public function getParents(): array
    {
        return
            $this->getQueryBuilder()
                ->leftJoin('roles', 'Role', 'id_role', 'id')
                ->where('Role.slug = :slug')
                ->setParameter('slug', 'parents')
                ->getQuery()
                ->getArrayResult(User::class);
    }

    /**
     * @param $idStudent
     * @return array Array of student
     */
    public function getParentsByIdStudent($idStudent): array
    {
        return
            $this->getQueryBuilder()
                ->leftJoin('users_parent', 'up', 'User.id', 'id_parent', true)
                ->leftJoin('roles', 'nr', 'id_role', 'id', true)
                ->where('nr.slug = :slug')
                ->where('up.id_student = :id')
                ->setParameter('slug', 'parents')
                ->setParameter('id', $idStudent)
                ->setParameter('slug', 'parents')
                ->getQuery()
                ->getArrayResult(User::class);
    }

    /**
     * @param int $idUser
     * @return Model|null
     */
    public function findWithRelation(int $idUser)
    {
        return $this->getQueryBuilder()
            ->leftJoin('roles', 'Role', 'id_role', 'id', true)
            ->where('User.id = :id')
            ->setParameter('id', $idUser)
            ->getQuery()
            ->getOneOrNullResult(User::class);
    }

    public function getTeachers()
    {
        return
            $this->getQueryBuilder()
                ->leftJoin('roles', 'Role', 'id_role', 'id')
                ->where('Role.slug = :slug')
                ->setParameter('slug', 'teachers')
                ->getQuery()
                ->getArrayResult(User::class);
    }
}
