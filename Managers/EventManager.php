<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Events;

/**
 * Class NewsManager
 * @package Scolabs\managers
 */
class EventManager extends Manager
{

    /**
     * EventManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Events::class, 'events');
    }

    /**
     * @return array
     */
    public function getLastFiveNews(): array
    {
        return $this->getQueryBuilder()
            ->limit(0, 5)
            ->where('state = :state')
            ->setParameter('state', 1)
            ->orderBy('Events.id', 'DESC')
            ->getQuery()
            ->getArrayResult(Events::class);
    }

    public function findByIdWithRelation(int $id)
    {
        return $this->getQueryBuilder()
            ->where('Events.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult(Events::class);
    }
}
