<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Role;

/**
 * Class RoleManager
 * @package Scolabs\managers
 */
class RoleManager extends Manager
{

    /**
     * RoleManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Role::class, 'roles');
    }
}
