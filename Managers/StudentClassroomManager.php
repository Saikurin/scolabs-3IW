<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Studentclassrooms;
use Scolabs\Models\User;

/**
 * Class StudentClassroomManager
 * @package Scolabs\managers
 */
class StudentClassroomManager extends Manager
{

    /**
     * StudentClassroomManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Studentclassrooms::class, 'students_classrooms');
    }

    /**
     * @param int $idClassromom
     * @return array
     */
    public function findByWithStudent(int $idClassromom)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users', 'User', 'id_student', 'id', true)
            ->where('id_classroom = :id')
            ->setParameter('id', $idClassromom)
            ->getQuery()
            ->getArrayResult(User::class);
    }

    /**
     * @param int $idStudent
     */
    public function deleteByIdStudent(int $idStudent)
    {
        $this->connection->query('DELETE FROM ' . DB_PREFIXE . $this->table . ' WHERE id = :id', [
            'id' =>
                $this->getQueryBuilder()
                    ->where('id_student = :id')
                    ->setParameter('id', $idStudent)
                    ->getQuery()
                    ->getOneOrNullResult(Studentclassrooms::class)->getId()
        ]);
    }

    /**
     * @param int $idStudent
     * @return Model|null
     */
    public function getClassroomByStudent(int $idStudent)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users','Student','id_student')
            ->leftJoin('classrooms','Classroom','id_classroom')
            ->where('Student.id = :id')
            ->setParameter('id', $idStudent)
            ->getQuery()
            ->getOneOrNullResult(Studentclassrooms::class)->getClassroom();
    }
}
