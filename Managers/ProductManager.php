<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Product;

/**
 * Class ProductManager
 * @package Scolabs\managers
 */
class ProductManager extends Manager
{
    /**
     * ProductManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Product::class, 'products');
    }

    /**
     * @return Product[]
     */
    public function getAllAvailable()
    {
        return $this->getQueryBuilder()
            ->where('availability = :available')
            ->setParameter('available', 'DISPONIBLE')
            ->getQuery()
            ->getArrayResult(Product::class);
    }
}
