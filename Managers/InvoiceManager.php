<?php


namespace Scolabs\Managers;

use Scolabs\Core\Builder\Querybuilder;
use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Invoice;

/**
 * Class InvoiceManager
 * @package Scolabs\managers
 */
class InvoiceManager extends Manager
{
    /**
     * InvoiceManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Invoice::class, 'invoices');
    }

    /**
     * @return int Next number invoice
     */
    public function getNewInvoiceNumber()
    {
        return $this->count() + 1;
    }

    /**
     * @return Model|null
     */
    public function getLastInvoice()
    {
        return $this->getQueryBuilder()
            ->orderBy('id', 'desc')
            ->getQuery()
            ->getOneOrNullResult(Invoice::class);
    }

    public function findWithRelations()
    {
        return (new Querybuilder())
            ->from($this->table, 'Invoice')
            ->leftJoin('users', 'Creator', 'id_creator')
            ->leftJoin('users', 'Updator', 'id_updator')
            ->leftJoin('users', 'Student', 'id_student')
            ->leftJoin('users', 'Customer', 'id_customer')
            ->orderBy('Invoice__id', 'ASC')
            ->getQuery()
            ->getArrayResult(Invoice::class);
    }

    public function findOneWithRelations(string $id)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users', 'Creator', 'id_creator')
            ->leftJoin('users', 'Updator', 'id_updator')
            ->leftJoin('users', 'Student', 'id_student')
            ->leftJoin('users', 'Customer', 'id_customer')
            ->where('Invoice.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult(Invoice::class);
    }

    public function getInvoicesByUser(int $id)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users', 'Creator', 'id_creator')
            ->leftJoin('users', 'Updator', 'id_updator')
            ->leftJoin('users', 'Student', 'id_student')
            ->leftJoin('users', 'Customer', 'id_customer')
            ->where('Invoice.id_customer = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult(Invoice::class);
    }
}
