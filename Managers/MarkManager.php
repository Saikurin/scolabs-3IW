<?php


namespace Scolabs\Managers;


use Scolabs\Core\Manager;
use Scolabs\Models\Mark;

class MarkManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Mark::class, "notes");
    }

    /**
     * @param int $id_student
     * @return array
     */
    public function findWithRelationById(int $id_student)
    {
        return $this->getQueryBuilder()
            ->leftJoin('subjects','Subject','id_subject')
            ->leftJoin('users','Teacher','Subject.id_teacher')
            ->leftJoin('users','Student','id_student')
            ->where('Mark.id_student = :id')
            ->setParameter('id',$id_student)
            ->getQuery()
            ->getArrayResult(Mark::class);
    }
}
