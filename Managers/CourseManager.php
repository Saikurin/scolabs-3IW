<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Course;

class CourseManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Course::class, "courses");
    }

    public function findBySubjectWithRelation(int $idSubject)
    {
        $qd = $this->getQueryBuilder();
        return $qd->where('id_subject = :id')
            ->setParameter('id', $idSubject)
            ->leftJoin('classrooms', 'Classroom', 'id_classroom')
            ->leftJoin('subjects', 'Subject', 'id_subject')
            ->getQuery()
            ->getArrayResult(Course::class);
    }

    public function findByClassroomWithRelation(int $idClassroom)
    {
        $qd = $this->getQueryBuilder();
        return $qd->where('id_classroom = :id')
            ->setParameter('id', $idClassroom)
            ->leftJoin('classrooms', 'Classroom', 'id_classroom')
            ->leftJoin('subjects', 'Subject', 'id_subject')
            ->leftJoin('users','Teacher','id_teacher')
            ->getQuery()
            ->getArrayResult(Course::class);
    }
}
