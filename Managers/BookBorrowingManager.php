<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Book;
use Scolabs\Models\BookBorrowing;
use Scolabs\Models\User;

/**
 * Class BookBorrowingManager
 * @package Scolabs\Managers
 */
class BookBorrowingManager extends Manager
{

    /**
     * BookBorrowingManager constructor.
     */
    public function __construct()
    {
        parent::__construct(BookBorrowing::class, 'book_borrowing');
    }

    /**
     * @param User $currentUser
     * @param Book $book
     * @param int $limit
     */
    public function reserveBook(User $currentUser, Book $book, int $limit)
    {
        $date = date('Y-m-d');
        $dateLimit = date('Y-m-d', strtotime($date . ' + ' . $limit . ' days'));

        $bookBorrowing = new BookBorrowing();
        $bookBorrowing->setUser($currentUser);
        $bookBorrowing->setBook($book);
        $bookBorrowing->setState('En cours de prêt');
        $bookBorrowing->setStart($date);
        $bookBorrowing->setEnd($dateLimit);

        $this->save($bookBorrowing);
    }
}
