<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Core\Model;
use Scolabs\Models\Passwordreset;

/**
 * Class PasswordResetManager
 * @package Scolabs\managers
 */
class PasswordResetManager extends Manager
{

    /**
     * PasswordResetManager constructor.
     */
    public function __construct()
    {
        parent::__construct(Passwordreset::class, 'password_reset');
    }

    /**
     * @param string $token
     * @return Model|null
     */
    public function findWithRelation(string $token)
    {
        return $this->getQueryBuilder()
            ->leftJoin('users', 'User', 'id_user', 'id', true)
            ->where('Passwordreset.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult(Passwordreset::class);
    }
}
