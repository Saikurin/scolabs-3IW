<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\User;
use Scolabs\models\UsersParent;

/**
 * Class BookBorrowingManager
 * @package Scolabs\Managers
 */
class UsersParentManager extends Manager
{

    /**
     * BookBorrowingManager constructor.
     */
    public function __construct()
    {
        parent::__construct(UsersParent::class, 'users_parent');
    }
}
