<?php


namespace Scolabs\Managers;

use Scolabs\Core\Manager;
use Scolabs\Models\Acl;

class AclManager extends Manager
{
    public function __construct()
    {
        parent::__construct(Acl::class, 'acl');
    }
}
